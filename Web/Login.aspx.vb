Partial Public Class Login
    Inherits System.Web.UI.Page

    Protected Sub btnsubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEnter.Click
        If Page.IsValid Then
            Dim user As IUser = New IUser(txtUserName.Text, txtPassword.Text)
            If user.IsAuthenticated Then
                'Dim userID As String = user.ID & ";" & user.LoginID

                '' Create the cookie that contains the forms authentication ticket.
                'Dim authCookie As HttpCookie = FormsAuthentication.GetAuthCookie(userID, cookieuser.Checked)

                '' Get the FormsAuthenticationTicket out of the encrypted cookie.
                'Dim ticket As FormsAuthenticationTicket = FormsAuthentication.Decrypt(authCookie.Value)

                ''Create a new FormsAuthenticationTicket that includes our custom User Data.
                ''Dim newTicket As New FormsAuthenticationTicket(ticket.Version, ticket.Name, ticket.IssueDate, DateTime.Now.AddYears(50), ticket.IsPersistent, user.Role)
                'Dim newTicket As New FormsAuthenticationTicket(ticket.Version, ticket.Name, ticket.IssueDate, DateTime.Now.AddMinutes(2), ticket.IsPersistent, user.Role)

                '' Update the authCookie's Value to use the encrypted version of newTicket.
                'authCookie.Value = FormsAuthentication.Encrypt(newTicket)
                'authCookie.Expires = newTicket.Expiration

                '' Manually add the authCookie to the Cookies collection.
                'Response.Cookies.Add(authCookie)

                Session("UserID") = user.ID
                Session("LoginName") = user.LoginID
                Session("UserRole") = user.Role
                Session.Timeout = 30

                ' Redirect browser back to recent page.
                If Request.QueryString("returnUrl") IsNot Nothing Then
                    Dim pos As Integer = Request.RawUrl.ToLower().IndexOf("returnurl")
                    Dim returnUrl As String = Request.RawUrl.Substring(pos + 10)
                    Dim url As String = "~" & Server.UrlDecode(returnUrl)
                    Response.Redirect(url)
                Else
                    Response.Redirect("~/Default.aspx")
                End If
            Else
                lblError.Visible = True
            End If
        End If
    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.txtUserName.Focus()
    End Sub
End Class