﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Template.Master" CodeFile="Search.aspx.vb" Inherits="Search" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="componentheading">
        <asp:Label ID="lblHeader" runat="server" Text="Label"/>
    </div>
    <table cellpadding="1" cellspacing="0" border="0">
        <tr> 
            <td>Tìm theo</td> 
            <td>
                <asp:DropDownList ID="ddlSearchFor" runat="server">
                    <asp:ListItem Selected="True">[Tất cả]</asp:ListItem>
                    <asp:ListItem>Chủ đề</asp:ListItem>
                    <asp:ListItem>Nội dung</asp:ListItem>
                    <asp:ListItem>Người gửi</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td><telerik:RadTextBox ID="txtSearch" runat="server" width="400px" EmptyMessage="Nhập từ khóa cần tìm." /></td>
            <td><asp:Button ID="btnSearch" runat="server" Text="Tìm kiếm" /></td>
        </tr>
    </table>
    <hr />
    <telerik:RadGrid ID="RadGrid1" runat="server" AutoGenerateColumns="False" GridLines="None"
        OnNeedDataSource="RadGrid1_NeedDataSource" ShowHeader="false" BorderWidth="0"
        PageSize="20" AllowPaging="True" AllowSorting="True" CssClass="searchfrm" Width="100%" Height="500px" >
        <PagerStyle Mode="NextPrevAndNumeric" Position="Top"></PagerStyle>
        <MasterTableView DataKeyNames="PersonalBox_Id,Source_Id,Type" Width="98%">
            <NoRecordsTemplate>
                <div>Không tìm thấy tài liệu.</div>
            </NoRecordsTemplate>

            <Columns>
                <telerik:GridTemplateColumn UniqueName="Column1" HeaderText="Content">
                    <ItemStyle Height="45px"></ItemStyle>
                    <ItemTemplate>
                        <p><asp:HyperLink ID="HyperLink1" Text='<%# Eval("Subject") %>' NavigateUrl='<%# IIF(Eval("type")="DO","~/viewdoc.aspx","~/viewmail.aspx") & "?id=" & Eval("Source_Id") & "&postid=" & Eval("PersonalBox_Id") %>' runat="server" CssClass="title"/><br /></p>
                        <span><%# DataBinder.Eval(Container.DataItem, "SendTime") %></span><br />
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
            </Columns>
        </MasterTableView>
        <ClientSettings ReorderColumnsOnClient="True" AllowColumnsReorder="True" EnableRowHoverStyle="true">
            <Selecting AllowRowSelect="True"></Selecting>
            <Scrolling AllowScroll="True" UseStaticHeaders="True"></Scrolling>
        </ClientSettings>

    </telerik:RadGrid>
</asp:Content>
