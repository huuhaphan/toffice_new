﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/Template.Master" CodeFile="AuthorizeEdit.aspx.vb" Inherits="AuthorizeEdit" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder">
    <script type="text/javascript">
    function OnClientItemsRequesting(sender,e)
    {
        if(sender.get_appendItems())
            e.get_context().CustomText="";
        else
            e.get_context().CustomText=sender.get_text();
    }
    </script>

    <div class="componentheading">Tạo mới/ Cập nhật ủy quyền</div>
    <table cellpadding="4" cellspacing="0" border="0">
        <colgroup>
            <col width="100" />
            <col width="220" />
        </colgroup>
        <tr>
            <td>Từ ngày</td>
            <td>
                <telerik:RadDatePicker ID="rdpFrDate" Style="vertical-align: middle;" Width="200px" runat="server">
                    <DateInput ID="DateInput1" runat="server" DateFormat="dd/MM/yyyy" />
                </telerik:RadDatePicker>
                <asp:RequiredFieldValidator ID="rfvFrDate" ControlToValidate="rdpFrDate" 
                    ErrorMessage="*" EnableClientScript="true" runat="server" ValidationGroup="Save"></asp:RequiredFieldValidator> 
            </td>
        </tr>
        <tr>
            <td>Đến ngày</td>
            <td>               
                <telerik:RadDatePicker ID="rdpToDate" Style="vertical-align: middle;" Width="200px" runat="server">
                    <DateInput runat="server" DateFormat="dd/MM/yyyy"/>
                </telerik:RadDatePicker>
                <asp:RequiredFieldValidator ID="rfvToDate" ControlToValidate="rdpToDate" 
                    ErrorMessage="*" EnableClientScript="true" runat="server" ValidationGroup="Save"></asp:RequiredFieldValidator> 
            </td>
        </tr>
        <tr>
            <td>Ủy quyền cho</td>
            <td>
                <telerik:RadComboBox ID="rcbUser" runat="server" Width="200px" AllowCustomText="True" ShowToggleImage="True" ShowMoreResultsBox="true"
                    EnableLoadOnDemand="True" MarkFirstMatch="True" OnClientItemsRequesting="OnClientItemsRequesting" OnItemsRequested="rcbUser_ItemsRequested" EnableVirtualScrolling="true" >
                </telerik:RadComboBox>
                <asp:RequiredFieldValidator ID="rfvUser" ControlToValidate="rcbUser" 
                    ErrorMessage="*" EnableClientScript="true" runat="server" ValidationGroup="Save"></asp:RequiredFieldValidator> 
            </td>
        </tr>
        <tr>
            <td>Mức độ uỷ quyền</td>
            <td><asp:DropDownList ID="ddlLevel" runat="server">
                <asp:ListItem Value="1">To&#224;n bộ c&#244;ng văn</asp:ListItem>
                <asp:ListItem Value="2">C&#244;ng văn mật</asp:ListItem>
                <asp:ListItem Value="3">C&#244;ng văn b&#236;nh thường</asp:ListItem>
            </asp:DropDownList></td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <hr />
                <asp:Button ID="cmdSave" runat="server" Text="Lưu" ValidationGroup="Save"/>
                <asp:Button ID="cmdExit" runat="server" Text="Trở về" />
            </td>
        </tr>
    </table>
</asp:Content>
