﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Template.Master"
    CodeFile="PostNews.aspx.vb" Inherits="PostNews" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="componentheading">Đăng bài</div>
    <telerik:RadTabStrip ID="RadTabStrip1" runat="server" Skin="Vista" MultiPageID="RadMultiPage1"
        SelectedIndex="0">
        <Tabs>
            <telerik:RadTab Text="Nội dung">
            </telerik:RadTab>
            <telerik:RadTab Text="File kèm">
            </telerik:RadTab>
        </Tabs>
    </telerik:RadTabStrip>
    <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="0" CssClass="multiPage">
        <telerik:RadPageView ID="RadPageView1" runat="server" >
            <table cellpadding="2" cellspacing="0" border="0" width="100%">
                <colgroup>
                    <col width="60" />
                    <col />
                </colgroup>
                <tr>
                    <td>Phân loại</td>
                    <td>
                        <asp:DropDownList ID="ddlType" runat="server" DataValueField="id" DataTextField="name"></asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>Tiêu đề</td>
                    <td>
                        <asp:TextBox ID="txtSubject" runat="server" Width="600px" CssClass="inputfield"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvSubject" Runat="server" Display="Dynamic" ControlToValidate="txtSubject" ErrorMessage="*" ValidationGroup="Post" />
                    </td>
                </tr>
                <tr>
                    <td valign="top">Trích yếu</td>
                    <td>
                        <asp:TextBox ID="txtDescription" runat="server" Height="56px" TextMode="MultiLine" Width="600px" CssClass="inputfield"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td valign="top">Nội dung</td>
                    <td>
                        <telerik:RadEditor ID="redtBody" runat="server" Height="300px" Width="100%" ToolsFile="EdtToolDefault.xml" EditModes="Design" AutoResizeHeight="false"> 
                            <ImageManager ViewPaths="~/News" UploadPaths="~/News" DeletePaths="~/News" MaxUploadFileSize = "4096000"/>
                        </telerik:RadEditor>
                    </td>
                </tr>
            </table>
        </telerik:RadPageView>
        <telerik:RadPageView ID="RadPageView2" runat="server">
            <div style="padding:20px">
                <telerik:RadUpload ID="RadUpload1" runat="server" InitialFileInputsCount="5"
                    EnableFileInputSkinning="true" InputSize="50" Width="600px" />
            </div>
        </telerik:RadPageView>
    </telerik:RadMultiPage>
    <hr />
    <asp:Button ID="btnSubmit" runat="server" Text="Cập nhật" ValidationGroup="Post"/>
</asp:Content>
