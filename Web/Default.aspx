﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Template.Master"
    CodeFile="Default.aspx.vb" Inherits="_Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <%--<div class="componentheading">Tin tức - Sự kiện</div>
    <div class="news">
        <asp:DataList ID="DataList1" DataKeyField="id" runat="server" Width="100%" OnItemDataBound="DataList1_ItemDataBound">
            <ItemTemplate>
                <div class="box">
                    <div class="box-title">
                        <asp:HyperLink ID="HyperLink1" Text='<%# Eval("Name") %>' NavigateUrl='<%# Eval("Id", "~/BrowseNews.aspx?id={0}") %>'
                            runat="server" CssClass="tcat"></asp:HyperLink>
                    </div>
                    <div class="box-body clearfix">
                        <telerik:RadGrid ID="GridView" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                            BorderWidth="0" GridLines="None" ShowHeader="false" Width="100%" Skin="Web20">
                            <MasterTableView>
                                <NoRecordsTemplate>&nbsp;</NoRecordsTemplate>
                                <Columns>
                                    <telerik:GridTemplateColumn>
                                        <ItemTemplate><asp:HyperLink ID="HyperLink1" Text='<%# Eval("Subject") %>' NavigateUrl='<%# Eval("Id", "~/News.aspx?id={0}") %>'
                                                runat="server" CssClass="thead"></asp:HyperLink></ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                            </MasterTableView>
                        </telerik:RadGrid>
                    </div>
                </div>
            </ItemTemplate>
        </asp:DataList>
        <div style="padding:5px"></div>
        <table border="0" cellspacing="0" cellpadding="0" width="99%">
            <tr>
                <td valign="top">
                    <div class="box-left">
                        <div class="box-title">Tin đọc nhiều nhất</div>
                        <div class="box-body">
                            <telerik:RadGrid ID="grdRead" runat="server" AllowPaging="True" AutoGenerateColumns="False" Skin="Web20"
                                BorderWidth="0" GridLines="None" ShowHeader="false" OnNeedDataSource="grdRead_NeedDataSource">
                                <MasterTableView>
                                    <NoRecordsTemplate>&nbsp;</NoRecordsTemplate>
                                    <Columns>
                                        <telerik:GridTemplateColumn>
                                            <ItemTemplate>
                                                <asp:HyperLink ID="HyperLink1" Text='<%# Eval("Subject") & " (" & Eval("viewcount") & ")" %>' NavigateUrl='<%# Eval("Id", "~/News.aspx?id={0}") %>' runat="server" CssClass="thead1"></asp:HyperLink>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                    </Columns>
                                </MasterTableView>
                            </telerik:RadGrid>
                        </div>
                    </div>
                </td>
                <td style="width:10px"></td>
                <td valign="top">
                    <div class="box-right">
                        <div class="box-title">Tin mới nhận <asp:Image ID="Image1" ImageUrl="~/Styles/Images/hot.gif" runat="server" /></div>
                        <div class="box-body">
                            <telerik:RadGrid ID="grdLatest" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                BorderWidth="0" GridLines="None" ShowHeader="false" OnNeedDataSource="grdLatest_NeedDataSource" Skin="Web20">
                                <MasterTableView>
                                    <NoRecordsTemplate>&nbsp;</NoRecordsTemplate>
                                    <Columns>
                                        <telerik:GridTemplateColumn>
                                            <ItemTemplate>
                                                <asp:HyperLink ID="HyperLink1" Text='<%# Eval("Subject") %>' NavigateUrl='<%# Eval("Id", "~/News.aspx?id={0}") %>'
                                                        runat="server" CssClass="thead1"></asp:HyperLink>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                    </Columns>
                                </MasterTableView>
                            </telerik:RadGrid>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </div>--%>
</asp:Content>
