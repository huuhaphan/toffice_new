﻿<%@ Page Language="vb" AutoEventWireup="false" CodeFile="ChooseUser.aspx.vb" Inherits="ChooseUser" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Danh sach nhom/nguoi nhan</title>
    <link rel="stylesheet" href="styles/default.css" />
    <style type="text/css">
        body
        {
            margin: 5px;
            background-color: #F0F5FB;
        }
        .inputfield
        {
            width: 99%;
        }
        #content .header
        {
            margin-left: 5px;
            font-weight: bold;
            text-decoration: underline;
        }
        #content .h2
        {
            margin-left: 10px;
        }
        hr
        {
            padding: 0px;
        }
        p
        {
            margin: 3px;
        }
        #btnSearch
        {
            margin: 0;
        }
        input.search
        {
            background-image: url(images/search.gif);
            background-position: left 1px;
            background-repeat: no-repeat;
            padding-left: 18px;
            height: 17px;
        }

        input.rfdDecorated
        {
            font-family: Arial,Helvetica,sans-serif;
            font-size: 9pt;
        }

    </style>
</head>
<body onload="ConfigureDialog();">
    <form id="form1" runat="server">
    <telerik:RadScriptManager runat="server" ID="RadScriptManager1" />
    <telerik:RadFormDecorator runat="server" ID="RadFormDecorator1" DecoratedControls="All" />
    <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">

        <script type="text/javascript">
            function remove(sender) {
                sender.parentNode.removeChild(sender);
            }

            function setOnClick() {
                if (!document.getElementsByTagName('a')) {
                    return;
                }
                var anchors = document.getElementsByTagName('a');
                for (var i = 0; i < anchors.length; i++) {
                    var anchor = anchors[i];
                    var relAttribute = String(anchor.getAttribute('rel')).substring(0, 1);
                    if (relAttribute == "%") {
                        anchor.onclick = function() {
                            remove(this);
                        }
                    }
                }
            }

            function addElement(str) {
                var fld = "";
                if (str == "RC") {
                    fld = "<%= lblTo.ClientID %>";
                }
                else if (str == "CC") {
                    fld = "<%= lblCc.ClientID %>";
                }
                else {
                    fld = "<%= lblBc.ClientID %>";
                }

                var obj = $get(fld);
                var grid = $find("<%= RadGrid1.ClientID %>");
                var mastertable = grid.get_masterTableView();
                var rows = mastertable.get_selectedItems();

                for (var i = 0; i < rows.length; i++) {
                    var row = rows[i];
                    var cname = mastertable.getCellByColumnUniqueName(row, "Name").innerHTML;
                    var cvalue = row.getDataKeyValue("Value");
                    var anchor = document.createElement("a");
                    anchor.rel = "%" + cvalue + "%";
                    anchor.innerHTML = cname + ";";
                    anchor.href = "javascript:void(0)";
                    anchor.onclick = function() {
                        javascript: remove(this);
                    }
                    obj.appendChild(anchor);
                    row.set_selected("false");
                }
            }

            function ClearField(str) {
                var fld = ""
                if (str == "RC") {
                    fld = "<%= lblTo.ClientID %>"
                }
                else if (str == "CC") {
                    fld = "<%= lblCc.ClientID %>"
                }
                else {
                    fld = "<%= lblBc.ClientID %>"
                }
                var obj = $get(fld)
                obj.innerHTML = "";
            }

            function ConfigureDialog() {
                // Get a reference to the radWindow wrapper
                var oWindow = GetRadWindow();
                if (oWindow) {
                    var oArg = oWindow.Argument;
                    $get("<%= lblTo.ClientID %>").innerHTML = oArg.tovalue;
                    $get("<%= lblCc.ClientID %>").innerHTML = oArg.ccvalue;
                    $get("<%= lblBc.ClientID %>").innerHTML = oArg.bcvalue;
                    setOnClick();
                }
            }

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement) {
                    oWindow = window.frameElement.radWindow;
                }
                return oWindow;
            }

            function CloseWindow(status) {
                var oWindow = GetRadWindow();
                if (oWindow) {
                    if (status == "OK") {
                        var oArg = new Object();
                        oArg.tovalue = $get("<%= lblTo.ClientID %>").innerHTML;
                        oArg.ccvalue = $get("<%= lblCc.ClientID %>").innerHTML;
                        oArg.bcvalue = $get("<%= lblBc.ClientID %>").innerHTML;
                        oWindow.Argument = oArg;
                    }
                    oWindow.close();
                }
                else {
                    window.close();
                }
            }
        </script>

    </telerik:RadScriptBlock>
    <div style="padding-bottom: 5px; height: 25px">
        <input type="button" value="Chấp nhận" style="width: 85px" onclick="CloseWindow('OK');" />
        <input type="button" value="Quay về" style="width: 85px" onclick="CloseWindow('XX');" />
    </div>
    <table cellpadding="0" cellspacing="0" style="border: solid 1px #6c9bd1;" width="100%">
        <colgroup>
            <col width="200px" />
            <col />
        </colgroup>
        <tr>
            <td style="border-right: solid 1px #6c9bd1;" valign="top">
                <div id="content">
                    <p class="header">Danh sách gửi</p>
                    <asp:RadioButtonList ID="rblUser" runat="server" CssClass="h2" AutoPostBack="True"
                        BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" Width="190px">
                        <asp:ListItem Value="A">Tất cả</asp:ListItem>
                        <asp:ListItem Value="G">Nh&#243;m</asp:ListItem>
                        <asp:ListItem Value="U">Người sử dụng</asp:ListItem>
                    </asp:RadioButtonList>
                    <div style="padding-top: 5px">
                        <asp:CheckBox ID="chkDisplay" runat="server" Text="Hiển thị toàn bộ danh sách trên cùng trang"
                            AutoPostBack="True" CssClass="h2" Width="190px" />
                    </div>
                    <hr />
                    <p class="header" style="">Tìm kiếm:</p>
                    <asp:RadioButtonList ID="rblSearchFor" runat="server" CssClass="h2" AutoPostBack="True" EnableViewState="true"
                        BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" Width="190px">
                        <asp:ListItem Value="L">T&#234;n nhân viên</asp:ListItem>
                        <asp:ListItem Value="F">Kh&#225;c</asp:ListItem>
                    </asp:RadioButtonList>
                    <div style="padding:10px">
                        <table cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td><telerik:RadTextBox ID="txtSearch" Runat="server" Width="120px" AutoPostBack="true" EmptyMessage="Nhập tên" /></td>
                                <td><asp:Button ID="btnSearch" runat="server" type="submit" Text="Tìm" Width="40px" /></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </td>
            <td valign="top" style="height: 300px">
                <telerik:RadGrid ID="RadGrid1" runat="server" Width="100%" Height="295px" BorderWidth="0"
                    AutoGenerateColumns="False" PageSize="20" OnNeedDataSource="RadGrid1_NeedDataSource"
                    GridLines="None" AllowPaging="True" AllowMultiRowSelection="True" AllowSorting="True"
                    EnableLinqExpressions="false">
                    <PagerStyle NextPagesToolTip="" NextPageToolTip="Trang kế" PagerTextFormat="Trang {0}/{1} {4}"
                        PrevPagesToolTip="" PrevPageToolTip="Trang trước" />
                    <MasterTableView AdditionalDataFieldNames="Type,Lastname,Name" ClientDataKeyNames="Value"
                        TableLayout="Fixed">
                        <Columns>
                            <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn">
                                <HeaderStyle Width="30px" />
                            </telerik:GridClientSelectColumn>
                            <telerik:GridTemplateColumn UniqueName="Users" Groupable="False">
                                <ItemTemplate>
                                    <%#IIf(Eval("Type") = "G", "<img src='images/users.png'>", "<img src='images/user.png'>")%>
                                </ItemTemplate>
                                <HeaderStyle Width="30px" />
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn SortExpression="Name" HeaderText="T&#234;n" DataField="Name"
                                UniqueName="Name" />
                            <telerik:GridBoundColumn SortExpression="Organization" HeaderText="Đơn vị" DataField="Organization"
                                UniqueName="Organization" />
                        </Columns>
                    </MasterTableView>
                    <ClientSettings ReorderColumnsOnClient="True" AllowColumnsReorder="True" EnableRowHoverStyle="True">
                        <Selecting AllowRowSelect="True"></Selecting>
                        <Scrolling AllowScroll="True" UseStaticHeaders="True"></Scrolling>
                    </ClientSettings>
                </telerik:RadGrid>
            </td>
        </tr>
    </table>
    <p>Danh sách người nhận</p>
    <table cellpadding="0" cellspacing="3" border="0" width="100%">
        <colgroup>
            <col width="80px" />
            <col />
            <col width="60px" />
        </colgroup>
        <tr>
            <td valign="top">
                <input id="btnTo" type="button" value="Người nhận" style="width: 85px" onclick="addElement('RC');" />
            </td>
            <td>
                <div class="inputfield">
                    <asp:Label ID="lblTo" runat="server"></asp:Label></div>
            </td>
            <td valign="top">
                <input type="button" value="Xóa hết" style="width: 85px" onclick="ClearField('RC');" />
            </td>
        </tr>
        <tr>
            <td valign="top">
                <input id="btnCc" type="button" value="Đồng nhận" style="width: 85px" onclick="addElement('CC');" />
            </td>
            <td>
                <div class="inputfield">
                    <asp:Label ID="lblCc" runat="server"></asp:Label></div>
            </td>
            <td valign="top">
                <input type="button" value="Xóa hết" style="width: 85px" onclick="ClearField('CC');" />
            </td>
        </tr>
        <tr>
            <td valign="top">
                <input id="btnBC" type="button" value="Đồng nhận#" style="width: 85px" onclick="addElement('BC');" />
            </td>
            <td>
                <div class="inputfield">
                    <asp:Label ID="lblBc" runat="server"></asp:Label></div>
            </td>
            <td valign="top">
                <input type="button" value="Xóa hết" style="width: 85px" onclick="ClearField('BC');" />
            </td>
        </tr>
    </table>
    <hr />
    <div style="height: 25px">
        <input type="button" value="Chấp nhận" style="width: 85px" onclick="CloseWindow('OK');" />
        <input type="button" value="Quay về" style="width: 85px" onclick="CloseWindow('XX');" />
    </div>

    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" DefaultLoadingPanelID="LoadingPanel1">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="rblUser">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rblUser" LoadingPanelID="NotDefined"  />
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="txtSearch">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rblSearchFor">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rblSearchFor" LoadingPanelID="NotDefined"  />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnSearch">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="chkDisplay">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server" />
    </form>
</body>
</html>
