
Partial Class Statistic
    Inherits SecurePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim inbox, deleted, draft, sent, privatefolder, total As Long
            inbox = Mails.Statistic("IN")
            draft = Mails.Statistic("DR")
            deleted = Mails.Statistic("DL")
            sent = Mails.Statistic("SD")
            privatefolder = Mails.Statistic("PF")
            total = sent + deleted + draft + inbox + privatefolder

            lblInbox.Text = Mails.ToByteString(inbox)
            lblDraft.Text = Mails.ToByteString(draft)
            lblDeleted.Text = Mails.ToByteString(deleted)
            lblSent.Text = Mails.ToByteString(sent)
            lblPrivateFolder.Text = Mails.ToByteString(privatefolder)

            Dim style As String = "<span style='font-weight:bold'>"
            Const megabyte As Integer = 1024 * 1024
            If Int(total / megabyte) > Common.GetSizeLimit(Enums.MailSize.MailBox) Then
                style = "<span style='color:red;font-weight:bold'>"
            End If
            lblSum.Text = "Tổng dung lượng đã sử dụng là : " + style + Mails.ToByteString(total) + "</span>"
        End If
    End Sub
End Class
