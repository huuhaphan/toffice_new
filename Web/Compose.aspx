﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/TemplateFull.Master"
    ValidateRequest="false" CodeFile="Compose.aspx.vb" Inherits="Compose" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            var labelTo, labelCc, labelBc

            //Page Load
            function pageLoad() {
                var xmlPanel = $find("<%= RadNotification1.ClientID %>")._xmlPanel;
                xmlPanel.set_enableClientScriptEvaluation(true);

                labelTo = $get("<%= lblTo.ClientID %>")
                labelCc = $get("<%= lblCc.ClientID %>")
                labelBc = $get("<%= lblBc.ClientID %>")
                setOnClick()
            }

            //show the window
            function showDialog() {
                var oWnd = window.radopen(null, "DialogWindow");
                oWnd.center();
            }

            function OnClientshow(radWindow) {
                //Create a new Object to be used as an argument to the radWindow
                var arg = new Object();
                //Using an Object as a argument is convenient as it allows setting many properties.
                arg.tovalue = labelTo.innerHTML;
                arg.ccvalue = labelCc.innerHTML;
                arg.bcvalue = labelBc.innerHTML;
                //Set the argument object to the radWindow
                radWindow.Argument = arg;
            }

            function OnClientClose(radWindow) {
                arg = radWindow.Argument;
                labelTo.innerHTML = arg.tovalue;
                labelCc.innerHTML = arg.ccvalue;
                labelBc.innerHTML = arg.bcvalue;
                $get("<%= hdn.ClientID %>").value = arg.tovalue + "#" + arg.ccvalue + "#" + arg.bcvalue + "#"
                setOnClick()
            }

            function remove(sender) {
                sender.parentNode.removeChild(sender);
                $get("<%= hdn.ClientID %>").value = labelTo.innerHTML + "#" + labelCc.innerHTML + "#" + labelBc.innerHTML + "#";
            }

            function setOnClick() {
                if (!document.getElementsByTagName('a')) { return; }
                var anchors = document.getElementsByTagName('a');
                for (var i = 0; i < anchors.length; i++) {
                    var anchor = anchors[i];
                    var relAttribute = String(anchor.getAttribute('rel')).substring(0, 1);
                    if (relAttribute == "%") {
                        anchor.onclick = function () {
                            remove(this);
                        }
                    }
                }
            }

            function TabSelected(sender, args) {
                if (sender.selectedIndex = 1) {
                    var icount = 0;
                    var upload = $find("<%= RadAsyncUpload1.ClientID %>");
                    var inputs = upload.getUploadedFiles();
                    for (i = inputs.length - 1; i >= 0; i--) {
                        if (inputs[i].value != "") { icount += 1 }
                        if (!upload.isExtensionValid(inputs[i].value))
                            upload.deleteFileInputAt(i);
                    }
                    if (icount > 0) { sender.get_tabs().getTab(1).set_text("File kèm (" + icount + ')') }
                }
            }

            function OnClientNotificationShowing(sender, eventArgs) {
                sender.update();
                sender.hide();
            }

        </script>
    </telerik:RadCodeBlock>
    <telerik:RadToolBar ID="RadToolBar1" runat="server" OnButtonClick="RadToolBar1_ButtonClick"
        Width="100%">
        <Items>
            <telerik:RadToolBarButton ImageUrl="~/images/send.gif" Text="Gửi thư" ToolTip="Gửi thư"
                Value="Send" runat="server" ValidationGroup="POST" />
            <telerik:RadToolBarButton ImageUrl="~/images/save.gif" Text="Lưu thư" ToolTip="Lưu thư"
                Value="Save" runat="server" ValidationGroup="POST" />
            <telerik:RadToolBarButton IsSeparator="True" runat="server" />
            <telerik:RadToolBarButton ImageUrl="~/images/print.gif" Value="Print" ToolTip="In"
                runat="server" />
        </Items>
    </telerik:RadToolBar>
    <asp:Panel ID="TipTextDiv" runat="server" Visible="false">
        <div class="TipTextFailure">
            <asp:Label ID="lblError" runat="server"></asp:Label></div>
    </asp:Panel>
    <div style="padding-top: 3px" />
    <telerik:RadTabStrip ID="RadTabStrip1" runat="server" MultiPageID="RadMultiPage1"
        SelectedIndex="0" OnClientTabSelected="TabSelected">
        <Tabs>
            <telerik:RadTab Text="Nội dung" />
            <telerik:RadTab Text="File kèm" />
        </Tabs>
    </telerik:RadTabStrip>
    <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="0" CssClass="multiPage">
        <telerik:RadPageView ID="RadPageView1" runat="server" CssClass="message" BackColor="transparent">
            <table cellpadding="0" cellspacing="3" border="0" width="100%" style="table-layout: fixed">
                <colgroup>
                    <col width="90" />
                    <col />
                    <col width="60" />
                </colgroup>
                <tr>
                    <td valign="top">
                        <input id="btnTo" type="button" value="Người nhận" style="width: 85px" onclick="showDialog();return false;" />
                    </td>
                    <td>
                        <div class="inputfield"><asp:Label ID="lblTo" runat="server"></asp:Label></div>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <input id="btnCc" type="button" value="Đồng nhận" style="width: 85px" onclick="showDialog();return false;" />
                    </td>
                    <td>
                        <div class="inputfield">
                            <asp:Label ID="lblCc" runat="server"></asp:Label></div>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <input id="btnBcc" type="button" value="Đồng nhận#" style="width: 85px" onclick="showDialog();return false;" />
                    </td>
                    <td>
                        <div class="inputfield">
                            <asp:Label ID="lblBc" runat="server"></asp:Label></div>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <th>
                        Chủ đề <span class="marker">(*)</span>:
                    </th>
                    <td>
                        <asp:TextBox ID="txtSubject" runat="server" CssClass="inputField" Width="99%"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="rfvSubject" runat="server" Display="Dynamic" ControlToValidate="txtSubject"
                            ErrorMessage="*" ValidationGroup="POST" SetFocusOnError="true" CssClass="validation" />
                    </td>
                </tr>
                <tr>
                    <th valign="top">
                        Đính kèm:
                    </th>
                    <td>
                        <div class="inputfield">
                            <asp:Label ID="lblAttachment" runat="server"></asp:Label></div>
                    </td>
                    <td>
                        <asp:CheckBox ID="Important" Text="Khẩn" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="margin-top: 10px" class="composewindow">
                        <telerik:RadEditor ID="redtBody" runat="server" Height="300px" Width="100%" ToolsFile="EdtToolDefault.xml"
                            EditModes="Design" AutoResizeHeight="false">
                        </telerik:RadEditor>
                    </td>
                </tr>
            </table>
        </telerik:RadPageView>
        <telerik:RadPageView ID="RadPageView2" runat="server">
            <div style="min-height:400px" >
                <h3><asp:Label runat="server" ID="lblHeader" /></h3>
                <telerik:RadProgressManager ID="Radprogressmanager1" runat="server" />
                <telerik:RadAsyncUpload runat="server" ID="RadAsyncUpload1" OnFileUploaded="RadAsyncUpload1_FileUploaded"
                    MultipleFileSelection="Automatic" Culture="vi-VN">
                    <Localization Cancel="Hủy chọn" Remove="Xóa File" Select="Chọn File" />
                </telerik:RadAsyncUpload>
                <telerik:RadProgressArea runat="server" ID="RadProgressArea1">
                </telerik:RadProgressArea>
            </div>
        </telerik:RadPageView>
    </telerik:RadMultiPage>
    <telerik:RadWindowManager ID="Singleton" runat="server">
        <Windows>
            <telerik:RadWindow ID="DialogWindow" Behaviors="Close, Move" ReloadOnShow="true"
                OnClientClose="OnClientClose" OnClientShow="OnClientshow" Modal="true" runat="server"
                Width="800px" Height="560px" NavigateUrl="ChooseUser.aspx" VisibleStatusbar="false"
                ShowContentDuringLoad="false">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <telerik:RadNotification ID="RadNotification1" runat="server" Position="Center" Width="240"
        Height="100" LoadContentOn="PageLoad" AutoCloseDelay="1500" Title="Cập nhật"
        Text="<strong>Thêm thời gian chờ để nhập!</strong>" TitleIcon="" EnableRoundedCorners="true"
        ShowCloseButton="false" OnClientShowing="OnClientNotificationShowing">
    </telerik:RadNotification>
    <input type="hidden" id="hdn" runat="server" />
    <input type="hidden" id="LimitSize" runat="server" />
    <script type="text/javascript">
		//<![CDATA[

        Telerik.Web.UI.Editor.CommandList["InsertEmoticon"] = function (commandName, editor, args) {
            var myCallbackFunction = function (sender, args) {
                editor.pasteHtml(String.format("<img src='{0}' border='0' alt='emoticon' /> ", args.image));
            }
            editor.showExternalDialog('InsertEmoticon.aspx', {}, 400, 310, myCallbackFunction, null, 'Insert Emoticon', true, Telerik.Web.UI.WindowBehaviors.Close + Telerik.Web.UI.WindowBehaviors.Move, false, true);
        };
		//]]>
    </script>
</asp:Content>
