﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/InnerTemplate.Master"
    Inherits="BrowseDocument" CodeFile="BrowseDocument.aspx.vb" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">

        <script type="text/javascript">
            //<![CDATA[

            var msgid;
            var postid;
            var grid;
            var ajaxManager;
            var alertTimerId = 0;
            var dataItem;
            var msgidover;
            var postidover;

            function pageLoad() {
                grid = $find("<%=RadGrid1.ClientID %>");
                ajaxManager = $find("<%=RadAjaxManager1.ClientID %>");
            }


            function RowSelected(sender, args) {
                clearTimeout(alertTimerId);
                msgid = args.getDataKeyValue("Source_Id");
                postid = args.getDataKeyValue("PersonalBox_Id");

                if (grid.get_masterTableView().get_selectedItems().length == 1) {
                    ajaxManager.ajaxRequest("ShowDetail:" + msgid);
                    dataItem = grid.get_masterTableView().get_dataItems()[args.get_itemIndexHierarchical()];
                    if (dataItem.get_element() != undefined && dataItem.get_element().style.fontWeight == "bold") {
                        alertTimerId = setTimeout("SetMarkRead()", 5000);
                    }
                }
            }

            function RowMouseOver(sender, args) {
                msgidover = args.getDataKeyValue("Source_Id");
                postidover = args.getDataKeyValue("PersonalBox_Id");
            }

            function RowDblClick(sender, args) {
                openWindow("viewdoc.aspx?id=" + msgid + "&postid=" + postid);
            }

            function SetMarkRead() {
                if (dataItem.get_element() != undefined) {
                    dataItem.get_element().style.fontWeight = "Normal";
                    ajaxManager.ajaxRequest("SetRead:" + postid);
                }
            }

            function ToolbarClicking(sender, args) {
                var item = args.get_item();
                var itemvalue = args.get_item().get_value();
                if (itemvalue == "Forward") {
                    if (grid.get_masterTableView().get_selectedItems().length == 0) {
                        alert("Bạn phải chọn công văn !");
                        args.set_cancel(true);
                    }
                    else {
                        item.set_navigateUrl(getUrl("compose.aspx", "DO", itemvalue, msgid));
                    }
                }
                else if (itemvalue == "Delete") {
                    if (!confirm("Bạn thực sự muốn xóa tất cả các công văn đã chọn ?")) { args.set_cancel(true); }
                }
            }

            function RowContextMenu(sender, args) {
                var menu = $find("<%=RadMenu1.ClientID %>");
                var evt = args.get_domEvent();
                menu.show(evt);

                evt.cancelBubble = true;
                evt.returnValue = false;

                if (evt.stopPropagation) {
                    evt.stopPropagation();
                    evt.preventDefault();
                }
            }

            function ContextMenuClicking(sender, args) {
                var item = args.get_item();
                var itemvalue = item.get_value();
                if (itemvalue == "Forward") { item.set_navigateUrl(getUrl("compose.aspx", itemvalue, msgidover)); }
                else if (itemvalue == "Open") {
                    openWindow("viewdoc.aspx?id=" + msgidover + "&postid=" + postidover);
                }
                else if (itemvalue == "Delete") {
                    if (!confirm("Bạn thực sự muốn xóa công văn này ?")) { args.set_cancel(true); }
                }
            }

            function GridKeyPressed(sender, eventArgs) {
                if (eventArgs.get_keyCode() == 40 || eventArgs.get_keyCode() == 38) { eventArgs.set_cancel(false) }
                else { eventArgs.set_cancel(true) }
            }
            //]]>
        </script>

    </telerik:RadCodeBlock>
    <telerik:RadSplitter ID="RadSplitter1" runat="server" Width="100%" Orientation="Horizontal"
        Height="100%" BorderStyle="None" PanesBorderSize="0">
        <telerik:RadPane ID="ToolbarPane" runat="server" Height="30px" Scrolling="None">
            <telerik:RadToolBar ID="RadToolBar1" runat="server" OnButtonClick="RadToolBar1_ButtonClick"
                OnClientButtonClicking="ToolbarClicking" CssClass="toolbar">
                <Items>
                    <telerik:RadToolBarButton ImageUrl="~/images/checkmail.gif" ToolTip="Nhận c&#244;ng văn mới"
                        Value="Check" runat="server" />
                    <telerik:RadToolBarButton IsSeparator="True" />
                    <telerik:RadToolBarButton ImageUrl="~/images/delete.gif" Value="Delete" ToolTip="X&#243;a c&#244;ng văn"
                        runat="server" />
                    <telerik:RadToolBarButton IsSeparator="True" />
                    <%--<telerik:RadToolBarButton ImageUrl="~/images/forward.gif" Text="Chuyển tiếp" Target="_blank"
                        NavigateUrl="#" Value="Forward" PostBack="False" runat="server" />--%>
                    <telerik:RadToolBarDropDown ImageUrl="~/images/movetofolder.gif" Text="Thư mục" ToolTip="Di chuyển đến thư mục"
                        runat="server" />
                </Items>
            </telerik:RadToolBar>
        </telerik:RadPane>
        <telerik:RadPane ID="GridPane" runat="server" Scrolling="None">
            <telerik:RadGrid ID="RadGrid1" runat="server" Height="100%" AllowSorting="True" AutoGenerateColumns="False"
                OnNeedDataSource="RadGrid1_NeedDataSource" BorderWidth="0" GridLines="None" ShowGroupPanel="True"
                AllowMultiRowSelection="True" AllowPaging="true" PageSize="20">
                <MasterTableView DataKeyNames="PersonalBox_Id,Seen" ClientDataKeyNames="PersonalBox_Id,Source_Id"
                    GroupLoadMode="Client">
                    <NoRecordsTemplate>
                        <div class="NoRecord">Không có công văn trong thư mục này</div>
                    </NoRecordsTemplate>
                    <Columns>
                        <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn" HeaderStyle-Width="27px" />
                        <telerik:GridTemplateColumn UniqueName="Important" HeaderText="" Groupable="False"
                            HeaderStyle-Width="27px">
                            <ItemTemplate>
                                <%# IIF(Eval("Important")="X","<img src='images/important.gif'>","&nbsp;") %>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn SortExpression="DocNum" HeaderText="Số CV" DataField="DocNum"
                            UniqueName="DocNum" HeaderStyle-Width="70px" >
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="Content" HeaderText="Chủ đề" DataField="Content"
                            UniqueName="Content" HeaderStyle-Width="200px">
                            <ItemStyle CssClass="hyperlink" /> 
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="Subject" HeaderText="Bút phê" DataField="Subject"
                            UniqueName="Subject" HeaderStyle-Width="160px">
                            <ItemStyle CssClass="hyperlink" /> 
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="Sendtime" HeaderText="Ngày đến" DataField="Sendtime"
                            DataFormatString="{0:dd/MM/yyyy}" UniqueName="Sendtime" HeaderStyle-Width="65px">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="ThoigianHoanthanh" HeaderText="TH hoàn thành" DataField="ThoigianHoanthanh"
                            DataFormatString="{0:dd/MM/yyyy}" UniqueName="ThoigianHoanthanh" HeaderStyle-Width="95px">
                        </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn UniqueName="Attach" HeaderText="" Groupable="False" HeaderStyle-Width="27px">
                            <ItemTemplate>
                                <%# IIF(Eval("AttachFile")="X","<img src='images/Attch.gif'>","&nbsp;") %>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                    </Columns>
                </MasterTableView>
                <ClientSettings AllowKeyboardNavigation="true" ClientEvents-OnKeyPress="GridKeyPressed" ReorderColumnsOnClient="True" 
                    AllowDragToGroup="True" AllowColumnsReorder="True" EnableRowHoverStyle="true">
                    <Selecting AllowRowSelect="True"></Selecting>
                    <Scrolling AllowScroll="True" UseStaticHeaders="True"></Scrolling>
                    <ClientEvents OnRowContextMenu="RowContextMenu" OnRowSelected="RowSelected" OnRowDblClick="RowDblClick"
                        OnRowMouseOver="RowMouseOver" />
                </ClientSettings>
                <GroupPanel Text="Nh&#243;m theo t&#234;n cột" ToolTip="Đưa con trỏ v&#224;o &#244; t&#234;n cột, nhấn v&#224; giữ nguy&#234;n n&#250;t tr&#225;i chuột sau đ&#243; k&#233;o v&#224; thả v&#224;o &#244; n&#224;y" />
                <SortingSettings SortToolTip="Nhấn vào đây để sắp xếp"></SortingSettings>
                <GroupingSettings ShowUnGroupButton="true" />
                <PagerStyle Mode="NextPrevAndNumeric" PageSizeLabelText="Số công văn hiển thị" NextPagesToolTip=""
                    NextPageToolTip="Trang kế" PageButtonCount="5" PagerTextFormat="Trang {0}/{1} {4} Tổng cộng {5} công văn"
                    PrevPagesToolTip="" PrevPageToolTip="Trang trước" />
            </telerik:RadGrid>
        </telerik:RadPane>
        <telerik:RadSplitBar ID="RadSplitBar1" runat="server" CollapseMode="Forward" />
        <telerik:RadPane ID="ContentPane" runat="server" Height="150px">
            <asp:Panel ID="MessagePanel" runat="server">
                <div class="messagetitle">
                    <asp:Label ID="lblSubject" runat="server" Font-Bold="true" />
                    <br />
                    <asp:Label ID="lblDocNum" runat="server" Font-Bold="true" />
                    <asp:Label ID="lblDate" runat="server" CssClass="rightcol" />
                    <br />
                    <asp:Label ID="lblTo" runat="server" />
                    <br />
                    <asp:Label ID="lblAttachment" runat="server" />
                </div>
                <div class="messagebody">
                    <asp:Label ID="lblBody" runat="server" />
                </div>
            </asp:Panel>
        </telerik:RadPane>
    </telerik:RadSplitter>
    <telerik:RadAjaxManager ID="RadAjaxManager1" DefaultLoadingPanelID="LoadingPanel1"
        OnAjaxRequest="RadAjaxManager1_AjaxRequest" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="MessagePanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadMenu1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadToolBar1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server" />
    <telerik:RadContextMenu ID="RadMenu1" runat="server" OnItemClick="RadMenu1_ItemClick"
        OnClientItemClicking="ContextMenuClicking">
        <Items>
            <telerik:RadMenuItem Text="Xem công văn" runat="server" Value="Open" NavigateUrl="#"
                Target="_blank" PostBack="false" />
            <telerik:RadMenuItem IsSeparator="true" />
            <telerik:RadMenuItem Text="Chuyển tiếp" runat="server" ImageUrl="images/forward.gif"
                Value="Forward" NavigateUrl="#" Target="_blank" PostBack="false" />
            <telerik:RadMenuItem Text="Xóa công văn" runat="server" Value="Delete" />
            <telerik:RadMenuItem IsSeparator="true" />
            <telerik:RadMenuItem Text="Đánh dấu đã đọc" runat="server" Value="MarkRead" />
            <telerik:RadMenuItem Text="Đánh dấu chưa đọc" runat="server" Value="MarkUnRead" />
            <telerik:RadMenuItem IsSeparator="true" />
            <telerik:RadMenuItem Text="Xóa tất cả công văn" runat="server" Value="DeleteAll" />
        </Items>
        <CollapseAnimation Duration="200" Type="OutQuint" />
    </telerik:RadContextMenu>
</asp:Content>
