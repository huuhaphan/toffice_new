﻿
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared

Partial Class Schedule
    Inherits SecurePage
    Dim cryRpt As New ReportDocument
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

            RequestAuthorize()
            RadDateInput1.SelectedDate = DateAndTime.Now
            LoadReport_Calendas()


        End If
    End Sub


    Private Sub LoadReport_Calendas()
        If Not Page.IsPostBack Then


            Dim crtableLogoninfos As New TableLogOnInfos
            Dim crtableLogoninfo As New TableLogOnInfo
            Dim crConnectionInfo As New ConnectionInfo
            Dim CrTables As Tables
            Dim CrTable As CrystalDecisions.CrystalReports.Engine.Table


            cryRpt.Load(Server.MapPath("~\RPT\rptlichtrucban.rpt"))
            Dim datetime = RadDateInput1.SelectedDate.Value
            Dim m = datetime.Month
            Dim y = datetime.Year
            cryRpt.SetParameterValue("m", m)
            cryRpt.SetParameterValue("y", y)

            With crConnectionInfo
                .ServerName = Common.Server '"dbazure.vsoftgroup.com"
                .DatabaseName = Common.DataBase ' "CangVu_Document"
                .UserID = Common.DataBaseUser ' "ucangvu"
                .Password = Common.DataBasePass ' "pcangvu"
            End With

            CrTables = cryRpt.Database.Tables
            For Each CrTable In CrTables
                crtableLogoninfo = CrTable.LogOnInfo
                crtableLogoninfo.ConnectionInfo = crConnectionInfo
                CrTable.ApplyLogOnInfo(crtableLogoninfo)
            Next

            CrystalReportViewer1.ReportSource = cryRpt
            CrystalReportViewer1.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None
            CrystalReportViewer1.SeparatePages = False

        End If
    End Sub



    Protected Sub RadDateInput1_SelectedDateChanged(sender As Object, e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs) Handles RadDateInput1.SelectedDateChanged
        Dim crtableLogoninfos As New TableLogOnInfos
        Dim crtableLogoninfo As New TableLogOnInfo
        Dim crConnectionInfo As New ConnectionInfo
        Dim CrTables As Tables
        Dim CrTable As CrystalDecisions.CrystalReports.Engine.Table

        'CrystalReportViewer1.RefreshReport()

        cryRpt.Load(Server.MapPath("~\RPT\rptlichtrucban.rpt"))
        Dim datetime = RadDateInput1.SelectedDate.Value
        Dim m = datetime.Month
        Dim y = datetime.Year
        cryRpt.SetParameterValue("m", m)
        cryRpt.SetParameterValue("y", y)

        With crConnectionInfo
            .ServerName = Common.Server '"dbazure.vsoftgroup.com"
            .DatabaseName = Common.DataBase ' "CangVu_Document"
            .UserID = Common.DataBaseUser ' "ucangvu"
            .Password = Common.DataBasePass ' "pcangvu"
        End With

        CrTables = cryRpt.Database.Tables
        For Each CrTable In CrTables
            crtableLogoninfo = CrTable.LogOnInfo
            crtableLogoninfo.ConnectionInfo = crConnectionInfo
            CrTable.ApplyLogOnInfo(crtableLogoninfo)
        Next

        CrystalReportViewer1.ReportSource = cryRpt
        CrystalReportViewer1.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None
        CrystalReportViewer1.SeparatePages = False
    End Sub
    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim cryRpt As New ReportDocument
        Dim crtableLogoninfos As New TableLogOnInfos
        Dim crtableLogoninfo As New TableLogOnInfo
        Dim crConnectionInfo As New ConnectionInfo
        Dim CrTables As Tables
        Dim CrTable As CrystalDecisions.CrystalReports.Engine.Table

        'CrystalReportViewer1.RefreshReport()

        cryRpt.Load(Server.MapPath("~\RPT\rptlichtrucban.rpt"))
        Dim datetime = RadDateInput1.SelectedDate.Value
        Dim m = datetime.Month
        Dim y = datetime.Year
        cryRpt.SetParameterValue("m", m)
        cryRpt.SetParameterValue("y", y)

        With crConnectionInfo
            .ServerName = Common.Server '"dbazure.vsoftgroup.com"
            .DatabaseName = Common.DataBase ' "CangVu_Document"
            .UserID = Common.DataBaseUser ' "ucangvu"
            .Password = Common.DataBasePass ' "pcangvu"
        End With

        CrTables = cryRpt.Database.Tables
        For Each CrTable In CrTables
            crtableLogoninfo = CrTable.LogOnInfo
            crtableLogoninfo.ConnectionInfo = crConnectionInfo
            CrTable.ApplyLogOnInfo(crtableLogoninfo)
        Next

        CrystalReportViewer1.ReportSource = cryRpt
        CrystalReportViewer1.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None
        CrystalReportViewer1.SeparatePages = False
    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        cryRpt.Close()
        cryRpt.Dispose()
    End Sub
End Class



