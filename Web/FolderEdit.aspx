﻿<%@ Page Language="VB" MasterPageFile="~/Template.Master" AutoEventWireup="false" CodeFile="FolderEdit.aspx.vb" Inherits="FolderEdit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
    <div class="componentheading"><asp:label runat="server" ID="lblHeading" Text=""></asp:label></div>
    <table cellpadding="4" cellspacing="0" border="0">
        <colgroup>
            <col width="100" />
            <col width="320" />
        </colgroup>

        <tr>
            <td>Tên thư mục</td>
            <td>
                <asp:TextBox ID="txtTaskName" runat="server" CssClass="textbox" Width="300px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvName" Runat="server" Display="Dynamic" ControlToValidate="txtTaskName" ErrorMessage="*" />
            </td>
        </tr>
        <tr>
            <td>Ghi chú</td>
            <td><asp:TextBox ID="txtRemark" runat="server" CssClass="textbox" Width="300px"></asp:TextBox></td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <hr />
                <asp:Button ID="cmdSave" runat="server" Text="Lưu" Width="80px" />
                <asp:Button ID="cmdExit" runat="server" Text="Trở về" CausesValidation="false" Width="80px"/>
            </td>
        </tr>
    </table>
</asp:Content>