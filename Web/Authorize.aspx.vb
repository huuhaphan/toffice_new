Imports Telerik.Web.UI
Imports system.Data
Partial Public Class Authorize
    Inherits SecurePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If Not IsInRole("DOCS") Then
                ErrHandler.ShowError(401)
            End If
        End If
    End Sub

    Public Sub RadGrid1_NeedDataSource(ByVal source As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs)
        RadGrid1.DataSource = Documents.GetAuthorizeList(Common.UserId)
    End Sub

    Protected Sub RadToolBar1_ButtonClick(ByVal sender As Object, ByVal e As RadToolBarEventArgs)
        If e.Item.Value = "Delete" Then
            'Delete
            Dim Authorize_Id As Integer
            For Each dataItem As Telerik.Web.UI.GridDataItem In RadGrid1.SelectedItems
                Authorize_Id = dataItem.GetDataKeyValue("Authorize_Id")
                Documents.DeleteAuthorize(Authorize_Id)
            Next
            RadGrid1.Rebind()
        End If
    End Sub

End Class