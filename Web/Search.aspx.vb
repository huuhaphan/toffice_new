Imports System.Data
Partial Public Class Search
    Inherits SecurePage
    Dim MsgType As String

    Private Property DataSource() As DataTable
        Get
            Dim ret As DataTable = DirectCast(ViewState("dataSource"), DataTable)
            If ret Is Nothing Then
                Dim MsgType As String = Common.GetQueryString("Type")
                If MsgType = "DO" Then
                    ret = Documents.GetSearchList(txtSearch.Text, ddlSearchFor.Text)
                Else
                    ret = Mails.GetSearchList(txtSearch.Text, ddlSearchFor.Text)
                End If
            End If
            Return ret
        End Get
        Set(ByVal value As DataTable)
            ViewState("dataSource") = value
        End Set
    End Property

    Public Sub RadGrid1_NeedDataSource(ByVal source As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs)
        RadGrid1.DataSource = DataSource
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        DataSource = Nothing
        RadGrid1.Rebind()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            MsgType = Common.GetQueryString("Type")
            If Common.ValidMsgType(MsgType) Then
                If MsgType = "DO" Then
                    If Not (IsInRole("DOCS")) Then
                        ErrHandler.ShowError(401)
                    End If
                    lblHeader.Text = "Tìm công văn"
                Else
                    If Not (IsInRole("MAIL")) Then
                        ErrHandler.ShowError(401)
                    End If
                    lblHeader.Text = "Tìm thư nội bộ"
                End If
            Else
                ErrHandler.ShowError(601)
            End If

        End If
    End Sub

    Protected Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        DataSource = Nothing
        RadGrid1.Rebind()
    End Sub
End Class