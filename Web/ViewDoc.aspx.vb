Imports Telerik.Web.UI
Partial Class ViewDoc
    Inherits SecurePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim DocId As Integer = Integer.Parse(Common.GetQueryString("id"))
            Dim PostId As Integer = CInt(Common.GetQueryString("postid"))

            Dim objDetail As IDocumentDetail = Documents.GetDocumentDetail(DocId)
            'lblFrom.Text = objDetail.FromUser
            'lblTo.Text = objDetail.ToUser
            lblSigner.Text = objDetail.Signer
            lblSubject.Text = Server.HtmlEncode(objDetail.Subject)
            lblAttachment.Text = objDetail.Attachment
            lblDocNum.Text = objDetail.DocNum
            lblBody.Text = objDetail.Body
            lblDate.Text = objDetail.PublicDate

            lbtFormSend.Text = objDetail.FromOffice
            lblToOffice.Text = objDetail.ToOffice
            lblToDep.Text = objDetail.ToDep
            lblPublishDep.Text = objDetail.PublishDep
            lblBophanchinh.Text = objDetail.Bophanchinh
            lblBophanphoihop.Text = objDetail.Bophanphoihop
            lbtSignerPos.Text = objDetail.SignerPos
            lblLinhvuc.Text = objDetail.Field
            lblToDate.Text = objDetail.ToDate
            lblApplyDate.Text = objDetail.PublicDate
            lblRegisterNum.Text = objDetail.RegisterNum
            lblType.Text = objDetail.MsgType
            lblFromDate.Text = objDetail.FromDate

            RadGrid2.DataSource = Documents.GetHangMuc(DocId)

            'Dim forwardbtn As RadToolBarButton = RadToolBar1.FindItemByValue("Forward")
            'forwardbtn.NavigateUrl = "~/compose.aspx?cmd=forward&type=DO&id=" & DocId.ToString

            Dim NextMsgId, NextPostId, PrevMsgId, PrevPostId As Integer
            Documents.GetPrevNextDoc(PostId, PrevMsgId, PrevPostId, NextMsgId, NextPostId)
            If PrevMsgId > 0 Then
                Dim prevbtn As RadToolBarButton = RadToolBar1.FindItemByValue("PrevMsg")
                prevbtn.NavigateUrl = "~/viewdoc.aspx?id=" & PrevMsgId.ToString & "&postid=" & PrevPostId.ToString
            End If
            If NextMsgId > 0 Then
                Dim nextbtn As RadToolBarButton = RadToolBar1.FindItemByValue("NextMsg")
                nextbtn.NavigateUrl = "~/viewdoc.aspx?id=" & NextMsgId.ToString & "&postid=" & NextPostId.ToString
            End If

        End If
    End Sub



    Protected Sub RadToolBar1_ButtonClick(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadToolBarEventArgs)
        If e.Item.Value = "Delete" Then
            Dim PostId As Integer = Integer.Parse(Common.GetQueryString("postid"))
            Common.MoveMessage(PostId, "DL")
            Common.CloseWindow()
        End If
    End Sub
End Class
