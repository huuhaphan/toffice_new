﻿<%@ Page Language="VB" MasterPageFile="~/Template.Master" AutoEventWireup="false" CodeFile="Password.aspx.vb" Inherits="Password"%>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
<script language="javascript" type="text/javascript">
// <!CDATA[

function CloseWindow() {
    window.close();
}

// ]]>
</script>

    <div class="componentheading">Thay đổi mật khẩu</div>
    <div class="login">
        <div class="loginwrapper">
            <table border="0" cellpadding="0" cellspacing="5" width="320px">
                <tr>
                    <td>Mã mật khẩu cũ:<br /><asp:TextBox ID="txtOldPwd" runat="server" MaxLength="30" TextMode="Password" Width="300px" /></td>
                </tr>
                <tr>
                    <td>Mã mật khẩu mới:<br /><asp:TextBox ID="txtNewPwd" runat="server" MaxLength="30" TextMode="Password" Width="300px" /></td>
                </tr>
                <tr>
                    <td>Xác nhận:<br /><asp:TextBox ID="txtConfirm" runat="server" MaxLength="30" TextMode="Password" Width="300px" /></td>
                </tr>
                <tr>
                    <td><asp:Label ID="lblError" runat="server" ForeColor="Red" Visible="False">Mật khẩu không hợp lệ.</asp:Label></td>
                </tr>
                <tr>
                    <td align="center" style="border-top: 1px solid #DEE8F6; padding-top: 5px">
                        <asp:Button ID="btnEnter" runat="server" Text="Đồng ý" />
                        <input accesskey="r" tabindex="1" type="reset" value="Thoát" id="cmdExit" onclick="javascript:history.go(-1)" />
                    </td>
                </tr>
            </table>
        </div>
    </div>

</asp:Content>

