Imports Telerik.Web.UI
Imports System.Data

Partial Public Class ChooseUser
    Inherits System.Web.UI.Page

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            rblUser.Text = "G"
            rblSearchFor.Text = "L"
            RadGrid1.MasterTableView.FilterExpression = "(Type = 'G')"
        End If
    End Sub

    Public Sub RadGrid1_NeedDataSource(ByVal source As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs)
        RadGrid1.DataSource = Common.GetUserGroupList()
    End Sub

    Protected Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        Call Search()
    End Sub

    Private Sub rblUser_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rblUser.SelectedIndexChanged
        Dim filterExpression As String = ""
        If sender.text <> "A" Then
            filterExpression = "(Type = '" + sender.text + "')"
        End If
        RadGrid1.MasterTableView.FilterExpression = filterExpression
        RadGrid1.MasterTableView.Rebind()
    End Sub

    Sub Search()
        Dim filterExpression As String = String.Empty
        If txtSearch.Text <> "" Then
            Dim SearchFor As String = Trim(txtSearch.Text)
            If rblSearchFor.Text = "F" Then
                filterExpression = "([Name] like '%" + SearchFor + "%' OR [Organization] like '%" + SearchFor + "%')"
            Else
                filterExpression = "([Lastname] = '" + UCase(SearchFor) + "')"
            End If
        Else
            If rblUser.Text <> "A" Then
                filterExpression = "(Type = '" + rblUser.Text + "')"
            End If
        End If
        RadGrid1.MasterTableView.FilterExpression = filterExpression
        RadGrid1.MasterTableView.Rebind()
    End Sub

    Protected Sub chkDisplay_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkDisplay.CheckedChanged
        RadGrid1.AllowPaging = Not chkDisplay.Checked
        RadGrid1.Rebind()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Call Search()
    End Sub
End Class
