Imports System.Net.Mail
Imports Telerik.Web.UI
Imports System.Configuration

Partial Class SendMail
    Inherits System.Web.UI.Page

    Protected Sub RadToolBar1_ButtonClick(ByVal sender As Object, ByVal e As RadToolBarEventArgs)
        lblStatus.Text = "Sending successfull"
        Try
            Dim mail As MailMessage = New MailMessage()
            Dim maillist As String() = cboTo.Text.Split(";")
            For Each mailuser As String In maillist
                mail.To.Add(mailuser)
            Next

            If Not String.IsNullOrEmpty(cboCc.Text) Then
                mail.CC.Add(cboCc.Text)
            End If

            If Not String.IsNullOrEmpty(cboBc.Text) Then
                mail.Bcc.Add(cboBc.Text)
            End If

            mail.From = New MailAddress("namdah@tiags.vn")
            mail.Subject = "Email using TOffice"
            mail.Body = Me.redtBody.Content
            mail.IsBodyHtml = True
            'Attachment
            If RadUpload1.UploadedFiles.Count > 0 Then
                For Each File As UploadedFile In RadUpload1.UploadedFiles
                    mail.Attachments.Add(New System.Net.Mail.Attachment(File.FileName))
                Next
            End If

            If Common.GetQueryString("Id") IsNot Nothing Then
                Dim MsgID As Integer = CInt(Common.GetQueryString("Id"))
                Dim FileAttachment As String
                If Common.GetQueryString("Type") = "ML" Then
                    FileAttachment = Mails.GetMailAttachmentPathByMailID(MsgID)
                Else
                    FileAttachment = Documents.GetDocAttachmentPathByDocID(MsgID)
                End If
                For Each FileName As String In FileAttachment.Split(";")
                    If Not String.IsNullOrEmpty(FileName) Then
                        mail.Attachments.Add(New Attachment(FileName))
                    End If
                Next
            End If

            Dim smtp As SmtpClient = New SmtpClient()
            Dim smtpUser As Char = "namdh@tiags.vn"
            Dim smtpPwd As Char = ""

            smtp.Host = ConfigurationManager.AppSettings("SmtpServer")
            smtp.Port = ConfigurationManager.AppSettings("SmtpPort")
            smtp.Credentials = New System.Net.NetworkCredential(smtpUser, smtpPwd)
            'smtp.EnableSsl = True
            smtp.Send(mail)
        Catch ex As Exception
            lblStatus.Text = ex.Message
            Panel1.Visible = True
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If Common.GetQueryString("Id") IsNot Nothing Then
                Dim MsgId As Integer = CInt(Common.GetQueryString("Id"))
                Dim MsgType As String = Common.GetQueryString("Type")
                Dim objMessage As Object
                If MsgType = "ML" Then
                    objMessage = Mails.GetMailDetail(MsgId)
                Else
                    objMessage = Documents.GetDocumentDetail(MsgId)
                End If
                txtSubject.Text = objMessage.Subject
                lblAttachment.Text = objMessage.Attachment
                redtBody.Content = objMessage.Body
            End If
        End If
    End Sub
End Class
