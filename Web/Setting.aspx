﻿<%@ Page Language="VB" MasterPageFile="~/Template.Master" AutoEventWireup="false"
    CodeFile="Setting.aspx.vb" Inherits="Setting" Title="Untitled Page" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <table>
        <tr>
            <td>Chữ ký điện tử</td>
            <td><telerik:radtextbox runat="server" id="txtSignature" height="100px" textmode="MultiLine" width="500px" ></telerik:radtextbox></td>
        </tr>
        <tr>
            <td>Tự động cập nhật khi đang soạn thư</td>
            <td></td>
        </tr>
        <tr>
            <td>Xóa thư</td>
            <td></td>
        </tr>
    </table>
</asp:Content>
