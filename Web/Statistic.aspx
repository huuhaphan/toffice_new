﻿<%@ Page Language="VB" MasterPageFile="~/Template.Master" AutoEventWireup="false" CodeFile="Statistic.aspx.vb" Inherits="Statistic" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
    <div class="componentheading">Dung lượng đã sử dụng</div>
    <p>Tổng dung lượng được phép sử dụng là: <b><%=Common.GetSizeLimit(Enums.MailSize.MailBox) %> MB</b></p>
    <p><asp:Label ID="lblSum" runat="server"/></p>
    <br />
    <table cellpadding="4" cellspacing="0" class="statitic">
        <colgroup>
            <col width="100px" />
            <col width="150px" />
         </colgroup>
        <thead>
            <tr>
                <th align="left">Thư mục</th>
                <th align="right">Đã sử dụng</th>
            </tr>
        </thead>
        <tr style=" background-color:#F4F8FD; border-top:4px solid #CCC">
            <td class="left">Hộp thư:</td>
            <td align="right"><asp:Label ID="lblInbox" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td class="left">Đang soạn:</td>
            <td align="right"><asp:Label ID="lblDraft" runat="server"></asp:Label></td>
        </tr>
        <tr style=" background-color:#F4F8FD;">
            <td class="left">Đã xóa:</td>
            <td align="right"><asp:Label ID="lblDeleted" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td class="left">Đã gửi:</td>
            <td align="right"><asp:Label ID="lblSent" runat="server"></asp:Label></td>
        </tr>
        <tr style=" background-color:#F4F8FD;">
            <td class="left">Thư mục riêng:</td>
            <td align="right"><asp:Label ID="lblPrivateFolder" runat="server"></asp:Label></td>
        </tr>
    </table> 
</asp:Content>

