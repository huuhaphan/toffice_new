﻿Partial Class DownloadAll
    Inherits SecurePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        RequestAuthorize()

        If Common.GetQueryString("refid") IsNot Nothing AndAlso Common.GetQueryString("type") IsNot Nothing Then
            Dim SoureId As Integer = CInt(Request.QueryString("refid"))
            Dim AttachList As New Collection
            If Common.GetQueryString("type") = "ML" Then
                Mails.GetAllMailAttachment(SoureId, AttachList)
            Else
                Documents.GetAllDocAttachment(SoureId, AttachList)
            End If
            Dim tempFileName = Server.MapPath("TempFiles/" & Guid.NewGuid().ToString() & ".zip")
            Dim tempZipName As String = SoureId.ToString + ".zip"
            Utils.ZipAllFiles(tempFileName, tempZipName, AttachList)
        End If
    End Sub
End Class