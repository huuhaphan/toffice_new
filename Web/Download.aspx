﻿<%@ Page Language="VB" MasterPageFile="~/Template.Master" AutoEventWireup="false" CodeFile="Download.aspx.vb" Inherits="Download" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
<div class="componentheading">Tiện ích</div>
<div style="padding:10px">
    <b>Hướng dẫn sử dụng</b>
    <ul>
        <li><asp:hyperlink ID="HyperLink6" runat="server" NavigateUrl="~/Download/HuongDan.pdf" >Hướng dẫn sử dụng (PDF)</asp:hyperlink></li>    
    </ul>
    <b>Trình duyệt web</b>
    <ul>
        <li><asp:hyperlink ID="HyperLink4" runat="server" NavigateUrl="~/Download/FireFoxSetup.exe" >Trình duyệt web FireFox</asp:hyperlink></li>
        <li><asp:hyperlink ID="HyperLink3" runat="server" NavigateUrl="~/Download/ChromeSetup.exe" >Trình duyệt web Google Chrome</asp:hyperlink></li>
    </ul>
    <b>Khác</b>   
    <ul>
        <li><asp:hyperlink ID="HyperLink1" runat="server" NavigateUrl="~/Download/FoxitReaderSetup.exe" >Phần mềm đọc file văn bản (PDF)</asp:hyperlink></li>
        <li><asp:hyperlink ID="HyperLink2" runat="server" NavigateUrl="~/Download/unicode.exe" >Font unicode cho Windows 98 </asp:hyperlink></li>
    </ul>
</div>
</asp:Content>

