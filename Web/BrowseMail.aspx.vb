Imports System.Data
Imports System.Data.Common
Imports Telerik.Web.UI
Imports Enums

Partial Public Class BrowseMail
    Inherits SecurePage

    Private ReadOnly Property Folder() As String
        Get
            Return Common.GetQueryString("Folder")
        End Get
    End Property

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        For Each column As GridColumn In RadGrid1.Columns
            Select Case column.UniqueName
                Case "From"
                    Dim boundColumn As GridBoundColumn = CType(column, GridBoundColumn)
                    boundColumn.Visible = IIf(Folder = "SD", False, True)
                Case "To"
                    Dim TemplateColumn As GridTemplateColumn = CType(column, GridTemplateColumn)
                    TemplateColumn.Visible = IIf(Folder = "SD", True, False)
            End Select
        Next

        'Inbox
        If Folder = "IN" Then
            RadMenu1.FindItemByValue("DeleteAll").Visible = False
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If Not IsInRole("MAIL") Then
                ErrHandler.ShowError(401)
            End If
            If Not Common.ValidFolder(Folder) Then
                ErrHandler.ShowError(601)
            End If
        End If
        Common.BuildToolbar(RadToolBar1, Common.UserId, "ML", Folder)
    End Sub

    Public Sub RadGrid1_NeedDataSource(ByVal source As Object, ByVal e As GridNeedDataSourceEventArgs)
        RadGrid1.DataSource = Mails.GetMailList(Folder)
    End Sub

    Protected Sub RadAjaxManager1_AjaxRequest(ByVal sender As Object, ByVal e As AjaxRequestEventArgs)
        Dim arguments As String() = e.Argument.Split(":")
        Select Case arguments(0)
            Case "ShowDetail"
                RefreshDetail(CInt(arguments(1)))
            Case "SetRead"
                Common.UpdateRead(CInt(arguments(1)), Enums.Mark.Read)
        End Select
    End Sub

    Sub RefreshDetail(Optional ByVal MailId As Integer = 0)
        If MailId > 0 Then
            Dim objDetail As Object = Mails.GetMailDetail(MailId)
            lblFrom.Text = objDetail.FromUser
            lblTo.Text = objDetail.ToUser
            lblSubject.Text = Server.HtmlEncode(objDetail.Subject)
            lblSendTime.Text = objDetail.SendTime
            lblAttachment.Text = objDetail.Attachment
            lblBody.Text = objDetail.Body
        Else
            lblFrom.Text = ""
            lblTo.Text = ""
            lblSubject.Text = ""
            lblSendTime.Text = ""
            lblAttachment.Text = ""
            lblBody.Text = ""
        End If
    End Sub

    Protected Sub RadMenu1_ItemClick(ByVal sender As Object, ByVal e As RadMenuEventArgs)
        RequestAuthorize()
        Select Case e.Item.Value
            Case "Delete"
                MoveToFolder("DL")
            Case "DeleteAll"
                If Folder = "DL" Then
                    Common.MoveAllMessage("ML", Folder, "DL")
                    RadGrid1.Rebind()
                    RefreshDetail()
                End If
            Case "MarkRead"
                MarkMessage(Mark.Read)
            Case "MarkUnRead"
                MarkMessage(Mark.Unread)
        End Select
    End Sub

    Protected Sub RadToolBar1_ButtonClick(ByVal sender As Object, ByVal e As RadToolBarEventArgs)
        RequestAuthorize()
        Select Case e.Item.Value
            Case "Check"
                RadGrid1.Rebind()
            Case "Inbox"
                MoveToFolder("IN")
            Case "Delete"
                MoveToFolder("DL")
            Case Else
                If Left(e.Item.Value, 2) = "FD" Then
                    MoveToFolder(e.Item.Value.Trim)
                End If
        End Select
    End Sub

    Sub MoveToFolder(ByVal newvalue As String)
        If RadGrid1.SelectedItems.Count > 0 AndAlso RadGrid1.SelectedItems.Count <= RadGrid1.PageSize Then
            Dim ForceEmpty As Boolean = IIf(Folder = "DL" AndAlso newvalue = "DL", True, False)
            Dim PersonalBox_Id As Integer
            For Each dataItem As GridDataItem In RadGrid1.SelectedItems
                PersonalBox_Id = dataItem.GetDataKeyValue("PersonalBox_Id")
                If PersonalBox_Id <> 0 Then
                    Common.MoveMessage(PersonalBox_Id, newvalue, ForceEmpty)
                End If
            Next
            RadGrid1.Rebind()
            RefreshDetail()
        End If
    End Sub

    Sub MarkMessage(ByVal value As String)
        If RadGrid1.SelectedItems.Count > 0 AndAlso RadGrid1.SelectedItems.Count <= RadGrid1.PageSize Then
            Dim PersonalBox_Id As Integer

            For Each dataItem As GridDataItem In RadGrid1.SelectedItems
                PersonalBox_Id = dataItem.GetDataKeyValue("PersonalBox_Id")
                If PersonalBox_Id <> 0 Then
                    Common.UpdateRead(PersonalBox_Id, value)
                End If
            Next
            RadGrid1.Rebind()
            RefreshDetail()
        End If
    End Sub

    Sub RadGrid1_ItemCreated(ByVal sender As Object, ByVal e As GridItemEventArgs)
        If TypeOf e.Item Is GridDataItem Then
            Dim dataItem As GridDataItem = e.Item
            'e.Item.Font.Bold = Not (e.Item.OwnerTableView.DataKeyValues(e.Item.ItemIndex)("Seen"))
            e.Item.Font.Bold = Not (dataItem.GetDataKeyValue("Seen"))
            If Folder = "SD" Then
                Dim Source_ID As Integer = dataItem.GetDataKeyValue("Source_ID")
                dataItem("To").Text = Mails.GetToUserList(Source_ID)
            End If
        End If
    End Sub
End Class