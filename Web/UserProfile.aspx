﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Template.Master" AutoEventWireup="false" CodeFile="UserProfile.aspx.vb" Inherits="UserProfile" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
    <div class="componentheading">Cập nhật thông tin người sử dụng</div>
    <table border="0" cellpadding ="4" cellspacing="0" class="multiPage" >
        <tr>
            <td><asp:CheckBox ID="chkPurge" Text="Xóa thư không lưu vào thư mục <b>Đã xoá</b>" runat="server" /></td>
        </tr>
        <tr>
            <td><asp:CheckBox ID="chkSignature" Text="Sử dụng chữ ký điện tử" runat="server" AutoPostBack="true" /></td>
        </tr>
        <tr>
            <td>
                <telerik:radeditor ID="redtBody" runat="server" Height="300px" Width="100%" 
                    ToolsFile="EdtToolDefault.xml" EditModes="Design" AutoResizeHeight="false" > 
                    <Content>
                    </Content>
                </telerik:radeditor>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center"><asp:Button ID="btnSave" runat="server" Text="Cập nhật" />&nbsp;<asp:Button ID="btnExit" runat="server" Text="Trở về" /></td>
        </tr>
    </table>
    <script type="text/javascript">
		//<![CDATA[

        Telerik.Web.UI.Editor.CommandList["InsertEmoticon"] = function (commandName, editor, args) {
            var myCallbackFunction = function (sender, args) {
                editor.pasteHtml(String.format("<img src='{0}' border='0' alt='emoticon' /> ", args.image));
            }
            editor.showExternalDialog('InsertEmoticon.aspx', {}, 400, 310, myCallbackFunction, null, 'Insert Emoticon', true, Telerik.Web.UI.WindowBehaviors.Close + Telerik.Web.UI.WindowBehaviors.Move, false, true);
        };
		//]]>
    </script>
</asp:Content>

