<%@ Page Language="VB" MasterPageFile="~/Template.Master" AutoEventWireup="false"
    CodeFile="Calendar.aspx.vb" Inherits="Calendar" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <telerik:RadScheduler runat="server" ID="RadScheduler1" ShowFooter="False"
        DayEndTime="21:00:00" TimeZoneOffset="07:00:00" Height="100%" Width="100%"
        FirstDayOfWeek="Monday" LastDayOfWeek="Saturday" DataKeyField="ID" DataStartField="Start"
        DataEndField="End" DataSubjectField="subject" DataRecurrenceField="RecurrenceRule"
        DataRecurrenceParentKeyField="RecurrenceParentID" HoursPanelTimeFormat="htt"
        ValidationGroup="RadScheduler1">
        <AdvancedForm Modal="true" />
        <TimelineView UserSelectable="False" />
    </telerik:RadScheduler>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadScheduler1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadScheduler1" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
</asp:Content>
