Imports System.Data
Partial Public Class BrowseNews
    Inherits Web.UI.Page

    Private ReadOnly Property NewsId() As Integer
        Get
            If Not Common.GetQueryString("Id") Is Nothing Then
                Return CInt(Common.GetQueryString("Id"))
            Else
                Return -1
            End If
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            lblTitle.Text = News.GetNewsTypeNameById(NewsId)
        End If
    End Sub

    Public Sub RadGrid1_NeedDataSource(ByVal source As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs)
        RadGrid1.DataSource = News.GetNewsList(NewsId)
    End Sub

End Class