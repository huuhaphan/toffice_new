Imports Telerik.Web.UI
Imports System.IO
Imports System.Data

Partial Public Class Compose
    Inherits SecurePage
    Dim MaxTotalBytes As Integer = 30 * 1024 * 1024 ' 1 MB
    Private totalBytes As Integer
    Dim DeniedUpload As New ArrayList

    Private Property MsgId() As Integer
        Get
            Return ViewState("MsgId")
        End Get
        Set(ByVal value As Integer)
            ViewState("MsgId") = value
        End Set
    End Property

    Private Property MsgType() As String
        Get
            Return ViewState("MsgType")
        End Get
        Set(ByVal value As String)
            ViewState("MsgType") = value
        End Set
    End Property

    Private Property MsgCmd() As String
        Get
            Return ViewState("MsgCmd")
        End Get
        Set(ByVal value As String)
            ViewState("MsgCmd") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

            RadNotification1.ShowInterval = (Session.Timeout - 1) * 60000
            If Not IsInRole("MAIL") Then
                ErrHandler.ShowError(401)
            End If

            If Not Mails.AllowCreateMail() Then
                Response.Redirect("error.aspx?code=602")
            End If

            'MaxUpload 
            Dim MaxUpload As Integer = Common.GetSizeLimit(Enums.MailSize.Attach)
            lblHeader.Text = "Tổng dung lượng là 30 MB.<br>Dung lượng tối đa của 1 file là " & MaxUpload.ToString & " MB."
            RadAsyncUpload1.MaxFileSize = MaxUpload * 1024 * 1024

            Dim RefId As Integer = CInt(Common.GetQueryString("id"))
            MsgId = 0
            MsgType = UCase(Common.GetQueryString("Type", "ML"))
            MsgCmd = UCase(Common.GetQueryString("Cmd", ""))

            'Check Parameter
            If Not (Common.ValidMsgType(MsgType) And Common.ValidMsgCmd(MsgCmd)) Then
                ErrHandler.ShowError(601)
            End If

            If RefId > 0 Then
                'Load message
                Dim objMessage As Object
                If Not Common.IsValidUser(RefId, MsgType) Then
                    ErrHandler.ShowError(401)
                End If

                If MsgType = "ML" Then
                    objMessage = Mails.GetMailDetail(RefId)
                Else
                    objMessage = Documents.GetDocumentDetail(RefId)
                End If

                Dim PreSubject As String
                If objMessage.Id IsNot Nothing Then
                    'Assign variable
                    Select Case MsgCmd
                        Case "FORWARD"
                            lblTo.Text = ""
                            hdn.Value = ""
                            lblAttachment.Text = objMessage.Attachment
                            PreSubject = "FW: "
                            redtBody.Content = GetMessageBody(objMessage)
                        Case "REPLYALL"
                            Dim MemberList As Collection = Common.GetMemberList(RefId, "ML")
                            ' ReceiverID
                            Dim cLink As String = ""
                            For Each person As UserReceiver In MemberList
                                If person.ReceiverType <> "BC" Then
                                    cLink += "<a rel=%" + person.UserType + person.LoginID + "%>" + person.Name + ";</a>"
                                End If
                            Next
                            ' SenderID
                            Dim SenderUser As Collection = Common.GetMemberList(objMessage.SenderId)
                            cLink += "<a rel=%" + SenderUser(1).usertype + SenderUser(1).loginid + "%>" + SenderUser(1).name + ";</a>"

                            lblTo.Text = cLink
                            hdn.Value = cLink & "#"
                            PreSubject = "RE: "
                            redtBody.Content = GetMessageBody(objMessage)
                            'txtSubject.Enabled = False
                        Case "REPLY"
                            Dim MemberList As Collection = Common.GetMemberList(objMessage.SenderId)
                            Dim cLink As String = "<a rel=%" + MemberList(1).usertype + MemberList(1).loginid + "%>" + MemberList(1).name + ";</a>"
                            lblTo.Text = cLink
                            hdn.Value = cLink & "#"
                            PreSubject = "RE: "
                            redtBody.Content = GetMessageBody(objMessage)
                            'txtSubject.Enabled = False
                        Case Else
                            PreSubject = ""
                            redtBody.Content = objMessage.Body
                            lblAttachment.Text = objMessage.Attachment
                            MsgId = RefId
                    End Select
                    txtSubject.Text = PreSubject & objMessage.Subject
                End If
            End If
            redtBody.Content = Common.Signature(Common.UserId) + redtBody.Content
        End If
    End Sub

    Protected Sub RadToolBar1_ButtonClick(ByVal sender As Object, ByVal e As RadToolBarEventArgs)
        Select Case e.Item.Value
            Case "Send"
                ProcessMessage("SD")

            Case "Save"
                ProcessMessage("DR")
        End Select
    End Sub

    Protected Sub ProcessMessage(ByVal Action As String)
        Me.TipTextDiv.Visible = False

        Dim MemberList As New Collection
        If Action = "SD" Then
            'Send
            InsertMember(VBLib.Strings.GetWordNum(hdn.Value, 1, "#"), "RC", MemberList)
            InsertMember(VBLib.Strings.GetWordNum(hdn.Value, 2, "#"), "CC", MemberList)
            InsertMember(VBLib.Strings.GetWordNum(hdn.Value, 3, "#"), "BC", MemberList)
            If MemberList.Count = 0 Then
                ShowError("Phải chọn người nhận.")
                Exit Sub
            End If
        End If

        MsgId = Mails.SaveMail(MsgId, Common.UserId, txtSubject.Text, redtBody.Content, 0, MemberList, Action, Important.Checked)

        If MsgId > 0 Then
            'Process Attachments
            If RadAsyncUpload1.UploadedFiles.Count > 0 AndAlso DeniedUpload.Count < RadAsyncUpload1.UploadedFiles.Count Then
                Dim FileName As String
                Dim PathId As String = "", SaveDir As String = ""
                Call Common.GetPhysicalPath("MAIL", PathId, SaveDir)
                SaveDir = Common.VerifyFolder(SaveDir)
                If String.IsNullOrEmpty(SaveDir) Then
                    ShowError("Chưa tạo đường dẫn để lưu file đính kèm. Vui lòng liên hệ với người quản trị.")
                    Exit Sub
                End If

                For Each File As UploadedFile In RadAsyncUpload1.UploadedFiles
                    If Not DeniedUpload.Contains(File.FileName) AndAlso File.ContentLength > 0 Then
                        FileName = Mails.InsMailAttach(MsgId, File.GetName, File.ContentLength, File.GetExtension, PathId)
                        File.SaveAs(Path.Combine(SaveDir, FileName))
                    End If
                Next
            End If

            'Neu Forward thi se chuyen luon file dinh kem
            MsgType = UCase(Common.GetQueryString("Type", "ML"))
            MsgCmd  = UCase(Common.GetQueryString("Cmd", ""))
            If MsgCmd = "FORWARD" Then
                Mails.MailAttachmentForward(CInt(Common.GetQueryString("id")), MsgType, MsgId)
            End If

            'Cap nhat lai tinh trang mail co dinh kem hay kg
            Mails.UpdatePersonalAttach(MsgId)
        End If

        If Action = "SD" Then
            If DeniedUpload.Count > 0 Then
                Dim fileError As String = ""
                For i = 0 To DeniedUpload.Count - 1
                    fileError = fileError & DeniedUpload(i) & ";"
                Next
                Common.ShowAlert("Các File không gửi được :" & fileError)
            End If
            'Dong cua so khi gui thanh cong
            Common.CloseWindow()
        End If
    End Sub

    Sub InsertMember(ByVal str As String, ByVal sType As String, ByRef Member As Collection)
        If String.IsNullOrEmpty(str) Then
            Exit Sub
        End If
        str = str.ToUpper
        Dim person As UserReceiver
        Dim cLink, cValue, cName As String
        Dim nWordCount As Integer = VBLib.Strings.GetWordCount(str, "</A>")
        For i As Integer = 1 To nWordCount
            cLink = VBLib.Strings.GetWordNum(str, i, ";</A>")
            cValue = VBLib.Strings.GetWordNum(cLink, 2, "%")
            cName = VBLib.Strings.GetWordNum(cLink, 2, ">")

            'Fill the user's information
            person.LoginID = cValue.Substring(1)
            person.ReceiverType = sType
            person.UserType = Left(cValue, 1)
            person.Name = cName
            Member.Add(person)
        Next
    End Sub

    Protected Function GetMessageBody(ByVal msg As Object) As String
        Dim strBody As String = ""
        strBody = "<br /><hr /><strong>Người gửi:</strong> " & msg.FromUser & "<br />"
        strBody += "<strong>Ngày gửi:</strong> " & Format(msg.SendTime, "dd/MM/yyyy-h:mtt") & "<br />"
        strBody += "<strong>Chủ đề:</strong> " & msg.Subject & "<br />"
        strBody += msg.Body
        Return strBody
    End Function

    Sub ShowError(ByVal ErrStr As String)
        Me.TipTextDiv.Visible = True
        Me.lblError.Text = ErrStr
    End Sub

    Public Sub RadAsyncUpload1_FileUploaded(ByVal sender As Object, ByVal e As FileUploadedEventArgs)
        Dim fileName As New Label()
        fileName.Text = e.File.FileName

        If totalBytes + e.File.ContentLength < MaxTotalBytes Then
            ' Total bytes limit has not been reached, accept the file
            e.IsValid = True
            totalBytes += e.File.ContentLength
        Else
            ' Limit reached, discard the file
            e.IsValid = False
        End If

        If Not e.IsValid Then
            DeniedUpload.Add(e.File.FileName)
        End If
    End Sub
End Class
