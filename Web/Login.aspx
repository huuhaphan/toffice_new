﻿<%@ Page Language="vb" AutoEventWireup="false" CodeFile="Login.aspx.vb" Inherits="Login"
    MasterPageFile="~/TemplateFull.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder">
    <div class="componentheading">Đăng nhập hệ thống</div>
    <div class="login">
        <table width="100%" cellspacing="10">
            <tr>
               <td align="center"><asp:label id="lblError" runat="server" forecolor="Red" visible="False">Đăng nhập không thành công!</asp:label></td>
            </tr>
        </table>

        <div class="loginwrapper">
            <table border="0" cellpadding="0" cellspacing="5" width="100%">
                <tr>
                    <%--<td><asp:Image runat="server" ID="ImgLogin" ImageUrl="~/images/saa.jpg" Width="310px" /></td>--%>
                  <%--  <td id="header-login-box"><h3 id="title-login-box">PHẦN MỀM QUẢN LÝ CÔNG VĂN - ĐIỀU HÀNH CÔNG VIỆC</h3></td>--%>
                </tr>
                <tr>
                    <td>Tài khoản:<br />
                        <telerik:RadTextBox ID="txtUserName" runat="server" 
                            Width="300px" EmptyMessage="Nhập mã người sử dụng vào đây" Height="28px" /></td>
                </tr>
                <tr>
                    <td>Mật mã:<br />
                        <telerik:RadTextBox ID="txtPassword" runat="server" MaxLength="30" 
                            TextMode="Password" Width="300px" type="password" value="" Height="28px" /></td>
                </tr>
                <tr>
                    <td><asp:CheckBox ID="cookieuser" runat="server" />Ghi Nhớ?</td>
                </tr>
                <tr>
                    <td align="center" style="border-top:1px solid #DEE8F6;padding-top:5px">
                        <telerik:RadButton ID="btnEnter" runat="server" Text="Ðăng Nhập" Width="85px"/>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
