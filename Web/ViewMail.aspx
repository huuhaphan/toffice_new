﻿<%@ Page Language="VB" MasterPageFile="~/TemplateFull.Master" AutoEventWireup="false" CodeFile="ViewMail.aspx.vb" Inherits="ViewMail" title="Xem thư" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
<script type="text/javascript">
    //<![CDATA[
    function ToolbarClicking(sender, args)
    {
        if (args.get_item().get_value() == "Delete") {if (!confirm("Bạn thực sự muốn xóa mẫu tin này?")) {args.set_cancel(true);}}
    } 
    //]]>
</script>

<div id="messagewrapper" class="message">
    <telerik:RadToolBar ID="RadToolBar1" runat="server" OnClientButtonClicking="ToolbarClicking" OnButtonClick="RadToolBar1_ButtonClick" Width="100%">
        <Items>
            <telerik:RadToolBarButton ImageUrl="~/images/reply.gif" Text="Trả lời" Target="_blank"
                Value="Reply" PostBack="False"  runat="server" />
            <telerik:RadToolBarButton ImageUrl="~/images/forward.gif" Text="Chuyển tiếp" Target="_blank"
                Value="Forward" PostBack="False" runat="server" />
            <telerik:RadToolBarButton IsSeparator="true" />
            <telerik:RadToolBarButton ImageUrl="~/images/print.gif" Value="Print" ToolTip="In" runat="server" NavigateUrl="javascript:window.print()" PostBack="false" />
            <telerik:RadToolBarButton ImageUrl="~/images/delete.gif" Value="Delete" ToolTip="Delete" runat="server" />
            <telerik:RadToolBarButton IsSeparator="true" />
            <telerik:RadToolBarButton ImageUrl="~/images/prev.png" Value="PrevMsg" ToolTip="Thư trước" runat="server" PostBack="False"/>
            <telerik:RadToolBarButton ImageUrl="~/images/next.png" Value="NextMsg" ToolTip="Thư kế" runat="server" PostBack="False"/>
        </Items>
    </telerik:RadToolBar>

    <div class="messagetitle">
    <table cellpadding="0" cellspacing="2" border="0" width="100%">
        <colgroup>
            <col width="80px" />
            <col />
        </colgroup>
        <tr>
            <td>Người gửi:</td>
            <td>
                <div class="inputfield"><asp:Label ID="lblFrom" runat="server" /></div>
            </td>
        </tr>
        <tr>
            <td>Người nhận:</td>
            <td>
                <div class="inputfield"><asp:Label ID="lblTo" runat="server" /></div>
            </td>
        </tr>
        <tr>
            <td>Đồng gửi:</td>
            <td>
                <div class="inputfield"><asp:Label ID="lblCc" runat="server" /></div>
            </td>
        </tr>
        <tr>
            <td>Chủ đề:</td>
            <td>
                <div class="inputfield"><asp:Label ID="lblSubject" runat="server" /></div>
            </td>
        </tr>
        <tr>
            <td>Đính kèm:</td>
            <td >
                <div class="inputfield"><asp:Label ID="lblAttachment" runat="server" /></div>
            </td>
        </tr>
    </table>
    </div>
    <div class="messagebody" style="min-height:350px;"><asp:Label ID="lblBody" runat="server"></asp:Label></div>
</div>
</asp:Content>

