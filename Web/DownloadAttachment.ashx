﻿<%@ WebHandler Language="VB" Class="DownloadAttachment" %>

Imports System
Imports System.Web

Public Class DownloadAttachment : Implements IHttpHandler
    
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim fileid As Integer = CInt(context.Request.QueryString("fileid"))
        Dim FileName As String = ""
        Dim PhysicalPath As String = ""
        If context.Request.QueryString("type") = "ML" Then
            Mails.GetMailAttachmentPath(fileid, FileName, PhysicalPath)
        Else
            Documents.GetDocAttachmentPath(fileid, FileName, PhysicalPath)
        End If
        If String.IsNullOrEmpty(PhysicalPath) Then
            ErrHandler.ShowError(604)
        Else
            Utils.SendFile(PhysicalPath, FileName)
        End If
    End Sub
 
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class