﻿<%@ Control Language="vb" AutoEventWireup="false" CodeFile="Menu.ascx.vb" Inherits="Menu" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<div style="padding:3px">
    <telerik:radpanelbar runat="server" id="RadPanelBar1" Width="100%" PersistStateInCookie="true">
        <CollapseAnimation Type="None" Duration="100"></CollapseAnimation>
        <Items>
            <telerik:RadPanelItem Text="Trang chủ" ImageUrl="~/Images/home.png" runat="server" NavigateUrl="~/Default.aspx"></telerik:RadPanelItem>
            <telerik:RadPanelItem ImageUrl="~/Images/calendar.png" Text="Lịch làm việc" Expanded="False" runat="server" NavigateUrl="~/Calendas.aspx" ></telerik:RadPanelItem>
            <telerik:RadPanelItem ImageUrl="~/Images/calendar.png" Text="Lịch trực ban" Expanded="False" runat="server" NavigateUrl="~/Schedule.aspx" ></telerik:RadPanelItem>

            <telerik:RadPanelItem ImageUrl="~/Images/note.png" Text="C&#244;ng văn" Expanded="True" runat="server">
                <Items>
                    <telerik:RadPanelItem Text="Công văn đến" runat="server" NavigateUrl="~/browsedocument.aspx?folder=IN" Value="DOCIN"/>
                    <telerik:RadPanelItem Text="Đ&#227; x&#243;a" runat="server" NavigateUrl="~/browsedocument.aspx?folder=DL" />
                   <%-- <telerik:RadPanelItem Text="Uỷ quyền" runat="server" NavigateUrl="~/authorize.aspx" />--%>
                    <telerik:RadPanelItem Text="T&#236;m c&#244;ng văn" runat="server" NavigateUrl="~/search.aspx?type=DO" />
                </Items>
            </telerik:RadPanelItem>
            <%--<telerik:RadPanelItem ImageUrl="~/Images/email.gif" Text="Thư nội bộ" runat="server">
                <Items>
                    <telerik:RadPanelItem Text="Hộp thư đến" runat="server" NavigateUrl="~/browsemail.aspx?folder=IN" Value="MAILIN" />
                    <telerik:RadPanelItem Text="Đang soạn" runat="server" NavigateUrl="~/browsemail.aspx?folder=DR" />
                    <telerik:RadPanelItem Text="Đ&#227; x&#243;a" runat="server" NavigateUrl="~/browsemail.aspx?folder=DL" />
                    <telerik:RadPanelItem Text="Đ&#227; gửi" runat="server" NavigateUrl="~/browsemail.aspx?folder=SD" />
                    <telerik:RadPanelItem Text="T&#236;m thư" runat="server" NavigateUrl="~/search.aspx?type=ML" />
                </Items>
            </telerik:RadPanelItem>--%>
            <%--<telerik:RadPanelItem ImageUrl="~/Images/folder.png" Text="Thư mục riêng" runat="server">
                <Items>
                    <telerik:RadPanelItem Text="Thêm/Xóa" runat="server" NavigateUrl="~/Folder.aspx" />
                </Items>           
            </telerik:RadPanelItem>--%>
            <%--<telerik:RadPanelItem ImageUrl="~/Images/news.png" Text="Tin tức &amp; Sự kiện" runat="server">
                <Items>
                    <telerik:RadPanelItem Text="Tin tức" runat="server" NavigateUrl="~/Default.aspx" />
                    <telerik:RadPanelItem Text="Soạn tin" runat="server" NavigateUrl="~/Postnews.aspx"/>
                    <telerik:RadPanelItem Text="Tìm tin" runat="server" NavigateUrl="~/SearchNews.aspx" />
                </Items>
            </telerik:RadPanelItem>--%>
            <%--<telerik:RadPanelItem ImageUrl="~/Images/template.png" Text="Biểu mẫu" runat="server" NavigateUrl="~/templates.aspx">
                <Items>
                    <telerik:RadPanelItem Text="Thêm mới" runat="server" NavigateUrl="~/templateEdit.aspx" />
                </Items>            
            </telerik:RadPanelItem>--%>
            <%--<telerik:RadPanelItem ImageUrl="~/Images/template.png" Text="Dành cho nhân viên" runat="server" NavigateUrl="~/Advise.aspx">
                <Items>
                    <telerik:RadPanelItem Text="Thêm mới" runat="server" NavigateUrl="~/AdviseEdit.aspx" />
                </Items>            
            </telerik:RadPanelItem>--%>
            <%--<telerik:RadPanelItem ImageUrl="~/Images/preferences.png" Text="Tiện ích" runat="server">
                <Items>
                    <telerik:RadPanelItem Text="Cài đặt phần mềm" runat="server" NavigateUrl="~/Download.aspx"/>
                </Items>
            </telerik:RadPanelItem>--%>
            <%--<telerik:RadPanelItem Text="Danh bạ" runat="server" Enabled="true">
                <Items>
                    <telerik:RadPanelItem Text="Danh bạ" runat="server">
                    </telerik:RadPanelItem>
                    <telerik:RadPanelItem Text="Th&#234;m danh bạ" runat="server">
                    </telerik:RadPanelItem>
                </Items>
            </telerik:RadPanelItem>--%>
        </Items>
        <ExpandAnimation Type="None" Duration="100"></ExpandAnimation>
    </telerik:radpanelbar>
</div>