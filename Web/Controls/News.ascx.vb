Imports System.Data
Imports Telerik.Web.UI
Partial Class Controls_News
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Using dr As IDataReader = News.GetNewsType()
                DataList1.DataSource = dr
                DataList1.DataBind()
            End Using
        End If
    End Sub

    Protected Sub DataList1_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles DataList1.ItemDataBound
        Dim id As Integer = CInt(DataList1.DataKeys(e.Item.ItemIndex))
        If id <> 0 Then
            Dim grd As RadGrid = e.Item.FindControl("gridview")
            grd.DataSource = News.GetNews(id)
            grd.DataBind()
        End If
    End Sub

    Public Sub grdLatest_NeedDataSource(ByVal source As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs)
        grdLatest.DataSource = News.GetNews(98)
    End Sub

    Public Sub grdRead_NeedDataSource(ByVal source As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs)
        grdRead.DataSource = News.GetNews(99)
    End Sub
End Class
