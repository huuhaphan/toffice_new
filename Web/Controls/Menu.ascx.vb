Imports System.Data
Partial Public Class Menu
    Inherits System.Web.UI.UserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If Not String.IsNullOrEmpty(Common.UserId) Then
                Dim owner As Telerik.Web.UI.RadPanelItem = RadPanelBar1.FindItemByText("Thư mục riêng")
                Using dr As IDataReader = TaskFolders.GetTaskFolderListByUser(Common.UserId)
                    Do While dr.Read()
                        Dim NewItem As New Telerik.Web.UI.RadPanelItem()
                        NewItem.Text = Trim(dr("TaskName"))
                        NewItem.NavigateUrl = "~/browsefolder.aspx?id=" & dr("TaskFolder_ID").ToString
                        owner.Items.Add(NewItem)
                    Loop
                End Using
            End If

            'Dim MailInbox As Telerik.Web.UI.RadPanelItem = RadPanelBar1.FindItemByValue("MAILIN")
            'MailInbox.Text = "Hộp thư đến (" + Common.CountUnRead("ML", "IN").ToString + ")"
        End If
    End Sub
End Class