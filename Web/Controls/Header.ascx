﻿<%@ Control Language="vb" AutoEventWireup="false" CodeFile="Header.ascx.vb" Inherits="Header" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<div class="header"></div>
<div class="menu">
    <ul>
        <li><a href="/default.aspx"><span>Trang ch&#7911;</span></a></li>
        <li><a href="/login.aspx"><span>Đăng nhập</span></a></li>
        <li><a href="/inbox.aspx"><span>Hướng dẫn</span></a></li>
        <li>
			<form action="search.aspx" method="post">
	            <div style="float:right">
	                <img src="../images/icon-search.gif" alt="search"/>
		            <input name="searchword" id="mod_search_searchword" maxlength="20" class="inputbox" type="text" size="20" value="search..."  onblur="if(this.value=='') this.value='search...';" onfocus="if(this.value=='search...') this.value='';" />
		        </div>
            </form>
        </li>
    </ul>
</div>
