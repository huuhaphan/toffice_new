﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="News.ascx.vb" Inherits="Controls_News" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<div class="news">
    <asp:DataList ID="DataList1" DataKeyField="id" runat="server" Width="100%" OnItemDataBound="DataList1_ItemDataBound">
        <ItemTemplate>
            <div class="box">
                <h4>
                    <asp:HyperLink ID="HyperLink1" Text='<%# Eval("Name") %>' NavigateUrl='<%# Eval("Id", "~/BrowseNews.aspx?id={0}") %>'
                        runat="server" CssClass="tcat"></asp:HyperLink>
                </h4>
                <div class="box-ct clearfix">
                    <telerik:RadGrid ID="GridView" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        BorderWidth="0" EnableViewState="false" GridLines="None" Skin="None" ShowHeader="false"
                        EnableEmbeddedSkins="false" Width="100%">
                        <MasterTableView>
                            <NoRecordsTemplate>
                                &nbsp;</NoRecordsTemplate>
                            <Columns>
                                <telerik:GridTemplateColumn>
                                    <ItemTemplate>
                                        <asp:HyperLink ID="HyperLink1" Text='<%# Eval("Subject") %>' NavigateUrl='<%# Eval("Id", "~/News.aspx?id={0}") %>'
                                            runat="server" CssClass="thead"></asp:HyperLink>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                </div>
            </div>
        </ItemTemplate>
    </asp:DataList>
    <div style="padding: 5px">
    </div>
    <table border="0" cellspacing="0" cellpadding="0" width="99%">
        <tr>
            <td>
                <div class="box-left">
                    <h4>Tin đọc nhiều nhất</h4>
                    <div class="bd">
                        <telerik:RadGrid ID="grdRead" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                            EnableEmbeddedSkins="false" BorderWidth="0" EnableViewState="false" GridLines="None"
                            Skin="None" ShowHeader="false" OnNeedDataSource="grdRead_NeedDataSource">
                            <MasterTableView>
                                <NoRecordsTemplate>
                                    &nbsp;</NoRecordsTemplate>
                                <Columns>
                                    <telerik:GridTemplateColumn>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="HyperLink1" Text='<%# Eval("Subject") & " (" & Eval("viewcount") & ")" %>'
                                                NavigateUrl='<%# Eval("Id", "~/News.aspx?id={0}") %>' runat="server" CssClass="thead"></asp:HyperLink>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                            </MasterTableView>
                        </telerik:RadGrid>
                    </div>
                </div>
            </td>
            <td style="width: 2px">
            </td>
            <td>
                <div class="box-right">
                    <h4>Tin mới nhận</h4>
                    <div class="bd">
                        <telerik:RadGrid ID="grdLatest" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                            BorderWidth="0" EnableViewState="false" GridLines="None" Skin="None" ShowHeader="false"
                            EnableEmbeddedSkins="false" OnNeedDataSource="grdLatest_NeedDataSource">
                            <MasterTableView>
                                <NoRecordsTemplate>
                                    &nbsp;</NoRecordsTemplate>
                                <Columns>
                                    <telerik:GridTemplateColumn>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="HyperLink2" Text='<%# Eval("Subject") %>' NavigateUrl='<%# Eval("Id", "~/News.aspx?id={0}") %>'
                                                runat="server" CssClass="thead"></asp:HyperLink>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                            </MasterTableView>
                        </telerik:RadGrid>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</div>
