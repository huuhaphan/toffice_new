Imports System
Imports System.Collections.Generic
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports Telerik.Web.UI
Imports Telerik.Web.UI.Calendar

Partial Class Calendar
    Inherits SecurePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            RequestAuthorize()

            Me.RadScheduler1.DataSource = Common.GetSchedule()
            RadScheduler1.DataBind()
        End If
    End Sub

    Protected Sub RadScheduler1_DataBound(ByVal sender As Object, ByVal e As EventArgs)
        For Each resType As ResourceType In RadScheduler1.ResourceTypes
            resType.AllowMultipleValues = False
        Next
    End Sub

    Protected Sub RadScheduler1_AppointmentDelete(ByVal sender As Object, ByVal e As Telerik.Web.UI.SchedulerCancelEventArgs) Handles RadScheduler1.AppointmentDelete
        Common.DeleteAppointment(e.Appointment.ID)
    End Sub

    Protected Sub RadScheduler1_AppointmentInsert(ByVal sender As Object, ByVal e As Telerik.Web.UI.SchedulerCancelEventArgs) Handles RadScheduler1.AppointmentInsert
        If e.Appointment.Subject = String.Empty Then
            e.Cancel = True
        Else
            Common.insertAppointment(e.Appointment.Subject, e.Appointment.Start, e.Appointment.End)
        End If

    End Sub

    Protected Sub RadScheduler1_AppointmentUpdate(ByVal sender As Object, ByVal e As Telerik.Web.UI.AppointmentUpdateEventArgs) Handles RadScheduler1.AppointmentUpdate
        Common.UpdateAppointment(e.Appointment.ID, e.Appointment.Subject, e.Appointment.Start, e.Appointment.End)
    End Sub
End Class
