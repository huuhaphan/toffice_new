Imports System.Data
Partial Public Class _News
    Inherits SecurePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim result As Boolean = False
            If IsAuthenticated() Then
                result = IsInRole("NEWS")
            End If
            RadToolBar1.Items(1).Enabled = result

            Dim nid As Integer = CInt(Common.GetQueryString("Id"))
            If nid > 0 Then
                RadGrid.DataSource = News.GetNewsDetail(nid)
                RadGrid.DataBind()
                News.UpdateViewCount(nid)
            End If
        End If
    End Sub

    Protected Sub RadToolBar1_ButtonClick(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadToolBarEventArgs) Handles RadToolBar1.ButtonClick
        If e.Item.Value = "Delete" Then
            News.DeleteNews(CInt(Common.GetQueryString("Id")))
            Response.Redirect("Default.aspx")
        End If
    End Sub
End Class