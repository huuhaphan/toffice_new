Imports Telerik.Web.UI

Partial Class Advise
    Inherits SecurePage

    Public Sub GridView_NeedDataSource(ByVal source As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs)
        GridView.DataSource = Common.GetAdviseList()
    End Sub

    Protected Sub RadGrid1_DeleteCommand(ByVal source As Object, ByVal e As Telerik.Web.UI.GridCommandEventArgs)
        If Common.IsInRole("ADVS") Then
            Dim pid As Integer = e.Item.OwnerTableView.DataKeyValues(e.Item.ItemIndex)("Id")
            If pid <> 0 Then
                News.DeleteTemplate(pid)
                GridView.DataSource = Nothing
            End If
        End If
    End Sub
End Class
