﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Template.Master"
    Inherits="Authorize" CodeFile="Authorize.aspx.vb" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">        
        <script type="text/javascript">
            function ToolBarClicking(sender, args)
            {
                if (args.get_item().get_value() == "Delete") {if (!confirm("Bạn thực sự muốn xóa ủy quyền này?")) {args.set_cancel(true);}}
            } 

        </script>
    </telerik:RadCodeBlock>

    <telerik:RadToolBar ID="RadToolBar1" runat="server" OnButtonClick="RadToolBar1_ButtonClick" OnClientButtonClicking="ToolBarClicking">
        <Items>
            <telerik:RadToolBarButton ImageUrl="~/images/new.png" Text="Tạo mới" ToolTip="Tạo mới" Value="New" NavigateUrl="AuthorizeEdit.aspx" PostBack="False" runat="server" />
            <telerik:RadToolBarButton ImageUrl="~/images/delete.gif" Text="Xóa" Value="Delete" ToolTip="X&#243;a" runat="server" />
        </Items>
        <CollapseAnimation Duration="200" Type="OutQuint" />
    </telerik:RadToolBar>
    <div style="padding-top:1px"></div>
    <telerik:RadGrid ID="RadGrid1" runat="server" AllowPaging="True"
        AllowSorting="True" AutoGenerateColumns="False" GridLines="None" OnNeedDataSource="RadGrid1_NeedDataSource" AllowMultiRowSelection="True">
        <MasterTableView DataKeyNames="Authorize_Id" ShowHeadersWhenNoRecords="false">
            <NoRecordsTemplate><div style="padding:10px;">Hiện tại không có uỷ quyền</div></NoRecordsTemplate>
            <Columns>
                <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn" HeaderStyle-Width="25px" />
                <telerik:GridTemplateColumn SortExpression="receiverid" HeaderText="Ủy quyền cho" UniqueName="receiverid" >
                    <ItemTemplate>
                        <asp:hyperlink ID="EditLink" Text='<%#Common.GetUserName(Eval("receiverid"))%>' NavigateUrl='<%# "~/AuthorizeEdit.aspx?id=" & eval("Authorize_Id") %>'  runat="server"></asp:hyperlink>
                    </ItemTemplate>                  
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn SortExpression="FromDate" HeaderText="Từ ngày" DataField="FromDate"
                    DataFormatString="{0:dd/MM/yyyy}" UniqueName="FromDate">
                    <HeaderStyle Width="75px"></HeaderStyle>
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn SortExpression="ToDate" HeaderText="Đến ngày" DataField="ToDate"
                    DataFormatString="{0:dd/MM/yyyy}" UniqueName="ToDate">
                    <HeaderStyle Width="75px"></HeaderStyle>
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn SortExpression="Level" HeaderText="Mức độ" DataField="Level" HeaderStyle-Width="60px" UniqueName="Subject" />
                <telerik:GridBoundColumn SortExpression="RecDate" HeaderText="Ngày tạo" DataField="recdate"
                    DataFormatString="{0:dd/MM/yyyy}" UniqueName="RecDate">
                    <HeaderStyle Width="75px"></HeaderStyle>
                </telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
        <SortingSettings SortToolTip="Nhấn vào đây để sắp xếp"></SortingSettings>
        <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True" Selecting-AllowRowSelect="true" EnableRowHoverStyle="true" />
    </telerik:RadGrid>
    <div style="padding:5px; font-weight:bold">
        <span>Mức độ: 1 - Toàn bộ công văn, 2 - Công văn mật, 3 - Công văn bình thường</span>
    </div>
</asp:Content>
