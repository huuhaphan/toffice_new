﻿Imports System
Imports System.Collections.Generic
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports Telerik.Web.UI
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared

Partial Class Calendas
    Inherits SecurePage
    Dim cryRpt As New ReportDocument
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            RequestAuthorize()
            LoadDropDown_Calendas()
            LoadReport_Calendas()


        End If
    End Sub

    Private Sub LoadDropDown_Calendas()
        If Not Page.IsPostBack Then
            ddlWeek.DataSource = _Calendas.GetCalendas()
            ddlWeek.DataTextField = "name"
            ddlWeek.DataValueField = "id"
            ddlWeek.DataBind()
        End If
        ' ...
    End Sub

    Private Sub LoadReport_Calendas()
        If Not Page.IsPostBack Then

            Dim crtableLogoninfos As New TableLogOnInfos
            Dim crtableLogoninfo As New TableLogOnInfo
            Dim crConnectionInfo As New ConnectionInfo
            Dim CrTables As Tables
            Dim CrTable As CrystalDecisions.CrystalReports.Engine.Table


            cryRpt.Load(Server.MapPath("~\RPT\rptlichcongtac.rpt"))
            cryRpt.SetParameterValue("Id", ddlWeek.SelectedValue)

            With crConnectionInfo
                .ServerName = Common.Server '"dbazure.vsoftgroup.com"
                .DatabaseName = Common.DataBase ' "CangVu_Document"
                .UserID = Common.DataBaseUser ' "ucangvu"
                .Password = Common.DataBasePass ' "pcangvu"
            End With

            CrTables = cryRpt.Database.Tables
            For Each CrTable In CrTables
                crtableLogoninfo = CrTable.LogOnInfo
                crtableLogoninfo.ConnectionInfo = crConnectionInfo
                CrTable.ApplyLogOnInfo(crtableLogoninfo)
            Next

            CrystalReportViewer1.ReportSource = cryRpt
            CrystalReportViewer1.Zoom(65)
            CrystalReportViewer1.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None

        End If
    End Sub
    Protected Sub ddlWeek_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlWeek.SelectedIndexChanged


        Dim crtableLogoninfos As New TableLogOnInfos
        Dim crtableLogoninfo As New TableLogOnInfo
        Dim crConnectionInfo As New ConnectionInfo
        Dim CrTables As Tables
        Dim CrTable As CrystalDecisions.CrystalReports.Engine.Table

        'CrystalReportViewer1.RefreshReport()

        cryRpt.Load(Server.MapPath("~\RPT\rptlichcongtac.rpt"))
        cryRpt.SetParameterValue("Id", ddlWeek.SelectedValue)

        With crConnectionInfo
            .ServerName = Common.Server '"dbazure.vsoftgroup.com"
            .DatabaseName = Common.DataBase ' "CangVu_Document"
            .UserID = Common.DataBaseUser ' "ucangvu"
            .Password = Common.DataBasePass ' "pcangvu"
        End With

        CrTables = cryRpt.Database.Tables
        For Each CrTable In CrTables
            crtableLogoninfo = CrTable.LogOnInfo
            crtableLogoninfo.ConnectionInfo = crConnectionInfo
            CrTable.ApplyLogOnInfo(crtableLogoninfo)
        Next

        CrystalReportViewer1.ReportSource = cryRpt

        CrystalReportViewer1.Zoom(65)
        CrystalReportViewer1.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None

    End Sub

    Protected Sub Page_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Unload
        cryRpt.Close()
        cryRpt.Dispose()
    End Sub
End Class
