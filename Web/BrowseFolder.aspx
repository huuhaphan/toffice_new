﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/InnerTemplate.Master"
    CodeFile="BrowseFolder.aspx.vb" Inherits="BrowseFolder" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            var msgid, msgtype, postid
            var msgidover, msgtype, postidover

            function RowSelected(sender, args) {
                msgid = args.getDataKeyValue("Source_Id");
                msgtype = args.getDataKeyValue("Type");
                postid = args.getDataKeyValue("PersonalBox_Id");
                $find("<%= RadAjaxManager1.ClientID %>").ajaxRequest(msgtype + msgid);
            }

            function RowDblClick(sender, args) {
                var page = (msgtype == "ML") ? "viewmail.aspx" : "viewdoc.aspx";
                openWindow(getUrl(page, "ML", "", msgid, postid));
            }

            function RowMouseOver(sender, args) {
                msgidover = args.getDataKeyValue("Source_Id");
                postidover = args.getDataKeyValue("PersonalBox_Id");
                msgtypeover = args.getDataKeyValue("Type");
            }

            function ToolBarClicking(sender, args) {
                if (args.get_item().get_value() == "Delete") {
                    if (!confirm("Bạn thực sự muốn xóa tin này?")) {
                        args.set_cancel(true);
                    }
                }
            }

            function RowContextMenu(sender, args) {
                var menu = $find("<%=RadMenu1.ClientID %>");
                var evt = args.get_domEvent();
                menu.show(evt);

                evt.cancelBubble = true;
                evt.returnValue = false;

                if (evt.stopPropagation) {
                    evt.stopPropagation();
                    evt.preventDefault();
                }
            }

            function ContextMenuClicking(sender, args) {
                var item = args.get_item();
                var itemvalue = item.get_value();
                if (itemvalue == "Reply" || itemvalue == "Forward") {
                    if (!(itemvalue == "Reply" && msgtype == "DO")) {
                        item.set_navigateUrl(getUrl("compose.aspx", msgtypeover, itemvalue, msgidover));
                    }
                }
                else if (itemvalue == "Open") {
                    var page = (msgtype == "ML") ? "viewmail.aspx" : "viewdoc.aspx";
                    item.set_navigateUrl(getUrl(page, "", "", msgidover, postidover));
                }
                else if (itemvalue == "Delete") {
                    if (!confirm("Bạn thực sự muốn xóa thư này ?")) { args.set_cancel(true); }
                }
            }
       
        </script>

    </telerik:RadCodeBlock>
    <telerik:RadSplitter ID="RadSplitter1" runat="server" Width="100%" Orientation="Horizontal"
        Height="100%" BorderStyle="None" PanesBorderSize="0">
        <telerik:RadPane ID="ToolbarPane" runat="server" Height="30px" Scrolling="None">
            <telerik:RadToolBar ID="RadToolBar1" runat="server" OnClientButtonClicking="ToolBarClicking" OnButtonClick="RadToolBar1_ButtonClick" CssClass="toolbar">
                <Items>
                    <telerik:RadToolBarButton runat="server" ImageUrl="~/images/delete.gif" Value="Delete" ToolTip="Xóa thư" />
                    <telerik:RadToolBarDropDown runat="server" ImageUrl="~/images/movetofolder.gif" Text="Thư mục" ToolTip="Di chuyển đến thư mục" />
                </Items>
            </telerik:RadToolBar>
        </telerik:RadPane>
        <telerik:RadPane ID="GridPane" runat="server" Scrolling="None">
            <telerik:RadGrid ID="RadGrid1" runat="server" Height="100%" BorderWidth="0"
                AutoGenerateColumns="False" OnNeedDataSource="RadGrid1_NeedDataSource" ShowGroupPanel="True"
                AllowMultiRowSelection="True" AllowSorting="True" AllowPaging="true" PageSize="20">
                <MasterTableView DataKeyNames="PersonalBox_Id" ClientDataKeyNames="PersonalBox_Id,Source_Id,Type" GroupLoadMode="Client">
                    <NoRecordsTemplate>
                        <div class="NoRecord">Không có công văn và thư trong thư mục này</div>
                    </NoRecordsTemplate>
                    <Columns>
                        <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn" HeaderStyle-Width="27px" />
                        <telerik:GridBoundColumn SortExpression="From" HeaderText="Người gửi" DataField="From"
                            UniqueName="From" HeaderStyle-Width="180px" />
                        <telerik:GridBoundColumn SortExpression="Subject" HeaderText="Chủ đề" DataField="Subject"
                            UniqueName="Subject" HeaderStyle-Width="270px" ItemStyle-Wrap="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="Sendtime" HeaderText="Ngày nhận" DataField="Sendtime"
                            DataFormatString="{0:dd/MM/yyyy}" UniqueName="Received" HeaderStyle-Width="75px" />
                        <telerik:GridTemplateColumn SortExpression="AttachFileSize" HeaderText="Kích cỡ"
                            UniqueName="AttachFileSize" ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="65px">
                            <ItemTemplate><%# Mails.ToByteString(Eval("AttachFileSize"))%></ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn UniqueName="Attach" HeaderText="" Groupable="False" HeaderStyle-Width="27px">
                            <ItemTemplate><%# IIF(Eval("AttachFile")="X","<img src='images/Attch.gif'>","&nbsp;") %></ItemTemplate>
                        </telerik:GridTemplateColumn>
                    </Columns>
                </MasterTableView>
                <ClientSettings ReorderColumnsOnClient="True" AllowDragToGroup="True" AllowColumnsReorder="True"
                    EnableRowHoverStyle="true">
                    <Selecting AllowRowSelect="True"></Selecting>
                    <Scrolling AllowScroll="True" UseStaticHeaders="True"></Scrolling>
                    <ClientEvents OnRowContextMenu="RowContextMenu" OnRowSelected="RowSelected" OnRowDblClick="RowDblClick" OnRowMouseOver="RowMouseOver"/>
                </ClientSettings>
                <GroupPanel Text="Nh&#243;m theo t&#234;n cột" ToolTip="Đưa con trỏ v&#224;o &#244; t&#234;n cột, nhấn v&#224; giữ nguy&#234;n n&#250;t tr&#225;i chuột sau đ&#243; k&#233;o v&#224; thả v&#224;o &#244; n&#224;y" />
                <PagerStyle Mode="NextPrevAndNumeric" NextPagesToolTip="" NextPageToolTip="Trang kế"
                    PageButtonCount="5" PagerTextFormat="Trang {0}/{1} {4}" PrevPagesToolTip="" PrevPageToolTip="Trang trước" />
            </telerik:RadGrid>
        </telerik:RadPane>
        <telerik:RadSplitBar ID="RadSplitBar1" runat="server" CollapseMode="Both" />
        <telerik:RadPane ID="ContentPane" runat="server" Height="150px">
            <asp:Panel ID="MessagePanel" runat="server">
                <div class="messagetitle">
                    <asp:Label ID="lblSubject" runat="server" Font-Bold="true" />
                    <br />
                    <asp:Label ID="lblFrom" runat="server" Font-Bold="true" />
                    <asp:Label ID="lblSendTime" runat="server" CssClass="rightcol" />
                    <br />
                    <asp:Label ID="lblTo" runat="server" />
                    <br />
                    <asp:Label ID="lblAttachment" runat="server" />
                </div>
                <div class="messagebody">
                    <asp:Label ID="lblBody" runat="server"></asp:Label>
                </div>
            </asp:Panel>
        </telerik:RadPane>
    </telerik:RadSplitter>

    <telerik:RadAjaxManager ID="RadAjaxManager1" DefaultLoadingPanelID="LoadingPanel1"
        OnAjaxRequest="RadAjaxManager1_AjaxRequest" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="MessagePanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadMenu1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                    <telerik:AjaxUpdatedControl ControlID="RadMenu1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadToolBar1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                    <telerik:AjaxUpdatedControl ControlID="MessagePanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server" />

    <telerik:RadContextMenu ID="RadMenu1" runat="server" OnItemClick="RadMenu1_ItemClick"
        OnClientItemClicking="ContextMenuClicking">
        <Items>
            <telerik:RadMenuItem Text="Xem nội dung" runat="server" Value="Open" NavigateUrl="#"
                Target="_blank" PostBack="false" />
            <telerik:RadMenuItem IsSeparator="true" />
            <telerik:RadMenuItem Text="Trả lời" runat="server" ImageUrl="images/reply.gif" Value="Reply"
                NavigateUrl="#" Target="_blank" PostBack="false" />
            <telerik:RadMenuItem Text="Chuyển tiếp" runat="server" ImageUrl="images/forward.gif"
                Value="Forward" NavigateUrl="#" Target="_blank" PostBack="false" />
            <telerik:RadMenuItem Text="Xóa" runat="server" Value="Delete" />
            <telerik:RadMenuItem IsSeparator="true" />
            <telerik:RadMenuItem Text="Xóa tất cả" runat="server" Value="DeleteAll" />
        </Items>
        <CollapseAnimation Duration="200" Type="OutQuint" />
    </telerik:RadContextMenu>
</asp:Content>
