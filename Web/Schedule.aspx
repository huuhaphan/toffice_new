﻿<%@ Page Language="VB" MasterPageFile="~/Template.Master"  AutoEventWireup="false" CodeFile="Schedule.aspx.vb" Inherits="Schedule" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" Namespace="CrystalDecisions.Web" TagPrefix="CR" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
  
    <asp:Label ID="Label2" runat="server" Text="Thời gian" CssClass="cardLabel" AssociatedControlID="RadDateInput1"></asp:Label>
    <telerik:RadDatePicker ID="RadDateInput1" runat="server" DateSelectionMode="Month"></telerik:RadDatePicker>
    <asp:Button ID="Button1" runat="server" Text="Xem" />
    <div id="parentrptlichtrucban">
    <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" AutoDataBind="true"
        HasRefreshButton="True" ToolPanelView="None" EnableDatabaseLogonPrompt="False" EnableParameterPrompt="False" ReuseParameterValuesOnRefresh="True" />
    
    </div>
    <script type="text/javascript">
        $('.parentrpt-lichtrucban').css({ 'overflow': '!important' });
        setTimeout(function () {
            document.getElementById("parentrptlichtrucban").className = "parentrpt-lichtrucban";
                //$(".parentrpt-lichtrucban").css({ 'overflow': 'scroll !important' });
                //alert('s');
            }, 1);
    </script>
</asp:Content>

