Imports System.Data
Imports Telerik.Web.UI

Partial Public Class BrowseFolder
    Inherits SecurePage

    Private ReadOnly Property Folder() As String
        Get
            Return Common.GetQueryString("Id")
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If Not IsAuthenticated() Then
                ErrHandler.ShowError(401)
            End If
        End If
        Common.BuildToolbar(RadToolBar1, Common.UserId, "FD", Folder)
    End Sub

    Public Sub RadGrid1_NeedDataSource(ByVal source As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs)
        RadGrid1.DataSource = TaskFolders.GetTaskList(Common.UserId, Folder)
    End Sub

    Protected Sub RadAjaxManager1_AjaxRequest(ByVal sender As Object, ByVal e As Telerik.Web.UI.AjaxRequestEventArgs)
        'Refresh
        Dim ref As String = e.Argument
        RefreshDetail(CInt(ref.Substring(2)), Left(ref, 2))
    End Sub

    Sub RefreshDetail(Optional ByVal MsgId As Integer = -1, Optional ByVal MsgType As String = "")
        If MsgId > 0 And MsgType <> "" Then
            Dim objDetail As Object = IIf(MsgType = "ML", Mails.GetMailDetail(MsgId), Documents.GetDocumentDetail(MsgId))
            lblFrom.Text = objDetail.FromUser
            lblTo.Text = objDetail.ToUser
            lblSubject.Text = Server.HtmlEncode(objDetail.Subject)
            lblSendTime.Text = objDetail.SendTime
            lblAttachment.Text = objDetail.Attachment
            lblBody.Text = objDetail.Body
        Else
            lblFrom.Text = ""
            lblTo.Text = ""
            lblSubject.Text = ""
            lblSendTime.Text = ""
            lblAttachment.Text = ""
            lblBody.Text = ""
        End If
    End Sub

    Protected Sub RadToolBar1_ButtonClick(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadToolBarEventArgs)
        RequestAuthorize()
        Select Case e.Item.Value
            Case "Inbox"
                MoveToFolder("IN")
            Case "Delete"
                MoveToFolder("DL")
            Case Else
                If Left(e.Item.Value, 2) = "FD" Then
                    MoveToFolder(e.Item.Value.Trim)
                End If
        End Select
    End Sub

    Protected Sub RadMenu1_ItemClick(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadMenuEventArgs)
        RequestAuthorize()
        Select Case e.Item.Value
            Case "Delete"
                MoveToFolder("DL")
            Case "DeleteAll"
                Common.MoveAllMessage("", "", "DL")
                RadGrid1.Rebind()
                RefreshDetail()
        End Select
    End Sub

    Sub MoveToFolder(ByVal newvalue As String)
        If RadGrid1.SelectedItems.Count > 0 AndAlso RadGrid1.SelectedItems.Count <= RadGrid1.PageSize Then
            Dim PersonalBox_Id As Integer
            Dim User_Id As String = Common.UserId

            For Each dataItem As GridDataItem In RadGrid1.SelectedItems
                PersonalBox_Id = dataItem.GetDataKeyValue("PersonalBox_Id")
                If PersonalBox_Id <> 0 Then
                    Common.MoveMessage(PersonalBox_Id, newvalue, False)
                End If
            Next
            RadGrid1.Rebind()
            RefreshDetail()
        End If
    End Sub
End Class