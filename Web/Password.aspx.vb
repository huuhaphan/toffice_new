Partial Class Password
    Inherits SecurePage

    Protected Sub btnEnter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEnter.Click
        If txtNewPwd.Text = txtConfirm.Text Then
            Dim user As New IUser
            Call user.ChangePassword(Session("LoginName"), txtOldPwd.Text, txtNewPwd.Text)
            If Not String.IsNullOrEmpty(user.ErrorCode) Then
                lblError.Visible = True
                lblError.Text = user.ErrorCode
            Else
                Response.Redirect("logout.aspx")
            End If
        Else
            lblError.Visible = True
        End If
    End Sub
End Class
