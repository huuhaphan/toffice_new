﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Template.Master"
    CodeFile="BrowseNews.aspx.vb" Inherits="BrowseNews" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="componentheading"><asp:Label runat="server" ID="lblTitle" Text=""></asp:Label></div>
    <div class="news">
        <telerik:RadGrid ID="RadGrid1" runat="server" AllowPaging="True" AutoGenerateColumns="False"
            BorderWidth="0" GridLines="None" ShowHeader="false" PagerStyle-Mode="NextPrevAndNumeric"
            OnNeedDataSource="RadGrid1_NeedDataSource" PageSize="20">
            <MasterTableView>
                <NoRecordsTemplate>Không tìm thấy kết quả nào</NoRecordsTemplate>
                <Columns>
                    <telerik:GridTemplateColumn>
                        <ItemTemplate>
                            <asp:HyperLink ID="HyperLink" Text='<%# Eval("Subject") %>' NavigateUrl='<%# Eval("Id", "~/News.aspx?id={0}") %>' runat="server" CssClass="thead1" /><br />
                            <span style="color:#4C4C4C">Ngày cập nhật:<asp:Literal ID="Literal2" runat="server" Text='<%# Format(Eval("recdate"),"dd/MM/yyyy") %>' /></span>
                            <br />
                            <div class ="news_separator">
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                </Columns>
                <PagerStyle Mode="NextPrevAndNumeric" PageSizeLabelText="Số tin hiển thị" NextPagesToolTip="" NextPageToolTip="Trang kế"
                    PageButtonCount="5" PagerTextFormat="Trang {0}/{1} {4} Tổng cộng {5} mẫu" PrevPagesToolTip=""
                    PrevPageToolTip="Trang trước" LastPageToolTip="Trang cuối" FirstPageToolTip="Trang đầu" />
            </MasterTableView>
        </telerik:RadGrid>
    </div>
</asp:Content>
