Partial Public Class [Error]
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim errcode As Integer = CInt(Request.QueryString("code"))
        Dim errmsg As String = ""
        Select Case errcode
            Case 101
                errmsg = "Cập nhật dữ liệu không thành công."
            Case 102
                errmsg = "Không thể xóa mẫu tin này."
            Case 401
                errmsg = "Bạn không có quyền truy cập vào chức năng này."
            Case 404
                errmsg = "The requested page or resource was not found."
            Case 408
                errmsg = "The request timed out. This may be caused by a too high traffic. Please try again later."
            Case 505
                errmsg = "The server encountered an unexpected condition which prevented it from fulfilling the request. Please try again later."
            Case 601
                errmsg = "Tham số truyền vào không hợp lệ."
            Case 602
                errmsg = "Hộp thư đã đầy. Bạn phải xóa thư cũ để tiếp tục."
            Case 603
                errmsg = "Tệp đính kèm không tìm thấy."
            Case 604
                errmsg = "Bạn không có quyền truy xuất tệp đính kèm này."
            Case Else
                errmsg = "Hiện tại trang web này đang có lỗi.<br> Nếu bạn thường xuyên gặp câu thông báo này xin vui lòng gửi mail cho tổ CNTT. "
        End Select
        lblError.Text = errmsg
    End Sub
End Class