Imports Telerik.Web.UI
Imports System.Data
Partial Class AuthorizeEdit
    Inherits SecurePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If Not IsAuthenticated() Then
                ErrHandler.ShowError(401)
            End If

            Dim AuthorizeId As Integer = CInt(Common.GetQueryString("Id"))
            If AuthorizeId <> 0 Then
                Using dr As IDataReader = Documents.GetAuthorizeByID(AuthorizeId)
                    While dr.Read()
                        rdpFrDate.SelectedDate = dr("Fromdate")
                        rdpToDate.SelectedDate = dr("Todate")
                        rcbUser.Text = Common.GetUserName(dr("receiverid"))
                        ddlLevel.Text = dr("level")
                    End While
                End Using
            End If
        End If
    End Sub

    Protected Sub cmdSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        Dim AuthorizeId As Integer = CInt(Common.GetQueryString("Id"))
        If Not String.IsNullOrEmpty(rcbUser.SelectedValue) Then
            Documents.UpdateAuthorize(AuthorizeId, rdpFrDate.SelectedDate, rdpToDate.SelectedDate, ddlLevel.Text, rcbUser.SelectedValue.Substring(1))
            Response.Redirect("Authorize.aspx")
        End If
    End Sub

    Protected Sub cmdExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdExit.Click
        Response.Redirect("Authorize.aspx")
    End Sub
    Protected Sub rcbUser_ItemsRequested(ByVal sender As Object, ByVal e As RadComboBoxItemsRequestedEventArgs)
        Using dr As IDataReader = Common.GetUserListByName(e.Context("CustomText").ToString(), True)
            rcbUser.DataSource = dr
            rcbUser.DataValueField = "LoginID"
            rcbUser.DataTextField = "FullName"
            rcbUser.DataBind()
        End Using
    End Sub

End Class
