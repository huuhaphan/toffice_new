mOffice.document = (function () {
var viewModel = kendo.observable ({
	documentsData: new kendo.data.DataSource(
		{
			pageSize: 30,
			serverPaging: true,
			transport: {
				read: {
					type: "POST",
					url: mOffice.configuration.documentUrl,
					contentType: "application/json; charset=utf-8",
					dataType: "json"
				},				
				parameterMap: function (options, operation) {
					var data = { uid: mOffice.userAccount.userID(), skip: (options.page-1)*options.pageSize, take: options.pageSize}
					return JSON.stringify(data);
				}
			},
			schema: {
				data: "d.Data",
				total: "d.Total",
				model: { id: "PID"}
			}		
		}),
		currentItem: null,
		onInit: function (e) {
			$("#documentList").kendoMobileListView({
				dataSource: viewModel.documentsData,
				endlessScroll: true,
				template: $("#documentTemplate").text(),
				filterable: { field: "Subject", operator: "startswith"}
			});
		},
		showDetailsView: function (e) {
			var id = parseInt(e.view.params.id), item = viewModel.documentsData.get(id);
			viewModel.set("currentItem", item);
		},
		onOpen: function (e) {
			var item = viewModel.documentsData.get(parseInt(e.context));
			currentItem = item;
			//this.element.find(".km-actionsheet-title").text(e.target.next().text());
		},
		forward: function (e) {
			$("#documentResult").html("Chuy?n ti?p c�ng van #" + e.context);
		},
		deleteDocument: function (e) {
			$("#documentResult").html("X�a c�ng van #" + e.context);
		}
	});
	
    return {
        viewModel: viewModel
    }
})();