﻿
mOffice.main = (function () {
    var application;

    function getApplication() {
        return application;
    }

    function initializeApp() {

        //initialize app
        application = new kendo.mobile.Application(document.body,
         {             
             transition: 'slide',
             loading: "<h3>Loading...</h3>",
			 skin:"flat"
         });
    }

    return {                
        initializeApp: initializeApp,
        getKendoApplication: getApplication        
    }
})();