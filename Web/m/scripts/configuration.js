﻿mOffice.configuration = (function () {

    var serviceUrl = "http://web.tiags.vn/";

    return {            
        serviceUrl: serviceUrl,
        accountUrl: serviceUrl + "wsOffice/service.svc/GetUsers",
		mailUrl: serviceUrl + "wsOffice/service.svc/GetMails",
		mailAttachmentUrl: serviceUrl + "toffice/mobile/WebService.asmx/GetMailAttachment",
		documentUrl: serviceUrl + "toffice/mobile/WebService.asmx/GetDocuments",
		docAttachmentUrl: serviceUrl + "toffice/mobile/WebService.asmx/GetDocAttachment"
    }
})();