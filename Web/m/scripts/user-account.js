﻿mOffice.userAccount = (function () {
    //ViewModel for User Account view
    var viewModel = kendo.observable({
        isUserLoggedIn: false,
		userID: "",
        fullName: "",
        userName: "", 
        password: "",
        userLogin: function () {
			//this.logOff();
			
			var loginOptions = {
                url: mOffice.configuration.accountUrl,
				data: {usercode:this.userName, password:this.password} ,
                requestType: "GET",
                dataType: "JSON",
                callBack: this.fnLoginCallBack
            };
            mOffice.dataAccess.callService(loginOptions);

        },
        //method for user login
        fnLoginCallBack: function (result) {
            if (result.success === true) {
				viewModel.set("userID", result.data.UserID);
				$.sessionStorage.setItem("userID", result.data.UserID);
                viewModel.set("fullName", result.data.FullName);
                viewModel.set("isUserLoggedIn", true);

                mOffice.common.showLogOffButton();
				kendo.history.navigate("#:back");				
            } else {
                //any error handling code
            }
        },		
        //method called when log off button is clicked
        logOff: function () {
            console.log('inside logOff');
            viewModel.set("fullName", "");
            viewModel.set("isUserLoggedIn", false);
			$.sessionStorage.clear()
            //hide log off button
            mOffice.common.hideLogOffButton();
            
        }
    });

	function isAuthenticated (e) {
		var usrId = getUID();
		alert( usrId);
		//if (viewModel.userID == "") {
		if (usrId == "") {
			//e.preventDefault();
			kendo.history.navigate(e.view.id);
            //navigate to User Account screen.
            mOffice.common.navigateToView("UserAccount.html");
		}
	}
	
	function getUID() {
		//return viewModel.userID;
		return $.sessionStorage.getItem("userID")
	}
	
    return {
        viewModel: viewModel,
		userID: getUID,
		isAuthenticated: isAuthenticated
    }
})();