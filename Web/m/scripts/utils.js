﻿
function convertDateJToDateJSON(input) {
	var d = new Date(input);
	if (isNaN(d)) {
		throw new Error("input not date");
	}
	// here is how we force wcf to parse as UTC and give correct local time serverside        
	var date = '\/Date(' + d.getTime() + '-0000)\/';
	return date;
}

function convertJSONDateToDate(input, throwOnInvalidInput) {
	var pattern = /Date\(([^)]+)\)/;
	var results = pattern.exec(input);
	if (results.length != 2) {
		if (!throwOnInvalidInput) {
			return s;
		}
		throw new Error(s + " is not .net json date.");
	}
	return new Date(parseFloat(results[1]));
}

function formatJSONDate(input,format) {
	var pattern = /Date\(([^)]+)\)/;
	var results = pattern.exec(input);
	if (results.length != 2) {
		if (!throwOnInvalidInput) {
			return s;
		}
		throw new Error(s + " is not .net json date.");
	}
	return $.format.date(new Date(parseFloat(results[1])),format);
}
	
function writeCookie(name,value,days) {
    var date, expires;
    if (days) {
        date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        expires = "; expires=" + date.toGMTString();
            }else{
        expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";
	alert("Write Cookie done");
}

function readCookie(name) {
    var i, c, ca, nameEQ = name + "=";
    ca = document.cookie.split(';');
    for(i=0;i < ca.length;i++) {
        c = ca[i];
        while (c.charAt(0)==' ') {
            c = c.substring(1,c.length);
        }
        if (c.indexOf(nameEQ) == 0) {
            return c.substring(nameEQ.length,c.length);
        }
    }
    return '';
}

function env() { 
	var b = navigator.userAgent;
	var a = {};
	a.RIM = RegExp("BlackBerry").test(b);
	a.RIM6 = a.RIM && RegExp("Version/6").test(b);
	a.IE = RegExp("MSIE ").test(b);
	a.OperaMobile = RegExp("Opera Mobi").test(b);
	a.Webkit = RegExp(" AppleWebKit/").test(b);
	a.Apple = RegExp("iPhone").test(b) || RegExp("iPad").test(b) || RegExp("iPod").test(b);
	a.iPhoneNew = a.Apple && RegExp("OS (\\d+)").test(b) && RegExp.$1 >= 3;
	a.Android = RegExp("Android").test(b);
	a.Android2 = RegExp("Android 2").test(b);
	a.WebOS = RegExp("webOS").test(b);
	return a;
}
