mOffice.mail = (function () {
	var viewModel = kendo.observable({
		mailsData: new mOffice.dataAccess.EndlessScrollDataSource({
			pageSize: 30,
			serverPaging: true,
			serverFiltering: true,
            serverSorting: true,
			sort: {
                field: "Subject",
                dir: "desc"
            },
			transport: {
				read: {
					url: mOffice.configuration.mailUrl,
					dataType: "json",
					contentType: "application/json; charset=utf-8",
					type: "POST"
				},
				parameterMap: function (options, operation) {
					//var data = { uid: mOffice.userAccount.userID(), skip: (options.page-1)*options.pageSize, take: options.pageSize}
					var data = { uid: mOffice.userAccount.userID(), request: { skip: (options.page-1)*options.pageSize, take: options.pageSize, page: options.page, sort: options.sort}}
					return JSON.stringify(data);
				}
			},
			error: function (e) {
				console.log(e.responseText);
			},
			schema: {
				data: "Data",
				total: "Total",
				model: { id: "PID" }
			}
		}),
		currentItem: null,
		mailto: [],
		mailbc: [],
		mailcc: [],
		showDetailsView: function (e) {
			var id = parseInt(e.view.params.id);
			var item = viewModel.mailsData.get(id);
			viewModel.set("currentItem", item);
			if (item !== null) {
				//viewModel.mailAttachment(item.MailID);
			}
						
			// var frame = $("#ifrm")[0].contentWindow.document;  			
			// frame.open();
			// frame.write(item.Content);
			// frame.close();
		},
		onInit: function (e) {
			$("#mailList").kendoMobileListView({
				dataSource: viewModel.mailsData,
				endlessScroll: true,
				template: $("#mailTemplate").text(),
				filterable: { field: "Subject", operator: "startswith"}
			});
		},
		onOpen: function (e) {
			var item = viewModel.mailsData.get(parseInt(e.context));
			viewModel.set("currentItem", item);
			viewModel.set("mailto", []);
			viewModel.set("mailbc", []);
			viewModel.set("mailcc", []);
		},
		reply: function (e) {
			var item = viewModel.get("currentItem");
			if (item != null) { 
				$("#mailCompose").find(".km-view-title").html("Tr? l?i thu")
				$("#compose-subject").val("RE: " + item.Subject);
				$("#compose-content").html(item.Content);
				var toItem = {id: item.SenderID, name: item.Sender};
				viewModel.mailto.push(toItem);
				app.navigate("#mailCompose");
			}
		},
		replyAll: function (e) {
			var item = viewModel.get("currentItem");
			$("#mailCompose").find(".km-view-title").html("Tr? l?i cho m?i ngu?i")
			$("#compose-subject").val("RE: " + item.Subject);
			app.navigate("#mailCompose");
		},
		forward: function (e) {
			var item = viewModel.get("currentItem");
			$("#mailCompose").find(".km-view-title").html("Chuy?n ti?p thu")
			$("#compose-subject").val("FW: " + item.Subject);
			viewModel.mailto.push(item.Sender);
			app.navigate("#mailCompose");
		},
		deleteMail: function (e) {
			$("#mailResult").html("X�a thu #" + e.context);
		},		
		mailAttachment: function (id) {
			var attachmentOptions = {
				url: mOffice.configuration.mailAttachmentUrl,
				data: {mailid:id} ,
				requestType: "POST",
				dataType: "JSON",
				callBack: this.fnMailAttachmentCallBack
			};
			mOffice.dataAccess.callService(attachmentOptions);
		},	
		//method for mailAttachment
		fnMailAttachmentCallBack: function (result) {
			if (result.success === true) {
				alert(result.data);
				console.log(result.data);
				$("mailAttach").html(result.data);
			} else {
				//any error handling code
			}
		}
	});
	
    return {
        viewModel: viewModel
    }
})();