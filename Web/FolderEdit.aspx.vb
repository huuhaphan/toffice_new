Imports System.Data
Partial Class FolderEdit
    Inherits SecurePage
    Dim TaskId As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

            TaskId = CInt(Common.GetQueryString("Id"))
            If TaskId <> 0 Then
                lblHeading.Text = "Sửa thư mục riêng"
                Using dr As IDataReader = TaskFolders.GetTaskFolderByID(TaskId)
                    If dr.Read() Then
                        txtTaskName.Text = dr("taskname")
                        txtRemark.Text = dr("remark")
                    End If
                End Using
            Else
                lblHeading.Text = "Tạo mới thư mục riêng"
            End If
        End If
    End Sub
    Protected Sub cmdSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        TaskId = CInt(Common.GetQueryString("Id"))
        TaskFolders.UpdateTaskfolder(TaskId, txtTaskName.Text, txtRemark.Text, Common.UserId)
        Response.Redirect("Folder.aspx")
    End Sub

    Protected Sub cmdExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdExit.Click
        Response.Redirect("Folder.aspx")
    End Sub
End Class
