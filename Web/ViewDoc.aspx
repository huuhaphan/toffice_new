﻿<%@ Page Language="VB" MasterPageFile="~/TemplateFull.Master" AutoEventWireup="false" CodeFile="ViewDoc.aspx.vb" Inherits="ViewDoc" title="Xem công văn" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">

<script type="text/javascript">
    //<![CDATA[
    function ToolbarClicking(sender, args)
    {
        if (args.get_item().get_value() == "Delete") {if (!confirm("Bạn thực sự muốn xóa mẫu tin này?")) {args.set_cancel(true);}}
    } 
    //]]>
</script>

<div id="messagewrapper" class="message">
    <telerik:RadToolBar ID="RadToolBar1" runat="server" OnClientButtonClicking="ToolbarClicking" OnButtonClick="RadToolBar1_ButtonClick" Width="100%">
        <Items>
            <%--<telerik:RadToolBarButton ImageUrl="~/images/forward.gif" Text="Chuyển tiếp" Target="_blank"
                Value="Forward" PostBack="False" runat="server" />--%>
            <telerik:RadToolBarButton IsSeparator="true" />             
            <telerik:RadToolBarButton ImageUrl="~/images/delete.gif" Value="Delete" ToolTip="Delete" runat="server"/>
            <telerik:RadToolBarButton ImageUrl="~/images/print.gif" Value="Print" ToolTip="In" runat="server" NavigateUrl="javascript:window.print()" PostBack="false" />
            <telerik:RadToolBarButton IsSeparator="true" />
            <telerik:RadToolBarButton ImageUrl="~/images/prev.png" Value="PrevMsg" ToolTip="Công văn trước" runat="server" PostBack="False"/>
            <telerik:RadToolBarButton ImageUrl="~/images/next.png" Value="NextMsg" ToolTip="Công văn kế" runat="server" PostBack="False"/>
        </Items>
    </telerik:RadToolBar>

    <div class="messagetitle">
        <div class="title-detail-document"><asp:Label ID="Label3" runat="server">Nội dung công văn</asp:Label></div>
    <table cellpadding="2" cellspacing="0" border="0" width="100%">
        <colgroup>
            <col width="90px" />
            <col />
        </colgroup>
        <tr>

            <th style="width:15%;">Số công văn:</th>
            <td>
                <table cellpadding="2" cellspacing="0" border="0" width="100%"> 
                    <tr> 
                        <td><div class="inputfield"><asp:Label ID="lblDocNum" runat="server" /></div> </td>
                        <th style="width:15%;">Lĩnh vực: </th>
                       <td> <div class="inputfield"><asp:Label ID="lblLinhvuc" runat="server" /></div> </td>
                        <th style="width:15%;">
                            Loại công văn
                        </th>
                        <td> <div class="inputfield"><asp:Label ID="lblType" runat="server" /></div> </td>
                </tr>                      
                </table>

            </td>
        </tr>
        <tr>
            <th>Nơi gửi:</th>
            <td><div class="inputfield"><asp:Label ID="lbtFormSend" runat="server" /></div></td>
        </tr>
        <tr>
            <th>Người  kí:</th>
            <td><table cellpadding="2" cellspacing="0" border="0" width="100%">
                <tr>
                    <td style="width:300px;"><div class="inputfield"><asp:Label ID="lblSigner" runat="server" /></div> </td>
                    <th style="width:15%;">Chức vụ: </th><td><div class="inputfield"><asp:Label ID="lbtSignerPos" runat="server" /></div></td>
                </tr>
                </table></td>
            
        </tr>
        <%--<tr>
            <th>Người nhận:</th>
            <td><div class="inputfield"><asp:Label ID="lblTo" runat="server" /></div></td>
        </tr>--%>
        <tr>
            <th>Ban hành:</th>
            <td><table cellpadding="2" cellspacing="0" border="0" width="100%">
                <tr>
                    <td style="width:300px;"><div class="inputfield"><asp:Label ID="lblDate" runat="server" /></div></td>
                    <th style="width:15%"> Ngày gửi</th>
                    <td><div class="inputfield"><asp:Label ID="lblFromDate" runat="server" /></div></td>
                </tr>
                </table> </td>
        </tr>
        
        <tr>
            <th>Bút phê:</th>
            <td><div class="inputfield"><asp:Label ID="lblSubject" runat="server" /></div></td>
        </tr>
        
        <tr>
            <th>Chủ đề:</th>
            <td><div class="messagebody" style="min-height:100px;"><asp:Label ID="lblBody" runat="server"></asp:Label></div></td>
        </tr>
        <tr>
            <th>Đính kèm:</th>
            <td><div class="inputfield"><asp:Label ID="lblAttachment" runat="server" /></div></td>
        </tr>
      </table>
      
    <div class="title-detail-document"><asp:Label ID="Label1" runat="server">Tiếp nhận công văn</asp:Label></div>
        <table cellpadding="2" cellspacing="0" border="0" width="100%">
        <colgroup>
            <col width="90px" />
            <col />
        </colgroup>
        <tr>
            <th style="width:15%;">Nơi nhận :</th>
            <td><div class="inputfield"><asp:Label ID="lblToOffice" runat="server" /></div></td>
        </tr>
             <tr>
            <th>Bộ phận :</th>
            <td><table  cellpadding="2" cellspacing="0" border="0" width="100%">
                <tr>
                    <td style="width:300px;">
                        <div class="inputfield"><asp:Label ID="lblToDep" runat="server" /></div>
                    </td>
                    <th style="width:15%;">
                        Ngày nhận
                    </th>
                    <td>
                        <div class="inputfield"><asp:Label ID="lblToDate" runat="server" /></div>
                    </td>
                </tr>
                </table>   </td>
        </tr>
               <tr>
            <th>Nơi lưu :</th>
            <td> <table cellpadding="2" cellspacing="0" border="0" width="100%">
                   <tr>
                       <td style="width:300px;">
                           <div class="inputfield"><asp:Label ID="lblPublishDep" runat="server" /></div>
                       </td>
                       <th style="width:15%;">
                           Ngày vào sổ
                       </th>
                       <td>  <div class="inputfield"><asp:Label ID="lblApplyDate" runat="server" /></div> </td>
                       <th style="width:15%;">
                           Số vào sổ
                       </th>
                       <td>  <div class="inputfield" style="font-weight:bold"><asp:Label ID="lblRegisterNum" runat="server" /></div> </td>
                   </tr>
                 </table> </td>
        </tr>

    </table>
           <div class="title-detail-document"><asp:Label ID="Label4" runat="server">Hạng mục công việc</asp:Label></div>
       <table cellpadding="2" cellspacing="0" border="0" width="100%">
        <colgroup>
            <col width="90px" />
            <col />
        </colgroup>
         <tr>
            <th style="width:15%;">Hạng mục:</th>

        <td>
            <telerik:RadGrid ID="RadGrid2" runat="server" Height="100%" AutoGenerateColumns="False"
                BorderWidth="0px"
                AllowPaging="true" PageSize="20" GridLines="None" Skin="Office2007">
                <MasterTableView DataKeyNames="Doc_Id" ClientDataKeyNames="Doc_Id"
                    GroupLoadMode="Client">
                    <NoRecordsTemplate>
                        <div class="NoRecord">Không có hạng mục công việc trong công văn này</div>
                    </NoRecordsTemplate>
                    <Columns>
                       
                        <telerik:GridBoundColumn  HeaderText="Hạng mục công việc" DataField="Mota"
                            UniqueName="Mota" HeaderStyle-Width="250px" >
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Ghi chú" DataField="Ghichu"
                            UniqueName="Ghichu" HeaderStyle-Width="250px">
                            <ItemStyle CssClass="hyperlink" /> 
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn   HeaderText="Thời gian hoàn thành" DataField="ThoigianHoanthanh"
                            DataFormatString="{0:dd/MM/yyyy}" UniqueName="ThoigianHoanthanh" HeaderStyle-Width="165px">
                        </telerik:GridBoundColumn>
                       
                    </Columns>
                </MasterTableView>
                <%--<ClientSettings AllowKeyboardNavigation="true" ClientEvents-OnKeyPress="GridKeyPressed" ReorderColumnsOnClient="True" 
                    AllowDragToGroup="True" AllowColumnsReorder="True" EnableRowHoverStyle="true">
                    <Selecting AllowRowSelect="True"></Selecting>
                    <Scrolling AllowScroll="True" UseStaticHeaders="True"></Scrolling>
                    <ClientEvents OnRowContextMenu="RowContextMenu" OnRowSelected="RowSelected" OnRowDblClick="RowDblClick"
                        OnRowMouseOver="RowMouseOver" />
                </ClientSettings>--%>
                <%--<GroupPanel Text="Nh&#243;m theo t&#234;n cột" ToolTip="Đưa con trỏ v&#224;o &#244; t&#234;n cột, nhấn v&#224; giữ nguy&#234;n n&#250;t tr&#225;i chuột sau đ&#243; k&#233;o v&#224; thả v&#224;o &#244; n&#224;y" />
                <SortingSettings SortToolTip="Nhấn vào đây để sắp xếp"></SortingSettings>
                <GroupingSettings ShowUnGroupButton="true" />--%>
                <PagerStyle Mode="NextPrevAndNumeric" PageSizeLabelText="Số công việc hiển thị" NextPagesToolTip=""
                    NextPageToolTip="Trang kế" PageButtonCount="5" PagerTextFormat="Trang {0}/{1} {4} Tổng cộng {5} công việc"
                    PrevPagesToolTip="" PrevPageToolTip="Trang trước" />
            </telerik:RadGrid>
            </td>
             </tr>
         </table>
         <div class="title-detail-document"><asp:Label ID="Label2" runat="server">Bộ phận tiếp nhận</asp:Label></div>
        <table cellpadding="2" cellspacing="0" border="0" width="100%">
        <colgroup>
            <col width="90px" />
            <col />
        </colgroup>
        <tr >
            <th style="width:15%;">Bộ phận chính :</th>
            <td><div class="inputfield"><asp:Label ID="lblBophanchinh" runat="server" /></div></td>
        </tr>
             <tr>
            <th>Bộ phận phối hợp :</th>
            <td><div class="inputfield"><asp:Label ID="lblBophanphoihop" runat="server" /></div></td>
        </tr>
               

    </table>
    </div>
    
</div>

</asp:Content>

