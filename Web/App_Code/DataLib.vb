﻿Imports System.Data
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Data.Sql

Public Class DataLib
    Public Shared ReadOnly ConnectString As String = "TiagsSqlServer"

    Public Shared Function ValidUser(ByVal id As String) As String
        Dim aes As New TLibs.Encryption.AES("My_SeCrEt_KeY")
        Dim userid As String = aes.DecryptToString(id)
        aes = Nothing
        Return userid
    End Function

    Public Shared Function GetDocumentList(ByVal folder As String, ByVal userID As String) As DataTable
        Dim sb As New Text.StringBuilder
        sb.Append(" SELECT  p.PersonalBox_ID, p.Source_ID, p.Subject, ")
        sb.Append("         d.Content, p.AttachFile, p.SendTime, p.SenderID, p.Seen ")
        sb.Append("  FROM dbo.PersonalBox AS p INNER JOIN dbo.Documents AS d ON p.Source_ID = d.Doc_ID ")
        sb.Append(" WHERE p.Type = 'DO' AND p.receiverid = @UserID AND folder = @Folder AND p.Status <> 'XX' AND d.Status <> 'XX'")
        sb.Append(" ORDER BY p.PersonalBox_ID DESC")

        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(ConnectString)
        Dim dbCommand As DbCommand = db.GetSqlStringCommand(sb.ToString)
        db.AddInParameter(dbCommand, "UserID", SqlDbType.VarChar, userID)
        db.AddInParameter(dbCommand, "Folder", SqlDbType.VarChar, Folder)
        Return db.ExecuteDataSet(dbCommand).Tables(0)
    End Function

    Public Shared Function GetMailList(ByVal folder As String, ByVal userID As String) As DataTable
        Dim sb As New Text.StringBuilder
        sb.Append(" SELECT  p.PersonalBox_ID, p.Source_ID, dbo.uf_GetUserNameById(p.SenderID) AS [Sender], p.Subject, ")
        sb.Append("         m.Content, p.AttachFile, m.Important, p.SendTime, p.SenderID, p.Seen ")
        sb.Append("  FROM dbo.PersonalBox AS p INNER JOIN dbo.Mails AS m ON p.Source_ID = m.Mail_ID ")
        sb.Append(" WHERE p.Type = 'ML' AND p.receiverid = @UserID AND folder = @Folder AND p.Status <> 'XX' AND m.Status <> 'XX'")
        sb.Append(" ORDER BY p.PersonalBox_ID DESC")

        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(ConnectString)
        Dim dbCommand As DbCommand = db.GetSqlStringCommand(sb.ToString)
        db.AddInParameter(dbCommand, "UserID", SqlDbType.VarChar, userID)
        db.AddInParameter(dbCommand, "Folder", SqlDbType.VarChar, folder)
        Return db.ExecuteDataSet(dbCommand).Tables(0)
    End Function

    Public Shared Function GetMailByID(ByVal pID As Integer, ByVal userID As String) As DataTable
        Dim sb As New Text.StringBuilder
        sb.Append(" SELECT  p.PersonalBox_ID, p.Source_ID, dbo.uf_GetUserNameById(p.SenderID) AS [Sender], p.Subject, ")
        sb.Append("         m.Content, p.AttachFile, m.Important, p.SendTime, p.SenderID, p.Seen ")
        sb.Append(" FROM dbo.PersonalBox AS p INNER JOIN dbo.Mails AS m ON p.Source_ID = m.Mail_ID ")
        sb.Append(" WHERE p.PersonalBox_ID = @pID")
        sb.Append(" ORDER BY p.PersonalBox_ID DESC")

        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(ConnectString)
        Dim dbCommand As DbCommand = db.GetSqlStringCommand(sb.ToString)
        db.AddInParameter(dbCommand, "UserID", SqlDbType.VarChar, userID)
        db.AddInParameter(dbCommand, "pID", SqlDbType.Int, pID)
        Return db.ExecuteDataSet(dbCommand).Tables(0)
    End Function

    Public Shared Function GetDocumentByID(ByVal pid As Integer, ByVal userID As String) As DataTable
        Dim sb As New Text.StringBuilder
        sb.Append(" SELECT  p.PersonalBox_ID, p.Source_ID, p.Subject, ")
        sb.Append("         d.Content, p.AttachFile, p.SendTime, p.SenderID, p.Seen ")
        sb.Append(" FROM dbo.PersonalBox AS p INNER JOIN dbo.Documents AS d ON p.Source_ID = d.Doc_ID ")
        sb.Append(" WHERE p.PersonalBox_ID = @pID")
        sb.Append(" ORDER BY p.PersonalBox_ID DESC")

        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(ConnectString)
        Dim dbCommand As DbCommand = db.GetSqlStringCommand(sb.ToString)
        db.AddInParameter(dbCommand, "UserID", SqlDbType.VarChar, userID)
        db.AddInParameter(dbCommand, "pID", SqlDbType.Int, pID)
        Return db.ExecuteDataSet(dbCommand).Tables(0)
    End Function

    Public Shared Function GetMailAttachment(ByVal MailID As Integer) As String
        Dim Result As String = String.Empty
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(ConnectString)
        Dim sqlCommand As String = "SELECT mailfile_id,OriginalName FROM mailfiles WHERE mail_id =" & MailID.ToString
        Dim i As Integer = 0
        Using dr As IDataReader = db.ExecuteReader(CommandType.Text, sqlCommand)
            While dr.Read()
                Result = Result & Common.AttachmentLink("ML", dr("mailfile_id"), dr("OriginalName"))
                i = i + 1
            End While
            If i > 1 Then
                Result = Result + "<a href='downloadall.aspx?type=ML&refid=" & MailID.ToString & "' class='attach' target='_blank'>Download All</a>;"
            End If
        End Using

        Return Result
    End Function
	

End Class
