Imports System.Data
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Data.Sql
Imports VBLib.Common
Imports System.IO
' Class Document
Public Class Documents
    Shared Function GetDocumentList(ByVal Folder As String) As DataTable
        Dim sqlCommand As String = "SELECT PersonalBox_ID, DocNum, Subject,Content, Sendtime, AttachFile, Important, Source_ID, Seen,ThoigianHoanthanh "
        sqlCommand += " FROM Document_vw WHERE receiverid = @UserId AND folder = @Folder ORDER BY PersonalBox_ID DESC"
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim dbCommand As DbCommand = db.GetSqlStringCommand(sqlCommand)
        db.AddInParameter(dbCommand, "UserID", SqlDbType.VarChar, Common.UserId)
        db.AddInParameter(dbCommand, "Folder", SqlDbType.VarChar, Folder)
        Return db.ExecuteDataSet(dbCommand).Tables(0)
    End Function

    Shared Function GetHangMuc(ByVal DocId As Integer) As DataTable
        Dim sqlCommand As String = "SELECT s_ID, Doc_ID, Mota, ThoigianHoanthanh, Ghichu"
        sqlCommand += " FROM HangmucCongviec WHERE Doc_ID = @DocId"
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim dbCommand As DbCommand = db.GetSqlStringCommand(sqlCommand)
        db.AddInParameter(dbCommand, "DocId", SqlDbType.VarChar, DocId)
        Return db.ExecuteDataSet(dbCommand).Tables(0)
    End Function

    Shared Function GetDocumentDetail(ByVal DocID As Integer) As IDocumentDetail
        Dim objDetail As New IDocumentDetail
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim dbCommand As DbCommand = db.GetSqlStringCommand("SELECT *  FROM Documents WHERE doc_id = @DocId")
        db.AddInParameter(dbCommand, "DocId", DbType.Int32, DocID)

        Dim RcCommand As DbCommand = db.GetStoredProcCommand("usp_getreceiverlist")
        db.AddInParameter(RcCommand, "Type", DbType.String, "DO")
        db.AddInParameter(RcCommand, "Id", DbType.Int32, DocID)
        db.AddOutParameter(RcCommand, "UserList", DbType.String, 4000)
        db.ExecuteNonQuery(RcCommand)

        Using dr As IDataReader = db.ExecuteReader(dbCommand)
            While dr.Read
                objDetail.Id = dr("Doc_ID")
                objDetail.MsgType = dr("Type")
                objDetail.ToUser = NVL(db.GetParameterValue(RcCommand, "UserList"), String.Empty)
                objDetail.Subject = dr("Subject")
                objDetail.Body = dr("Content")
                objDetail.Attachment = GetDocAttachmentLink(DocID)
                objDetail.SendTime = dr("Publicdate")
                objDetail.DocNum = dr("Docnum")
                objDetail.FromOffice = dr("FromOffice")
                objDetail.ToOffice = dr("ToOffice")
                objDetail.ToDep = dr("ToDep")
                objDetail.RegisterNum = dr("RegisterNum")
                objDetail.PublishDep = dr("PublicDep")
                objDetail.Bophanchinh = dr("Bophanchinh")
                objDetail.Bophanphoihop = dr("Donviphoihop")
                objDetail.Signer = dr("Signer")
                objDetail.SignerPos = dr("SignerPos")
                objDetail.Field = dr("Field")
                objDetail.ToDate = dr("ToDate")
                objDetail.ApplyDate = dr("ApplyDate")
                objDetail.PublicDate = dr("PublicDate")
                objDetail.FromDate = dr("FromDate")

            End While
        End Using
        Return objDetail
    End Function

    Shared Function GetDocumentDetailPart(ByVal DocID As Integer) As IDocumentDetail
        Dim objDetail As New IDocumentDetail
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim dbCommand As DbCommand = db.GetSqlStringCommand("SELECT * FROM Documents WHERE doc_id = @DocId")
        db.AddInParameter(dbCommand, "DocId", DbType.Int32, DocID)
        Using dr As IDataReader = db.ExecuteReader(dbCommand)
            While dr.Read()
                objDetail.Id = dr("Doc_ID")
                objDetail.MsgType = "DO"
                objDetail.FromUser = "Văn Thư"
                objDetail.Subject = dr("Subject")
                objDetail.Body = dr("Content")
                objDetail.Attachment = GetDocAttachmentLink(DocID)
                objDetail.SendTime = dr("Publicdate")
                objDetail.DocNum = dr("Docnum")
            End While
        End Using
        Return objDetail
    End Function

    Public Shared Function GetAuthorizeList(ByVal UserId As String) As IDataReader
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim sqlCommand As String = "SELECT * FROM Authorize WHERE createid = @UserId AND status = 'OK'"
        Dim dbCommand As DbCommand = db.GetSqlStringCommand(sqlCommand)
        db.AddInParameter(dbCommand, "UserID", SqlDbType.VarChar, Common.UserId)
        Return db.ExecuteReader(dbCommand)
    End Function

    Public Shared Function GetAuthorizeByID(ByVal AuthorizeId As Integer) As IDataReader
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim dbCommand As DbCommand = db.GetSqlStringCommand("SELECT * FROM Authorize WHERE authorize_id = @AuthorizeId")
        db.AddInParameter(dbCommand, "AuthorizeId", DbType.Int32, AuthorizeId)
        Return db.ExecuteReader(dbCommand)
    End Function

    Public Shared Sub UpdateAuthorize(ByVal AuthorizeId As Integer, ByVal Fromdate As Date, ByVal Todate As Date, ByVal Level As Integer, ByVal ReceiverId As String)
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim dbCommand As DbCommand = db.GetStoredProcCommand("usp_Authorize")
        db.AddInParameter(dbCommand, "Authorize_Id", DbType.Int32, AuthorizeId)
        db.AddInParameter(dbCommand, "Fromdate", DbType.DateTime, Fromdate)
        db.AddInParameter(dbCommand, "Todate", DbType.DateTime, Todate)
        db.AddInParameter(dbCommand, "Level", DbType.Int32, Level)
        db.AddInParameter(dbCommand, "CreateId", DbType.String, Common.UserId)
        db.AddInParameter(dbCommand, "ReceiverId", DbType.String, ReceiverId)
        db.ExecuteNonQuery(dbCommand)
    End Sub

    Public Shared Function DeleteAuthorize(ByVal AuthorizeId As Integer) As Boolean
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim dbCommand As DbCommand = db.GetSqlStringCommand("UPDATE authorize SET status = 'XX', Deletedate = Getdate() WHERE authorize_id = @AuthorizeId")
        db.AddInParameter(dbCommand, "AuthorizeId", DbType.Int32, AuthorizeId)
        Dim rowAffected As Integer = db.ExecuteNonQuery(dbCommand)
        Return (rowAffected > 0)
    End Function

    Public Shared Sub GetAllDocAttachment(ByVal DocId As Integer, ByRef AttachList As Collection)
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim sqlCommand As String = "SELECT * FROM docfiles WHERE doc_id =" & DocId.ToString
        Using dr As IDataReader = db.ExecuteReader(CommandType.Text, sqlCommand)
            Do While dr.Read()
                If Common.IsValidUser(dr("Doc_Id"), "DO") Then
                    Dim attFile As New AttachFile
                    attFile.FileName = Trim(dr("OriginalName"))
                    attFile.PhysicalPath = Path.Combine(Trim(Common.GetPhysicalPathByPathID(dr("Path_id"))), dr("filename"))
                    AttachList.Add(attFile)
                End If
            Loop
        End Using
    End Sub

    Public Shared Sub GetDocAttachmentPath(ByVal fileid As Integer, ByRef FileName As String, ByRef PhysicalPath As String)
        Dim result As String = String.Empty
        Dim sqlCommand As String = "SELECT * FROM docfiles WHERE docfile_id =" & fileid
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Using dr As IDataReader = db.ExecuteReader(CommandType.Text, sqlCommand)
            If dr.Read() Then
                If Common.IsValidUser(dr("Doc_Id"), "DO") Then
                    FileName = Trim(dr("OriginalName"))
                    PhysicalPath = Path.Combine(Trim(Common.GetPhysicalPathByPathID(dr("Path_id"))), dr("filename"))
                End If
            End If
        End Using
    End Sub

    Protected Shared Function GetDocAttachmentLink(ByVal DocID As Integer) As String
        Dim Result As String = String.Empty

        Dim sqlCommand As String = "SELECT docfile_id, OriginalName FROM docfiles, UserList where doc_id=" & DocID.ToString
        sqlCommand += " and LoginID like '" & Common.UserId.ToString & "'"
        sqlCommand += " and UserList.Role like '%Download%'"

        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Using dr As IDataReader = db.ExecuteReader(CommandType.Text, sqlCommand)
            While dr.Read()
                Result = Result & Common.AttachmentLink("Do", dr("docfile_id"), dr("OriginalName"))
            End While
        End Using

        Return Result
    End Function

    Public Shared Function GetDocAttachmentPathByDocID(ByVal DocId As Integer) As String
        Dim result As String = String.Empty
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim sqlCommand As String = "Select filename FROM docfiles WHERE doc_id = " & DocId.ToString
        Using dr As IDataReader = db.ExecuteReader(CommandType.Text, sqlCommand)
            Do While dr.Read()
                Dim SaveDir As String = Trim(Common.GetPhysicalPathByPathID(dr("Path_id")))
                result = result & Path.Combine(SaveDir, dr("filename")) & ";"
            Loop
        End Using
        Return result
    End Function

    Public Shared Function GetSearchList(ByVal SearchFor As String, ByVal SearchField As String) As DataTable
        Dim result As New DataTable
        If Not String.IsNullOrEmpty(SearchFor) Then
            Dim condition As String = ""
            Select Case SearchField
                Case "[Tất cả]"
                    condition = "(charindex(@SearchFor,subject)>0 Or charindex(@SearchFor,docnum)>0 Or charindex(@SearchFor,content)>0)"
                Case "Chủ đề"
                    condition = "charindex(@SearchFor,subject)>0"
                Case "Nội dung"
                    condition = "charindex(@SearchFor,content)>0"
                Case "Người gửi"
                    condition = "charindex(@SearchFor,[from])>0"
            End Select

            Dim sqlCommand As String = "Select personalbox_id,source_id,type,sendtime,subject,content,folder FROM searchdoc_vw "
            sqlCommand = sqlCommand + " WHERE receiverid = @UserId And " + condition + " ORDER BY sendtime"
            Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
            Dim dbCommand As DbCommand = db.GetSqlStringCommand(sqlCommand)
            db.AddInParameter(dbCommand, "UserID", SqlDbType.VarChar, Common.UserId)
            db.AddInParameter(dbCommand, "SearchFor", SqlDbType.NVarChar, SearchFor)
            result = db.ExecuteDataSet(dbCommand).Tables(0)
        End If
        Return result
    End Function

    Public Shared Sub GetPrevNextDoc(ByVal PostID As Integer, ByRef PrevMsgId As Integer, ByRef PrevPostId As Integer, ByRef NextMsgId As Integer, ByRef NextPostId As Integer)
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim sqlCommand As String = ""
        sqlCommand = "Select top 1 personalbox_id, source_id FROM [dbo].[personalbox]"
        sqlCommand += " WHERE type='DO' AND receiverid = '" & Common.UserId & "' AND folder='IN' and personalbox_id > " & PostID.ToString
        Using dr As IDataReader = db.ExecuteReader(CommandType.Text, sqlCommand)
            If dr.Read() Then
                NextMsgId = dr("source_id")
                NextPostId = dr("personalbox_id")
            End If
        End Using

        sqlCommand = "SELECT top 1 personalbox_id, source_id FROM [dbo].[personalbox]"
        sqlCommand += " WHERE type='DO' AND receiverid = '" & Common.UserId & "' and folder='IN' and personalbox_id < " & PostID.ToString
        sqlCommand += " ORDER BY personalbox_id desc"
        Using dr As IDataReader = db.ExecuteReader(CommandType.Text, sqlCommand)
            If dr.Read() Then
                PrevMsgId = dr("source_id")
                PrevPostId = dr("personalbox_id")
            End If
        End Using
    End Sub

End Class

Public Class IDocumentDetail
    Public Id As Integer
    Public SenderId As String
    Public MsgType As String
    Public FromUser As String
    Public ToUser As String
    Public Attachment As String
    Public Subject As String
    Public Body As String
    Public SendTime As DateTime
    Public DocNum As String
    Public FromOffice As String
    Public ToOffice As String
    Public ToDep As String
    Public PublishDep As String
    Public RegisterNum As String
    Public Bophanchinh As String
    Public Bophanphoihop As String
    Public Signer As String
    Public SignerPos As String
    Public Field As String
    Public ToDate As String
    Public ApplyDate As String
    Public PublicDate As String
    Public FromDate As String
End Class
