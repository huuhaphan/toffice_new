Imports System.Data
Imports System.Data.Common
Imports Telerik.Web.UI
Imports System.Web.UI.ScriptControl
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Data.Sql
Imports System.IO
Imports Enums
Imports System.Web

Public Class Common
    Public Shared ReadOnly ConnectString As String = "TiagsSqlServer"

    'Function IsAuthenticated() As Boolean
    '    Return HttpContext.Current.User.Identity.IsAuthenticated
    'End Function

    ''Current UserID
    'Public Shared Function UserId(Optional ByVal Type As UserType = UserType.UserId) As String
    '    Dim mUserId As String = ""
    '    If HttpContext.Current.User.Identity.IsAuthenticated Then
    '        Dim User As String() = HttpContext.Current.User.Identity.Name.Split(";"c)
    '        mUserId = User(Type)
    '    End If
    '    Return mUserId
    'End Function

    Public Shared Function IsAuthenticated() As Boolean
        Return IIf(IsNothing(HttpContext.Current.Session("UserID")), False, True)
    End Function

    'Current UserID
    Public Shared Function UserRole() As String
        Return HttpContext.Current.Session("UserRole")
    End Function

    Public Shared Function UserId() As String
        Return HttpContext.Current.Session("UserID")
    End Function
    Public Shared Function Server() As String
        Return "dbazure.vsoftgroup.com"
    End Function
    Public Shared Function DataBase() As String
        Return "CangVu_Document"
    End Function
    Public Shared Function DataBaseUser() As String
        Return "ucangvu"
    End Function
    Public Shared Function DataBasePass() As String
        Return "pcangvu"
    End Function

    Public Shared Sub BuildToolbar(ByRef oToolbar As RadToolBar, ByVal UserId As String, ByVal Type As String, ByVal currentFolder As String)
        Dim group As String = "MoveFolder"
        Dim itembar As RadToolBarDropDown = oToolbar.Items.FindItemByText("Thư mục")
        itembar.Buttons.Clear()
        If itembar IsNot Nothing Then
            If Type = "FD" Then
                AddToolbarDropDown(itembar, "Hộp thư", "Inbox", group)
                AddToolbarDropDown(itembar, "Đã xóa", "Delete", group)

                'Task Folder
                Using dr As IDataReader = TaskFolders.GetTaskFolderListByUser(UserId)
                    While dr.Read()
                        If dr("TaskFolder_ID") <> CInt(currentFolder) Then
                            AddToolbarDropDown(itembar, dr("TaskName"), "FD" + dr("TaskFolder_ID").ToString, group)
                        End If
                    End While
                End Using
            Else
                'DropDownToolbar MoveFolder
                If currentFolder = "DL" Then
                    AddToolbarDropDown(itembar, "Hộp thư", "Inbox", group)
                Else
                    AddToolbarDropDown(itembar, "Đã xóa", "Delete", group)
                End If

                'Task Folder
                Using dr As IDataReader = TaskFolders.GetTaskFolderListByUser(UserId)
                    While dr.Read()
                        AddToolbarDropDown(itembar, dr("TaskName"), "FD" + dr("TaskFolder_ID").ToString, group)
                    End While
                End Using
            End If
        End If
    End Sub

    Shared Sub AddToolbarDropDown(ByRef tbitem As RadToolBarDropDown, ByVal str As String, ByVal cValue As String, ByVal Group As String)
        Dim newButton As RadToolBarButton = New RadToolBarButton(str, False, Group)
        newButton.Value = cValue
        tbitem.Buttons.Add(newButton)
    End Sub

    Public Shared Sub MoveMessage(ByVal Id As Integer, ByVal NewFolder As String, Optional ByVal Permanent As Boolean = False)
        Dim sqlCommand As String = ""

        If NewFolder = "DL" AndAlso (Permanent Or Common.IsPurgeMessage(UserId)) Then
            sqlCommand = [String].Format("DELETE personalbox WHERE personalbox_id = {0}", Id)
        ElseIf Left(NewFolder, 2) = "FD" Then
            sqlCommand = [String].Format("UPDATE personalbox SET folder = '', taskfolder_id = {0} WHERE personalbox_id = {1}", NewFolder.Substring(2).Trim, Id)
        Else
            sqlCommand = [String].Format("UPDATE personalbox SET folder = '{0}', taskfolder_id = 0 WHERE personalbox_id = {1}", NewFolder, Id)
        End If

        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim dbCommand As DbCommand = db.GetSqlStringCommand(sqlCommand)
        Dim RowAffected As Integer = db.ExecuteNonQuery(dbCommand)
    End Sub

    Public Shared Sub MoveAllMessage(ByVal MsgType As String, ByVal OldFolder As String, ByVal NewFolder As String)
        Dim UserId As String = Common.UserId
        Dim sqlCommand As String = ""

        If NewFolder = "DL" AndAlso (Common.IsPurgeMessage(UserId) Or OldFolder = "DL") Then
            sqlCommand = [String].Format("DELETE personalbox WHERE ReceiverID= '{0}' AND Type = '{1}' AND Folder = 'DL'", UserId, MsgType)
        ElseIf Left(NewFolder, 2) = "FD" Then
            sqlCommand = [String].Format("UPDATE personalbox SET folder = '', taskfolder_id = {0} WHERE receiverid='{1}' AND folder= '{2}' AND Type = '{3}'", NewFolder.Substring(2).Trim, UserId, OldFolder, MsgType)
        Else
            sqlCommand = [String].Format("UPDATE personalbox SET folder = '{0}', taskfolder_id = 0 WHERE receiverid='{1}' and folder= '{2}' AND Type = '{3}'", NewFolder, UserId, OldFolder, MsgType)
        End If
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim dbCommand As DbCommand = db.GetSqlStringCommand(sqlCommand)
        Dim RowAffected As Integer = db.ExecuteNonQuery(dbCommand)
    End Sub

    'Member list
    Public Overloads Shared Function GetMemberList(ByVal MsgId As Integer, ByVal MsgType As String) As Collection
        Dim MemberList As New Collection
        Dim Person As UserReceiver = Nothing

        Dim sqlCommand As String = ""

        If MsgType = "DO" Then
            sqlCommand = sqlCommand + "SELECT m.receiverid, m.receiver_type, m.type, g.name"
            sqlCommand = sqlCommand + "  FROM docreceivers as m inner join allusergroup_vw as g"
            sqlCommand = sqlCommand + "    ON m.receiverid = g.id and m.receiver_type = g.type"
            sqlCommand = sqlCommand + " WHERE doc_id = @Id"
        Else
            sqlCommand = sqlCommand + "SELECT m.receiverid, m.receiver_type, m.type, g.name"
            sqlCommand = sqlCommand + "  FROM mailreceivers as m inner join allusergroup_vw as g"
            sqlCommand = sqlCommand + "    ON m.receiverid = g.id and m.receiver_type = g.type"
            sqlCommand = sqlCommand + " WHERE mail_id = @Id"
        End If

        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim dbCommand As DbCommand = db.GetSqlStringCommand(sqlCommand)
        db.AddInParameter(dbCommand, "Id", DbType.Int32, MsgId)
        Using dr As IDataReader = db.ExecuteReader(dbCommand)
            While dr.Read
                Person.LoginID = dr("receiverid")
                Person.ReceiverType = dr("type")
                Person.UserType = dr("receiver_type")
                Person.Name = Trim(dr("name"))
                MemberList.Add(Person)
            End While
        End Using
        Return MemberList
    End Function

    Public Overloads Shared Function GetMemberList(ByVal UserId As String) As Collection
        Dim MemberList As New Collection
        Dim person As UserReceiver = Nothing
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim dbCommand As DbCommand = db.GetSqlStringCommand("SELECT id, type, name FROM allusergroup_vw WHERE id = @UserId And type = 'U'")
        db.AddInParameter(dbCommand, "UserID", SqlDbType.VarChar, UserId)
        Using dr As IDataReader = db.ExecuteReader(dbCommand)
            While dr.Read
                person.LoginID = dr("id")
                person.ReceiverType = "RC"
                person.UserType = dr("type")
                person.Name = Trim(dr("name"))
                MemberList.Add(person)
            End While
        End Using
        Return MemberList
    End Function

    Public Shared Function GetUserName(ByVal UserId As String) As String
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim sqlCommand As String = "SELECT rtrim(FullName) FROM UserList WHERE loginid = '" & UserId & "'"
        Dim dbCommand As DbCommand = db.GetSqlStringCommand(sqlCommand)
        Return VBLib.Common.NVL(db.ExecuteScalar(dbCommand), "")
    End Function

    Public Shared Function GetServerTime() As DateTime
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim sqlCommand As String = "SELECT Getdate()"
        Dim dbCommand As DbCommand = db.GetSqlStringCommand(sqlCommand)
        Return Convert.ToDateTime(db.ExecuteScalar(dbCommand))
    End Function

    Public Shared Function GetUserGroupList() As DataTable
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim sqlCommand As String = "SELECT Type, RTrim(Name) AS Name, Type+ID AS [Value], lastname, organization FROM AllUserGroup_vw WHERE NOT (type='U' AND CHARINDEX('MAIL', role) = 0) ORDER BY Type, LastName"
        Dim dbCommand As DbCommand = db.GetSqlStringCommand(sqlCommand)
        Return db.ExecuteDataSet(dbCommand).Tables(0)
    End Function

    Public Shared Function GetUserListByName(ByVal UserName As String, Optional ByVal UserOnly As Boolean = False) As IDataReader
        If Not String.IsNullOrEmpty(UserName) Then
            Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
            Dim Cmd As String = "SELECT RTrim(Name)+ '-' + RTrim(organization) AS Fullname, Type+ID AS [LoginID] FROM AllUserGroup_vw WHERE  NOT (type='U' AND CHARINDEX('MAIL', role) = 0) AND charindex(@UserName,lastname)>0"
            If UserOnly Then
                Cmd = Cmd + " AND type='U'"
            End If
            Dim dbCommand As DbCommand = db.GetSqlStringCommand(Cmd)
            db.AddInParameter(dbCommand, "UserName", SqlDbType.NVarChar, UserName)
            Return db.ExecuteReader(dbCommand)
        End If
        Return Nothing
    End Function

    Public Shared Function GetQueryString(ByVal str As String, Optional ByVal DefaultValue As String = Nothing) As String
        GetQueryString = Replace(HttpContext.Current.Request.QueryString(str), "'", "")
        If GetQueryString = String.Empty Then
            GetQueryString = DefaultValue
        End If
    End Function

    Public Shared Function ValidMsgType(ByVal value As String) As Boolean
        If value Is Nothing Then
            ValidMsgType = False
        Else
            ValidMsgType = IIf(InStr("ML;DO", value.ToUpper) > 0, True, False)
        End If
    End Function

    Public Shared Function ValidMsgCmd(ByVal value As String) As Boolean
        If value Is Nothing Then
            ValidMsgCmd = False
        Else
            ValidMsgCmd = VBLib.Common.InList(value.ToUpper, "", "REPLY", "REPLYALL", "FORWARD")
        End If
    End Function

    Public Shared Function ValidFolder(ByVal value As String) As Boolean
        If value Is Nothing Then
            ValidFolder = False
        Else
            ValidFolder = IIF(InStr("IN;DL;DR;SD", value.ToUpper) > 0,true,false)
        End If
    End Function

    Public Shared Function AddBs(ByVal Path As String) As String
        Path = Replace(Path.Trim, "//", "/")
        If Not Path.EndsWith("/") Then
            Path = Path & "/"
        End If
        AddBs = Path
    End Function

    Public Shared Function VerifyFolder(ByVal Path As String) As String
        If Not Directory.Exists(Path) Then
            Directory.CreateDirectory(Path)
        End If
        VerifyFolder = Path
    End Function

    Shared Function GetVirtualDirPath(ByVal vdName As String) As String
        Dim adsiPath As String = "IIS://localhost/W3SVC/1/ROOT/" + vdName

        Try
            Dim entry As New DirectoryServices.DirectoryEntry(adsiPath)
            Return entry.Properties("Path").Value.ToString()
        Catch ex As Exception
            ' If Virtual Directory is not found,
            ' it will throw exception.
            Return ""
        End Try

        Return ""
    End Function

    Public Shared Function GetVitualPath(ByVal PathId As String) As String
        Dim Result As String = ""
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim sqlCommand As String = "SELECT Web_Path FROM dbo.FilePath WHERE path_id = '" & Trim(PathId) & "'"
        Dim dbCommand As DbCommand = db.GetSqlStringCommand(sqlCommand)
        Using dr As IDataReader = db.ExecuteReader(dbCommand)
            If dr.Read() Then
                Result = Trim(dr("Web_Path"))
            End If
        End Using
        Return Result
    End Function

    Public Shared Sub GetPhysicalPath(ByVal Type As String, ByRef PathID As String, ByRef Path As String)
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim Cmd As String = "dbo.usp_GetFilePath"
        Dim dbCmd As DbCommand = db.GetStoredProcCommand(Cmd)
        db.AddInParameter(dbCmd, "Type", SqlDbType.Char, Type)
        db.AddOutParameter(dbCmd, "Path_ID", SqlDbType.Char, 10)
        db.AddOutParameter(dbCmd, "ServerIP", SqlDbType.Char, 20)
        db.AddOutParameter(dbCmd, "UserName", SqlDbType.Char, 50)
        db.AddOutParameter(dbCmd, "Password", SqlDbType.Char, 20)
        db.AddOutParameter(dbCmd, "Physical_Path", SqlDbType.Char, 100)
        db.AddOutParameter(dbCmd, "Virtual_Path", SqlDbType.Char, 100)
        db.AddOutParameter(dbCmd, "Web_Path", SqlDbType.Char, 100)
        db.ExecuteNonQuery(dbCmd)
        PathID = Trim(db.GetParameterValue(dbCmd, "Path_ID"))
        Path = Trim(db.GetParameterValue(dbCmd, "Physical_Path"))
    End Sub

    Public Overloads Shared Function AttachmentLink(ByVal Type As String, ByVal fileid As String, ByVal Description As String) As String
        AttachmentLink = "<a href='downloadattachment.aspx?type=" & Type & "&fileid=" & fileid & " ' class='attach' target='_blank'><img src='images/attch.gif' border='0'>" & System.IO.Path.GetFileName(Description) & "</a>;"
    End Function

    Public Overloads Shared Function AttachmentLink(ByVal Path As String, ByVal Description As String) As String
        AttachmentLink = "<a href='" & Path & "' class='attach' target='_blank'><img src='images/attch.gif' border='0'>" & System.IO.Path.GetFileName(Description) & "</a>;"
    End Function

    Public Shared Function IsValidUser(ByVal SourceID As Integer, ByVal Type As String) As Boolean
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim sqlCommand As String = "SELECT source_id FROM personalbox WHERE source_id = @SourceID AND type = @type AND receiverID = @UserId"
        Dim DbCommand As DbCommand = db.GetSqlStringCommand(sqlCommand)
        db.AddInParameter(DbCommand, "SourceID", SqlDbType.Int, SourceID)
        db.AddInParameter(DbCommand, "Type", SqlDbType.Char, Type)
        db.AddInParameter(DbCommand, "UserID", SqlDbType.VarChar, Common.UserId)

        IsValidUser = IIf(db.ExecuteScalar(DbCommand) > 0, True, False)
    End Function

    Public Shared Sub CloseWindow()
        HttpContext.Current.Response.Write("<script language=javascript>window.open('','_parent','');window.close();</script>")
    End Sub

    Public Shared Sub RefreshPage()
        HttpContext.Current.Response.Write("<script language='javascript'>window.location.reload();</script>")
    End Sub

    Public Shared Sub ShowAlert(ByVal Message As String)
        HttpContext.Current.Response.Write("<script language='javascript'>alert('" + Message.Trim + "');</script>")
    End Sub


    Public Shared Sub UpdateRead(ByVal PbId As Integer, ByVal value As Enums.Mark)
        ' Update PersonalBox read/unread
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim sqlCommand As String = "UPDATE PersonalBox SET seen = {0} WHERE PersonalBox_id={1}"
        Dim dbCommand As DbCommand = db.GetSqlStringCommand([String].Format(sqlCommand, IIf(value = Mark.Read, 1, 0), PbId))
        db.ExecuteScalar(dbCommand)
    End Sub

    Public Shared Function GetSizeLimit(ByVal input As Enums.MailSize) As Integer
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim sqlCommand As String = "SELECT " + IIf(input = Enums.MailSize.MailBox, "mail_quota", "attach_quota") + " FROM userlist WHERE loginid ='" & Common.UserId & "'"
        Dim DbCommand As DbCommand = db.GetSqlStringCommand(sqlCommand)
        GetSizeLimit = VBLib.Common.NVL(db.ExecuteScalar(DbCommand), 0)
    End Function

    Public Shared Function GetPhysicalPathByPathID(ByVal PathID As String) As String
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim sqlCommand As String = "SELECT physical_path FROM dbo.filepath WHERE path_id = '" & PathID & "'"
        Dim dbCommand As DbCommand = db.GetSqlStringCommand(sqlCommand)
        Return VBLib.Common.NVL(db.ExecuteScalar(dbCommand), "")
    End Function

    Public Shared Function GetTemplateList() As DataTable
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim sqlCommand As String = "SELECT id,subject,description,link,recdate FROM Templates"
        sqlCommand += " WHERE type = 'DO' AND status = 'OK' ORDER BY id"
        Dim dbCommand As DbCommand = db.GetSqlStringCommand(sqlCommand)
        Return db.ExecuteDataSet(dbCommand).Tables(0)
    End Function

    Public Shared Function GetAdviseList() As DataTable
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim sqlCommand As String = "SELECT id,subject,description,link,recdate FROM Templates"
        sqlCommand += " WHERE type = 'AD' AND status = 'OK' ORDER BY id"
        Dim dbCommand As DbCommand = db.GetSqlStringCommand(sqlCommand)
        Return db.ExecuteDataSet(dbCommand).Tables(0)
    End Function
	
    Public Shared Function IsInRole(ByVal rolename As String) As Boolean
        Dim userRole As String = HttpContext.Current.Session("UserRole")
        If String.IsNullOrEmpty(userRole) Then
            Return False
        End If

        Dim retval As Boolean = False
        For Each role As String In userRole.Split(";"c)
            If Trim(role.ToLower) = Trim(rolename.ToLower) Then
                retval = True
                Exit For
            End If
        Next

        'If TypeOf HttpContext.Current.User.Identity Is FormsIdentity Then
        '    Dim id As FormsIdentity = DirectCast(HttpContext.Current.User.Identity, FormsIdentity)
        '    Dim ticket As FormsAuthenticationTicket = id.Ticket
        '    Dim userData As String = ticket.UserData
        '    Dim roles As String() = userData.Split(";"c)
        '    For Each role As String In roles
        '        If Trim(role.ToLower) = Trim(rolename.ToLower) Then
        '            retval = True
        '            Exit For
        '        End If
        '    Next
        'End If
        Return retval
    End Function
	
    Public Shared Function IsPurgeMessage(ByVal LoginId As String) As Boolean
        ' Get roles from UserRoles table, and add to cookie
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim sqlCommand As String = "SELECT deletetofolder FROM dbo.userlist WHERE loginid ='" & LoginId + "'"
        Dim result As String = db.ExecuteScalar(CommandType.Text, sqlCommand)
        Return IIf(result = "Y", False, True)
    End Function

    Public Shared Function Signature(ByVal LoginId As String) As String
        ' Get roles from UserRoles table, and add to cookie
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim sqlCommand As String = "SELECT case signature when 'Y' then signaturecontent else '' end [signature] FROM dbo.userlist WHERE loginid ='" & LoginId + "'"
        Dim result As String = db.ExecuteScalar(CommandType.Text, sqlCommand)
        If Not String.IsNullOrEmpty(result) Then
            result = "<br/><br/>" & result
        End If
        Return result
    End Function

    Public Shared Function CountUnRead(ByVal Type As String, ByVal Folder As String) As Integer
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim sqlCommand As String = [String].Format("SELECT Count(*) FROM dbo.PersonalBox WHERE ReceiverID = '{0}' AND Type = '{1}' AND Folder = '{2}' AND Seen=0", UserId(), Type, Folder)
        Dim dbCommand As DbCommand = db.GetSqlStringCommand(sqlCommand)
        Return VBLib.Common.NVL(db.ExecuteScalar(dbCommand), 0)
    End Function

    Public Shared Function GetSchedule() As DataTable
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(ConnectString)
        Dim sqlCommand As String = "SELECT * FROM Appointments WHERE userid = '" & Common.UserId & "'"
        Dim DbCommand As DbCommand = db.GetSqlStringCommand(sqlCommand)
        Return db.ExecuteDataSet(DbCommand).Tables(0)
    End Function

    Public Shared Sub insertAppointment(ByVal subject As String, ByVal starttime As Date, ByVal endtime As Date)
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(ConnectString)
        Dim sqlCommand As String = "Insert into appointments (subject, start, [end], userid) values ('"
        sqlCommand += subject & "','" & Format(starttime, "g") & "','" + Format(endtime, "g") & "','" & Common.UserId & "')"
        Dim DbCommand As DbCommand = db.GetSqlStringCommand(sqlCommand)
        db.ExecuteNonQuery(DbCommand)
    End Sub

    Public Shared Sub UpdateAppointment(ByVal Id As Integer, ByVal subject As String, ByVal starttime As Date, ByVal endtime As Date)
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(ConnectString)
        Dim sqlCommand As String = "update appointments set subject = '" & subject & "', start = '"
        sqlCommand += Format(starttime, "g") & "', [end] = '" + Format(endtime, "g") & "'"
        sqlCommand += " Where Id = " & Id.ToString
        Dim DbCommand As DbCommand = db.GetSqlStringCommand(sqlCommand)
        db.ExecuteNonQuery(DbCommand)
    End Sub

    Public Shared Sub DeleteAppointment(ByVal Id As Integer)
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(ConnectString)
        Dim sqlCommand As String = "Delete appointments where id = " + Id.ToString
        Dim DbCommand As DbCommand = db.GetSqlStringCommand(sqlCommand)
        db.ExecuteNonQuery(DbCommand)
    End Sub
End Class

Public Structure UserReceiver
    Dim UserType As String 'G:Group/U:User
    Dim LoginID As String
    Dim Name As String
    Dim ReceiverType As String 'RC/CC/BC
End Structure

Public Structure AttachFile
    Dim FileName As String
    Dim PhysicalPath As String
End Structure
