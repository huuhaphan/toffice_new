Imports Microsoft.VisualBasic
Imports System
Imports System.Text
Imports System.Web.UI
Imports System.IO
Imports System.Security.Permissions
Imports Microsoft.Win32
Imports ICSharpCode.SharpZipLib.Zip

Public Class Utils
    Public Shared Sub SendFile(ByVal fullpath As String, ByVal filename As String)
        If File.Exists(fullpath) Then
            Dim ext As String = Path.GetExtension(filename)
            If Not String.IsNullOrEmpty(ext) Then
                ext = ext.Substring(1, ext.Length - 1)
            End If
            Dim userAgent As String = HttpContext.Current.Request.UserAgent
            Dim encodedFilename As String
            If userAgent.IndexOf("MSIE") > -1 Then
                encodedFilename = HttpContext.Current.Server.UrlPathEncode(filename)
            Else
                encodedFilename = filename
            End If

            HttpContext.Current.Response.BufferOutput = False
            HttpContext.Current.Response.Clear()
            HttpContext.Current.Response.ContentType = GetAttachmentMimeTypeFromFileExtension(ext)
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=""" & encodedFilename & """")
            HttpContext.Current.Response.WriteFile(fullpath)
            HttpContext.Current.Response.Flush()
            HttpContext.Current.Response.End()
        Else
            ErrHandler.ShowError(603)
        End If
    End Sub

    Public Shared Function GetAttachmentMimeTypeFromFileExtension(ByVal fileExtension As String) As String
        Dim result As String = "application/octet-stream"

        If String.IsNullOrEmpty(fileExtension) Then
            Return result
        End If

        Select Case fileExtension.ToLower
            Case "gif"
                Return "image/gif"
            Case "png"
                Return "image/png"
            Case "jpe", "jpg", "jpeg"
                Return "image/jpeg"
            Case "tif", "tiff"
                Return "image/tiff"
            Case "bin", "dms", "lha", "lzh", "exe", "class", "dll"
                Return "application/octet-stream"
            Case "js"
                Return "application/x-javascript"
            Case "swf"
                Return "application/x-shockwave-flash"
            Case "doc", "docx"
                Return "application/msword"
            Case "xls", "xlsx"
                Return "application/vnd.ms-excel"
            Case "ppt", "pptx"
                Return "application/vnd.ms-powerpoint"
            Case "zip"
                Return "application/zip"
            Case "ai", "eps", "ps"
                Return "application/postscript"
            Case "pdf"
                Return "application/pdf"
            Case "rtf"
                Return "application/rtf"
            Case "htm", "html"
                Return "text/html"
            Case "css"
                Return "text/css"
            Case "rtx"
                Return "text/richtext"
            Case "txt", "asc"
                Return "text/plain"
            Case "xml"
                Return "text/xml"
            Case "wav"
                Return "audio/x-wav"
            Case "mid", "midi"
                Return "audio/midi"
            Case "mpga", "mp2", "mp3"
                Return "audio/mpeg"
            Case "aif", "aiff"
                Return "audio/x-aiff"
            Case "ra"
                Return "audio/x-realaudio"
            Case "mpeg", "mpg", "mpe"
                Return "video/mpeg"
            Case "qt", "mov"
                Return "video/quicktime"
            Case "avi"
                Return "video/x-msvideo"
        End Select


        Dim regKey As RegistryKey = Nothing
        Try
            If fileExtension(0) = "."c Then
                fileExtension = fileExtension.Remove(0, 1)
            End If

            regKey = Registry.ClassesRoot
            If regKey IsNot Nothing Then
                regKey = regKey.OpenSubKey(String.Format(".{0}", fileExtension))
            End If
            If regKey IsNot Nothing Then
                result = DirectCast(regKey.GetValue("Content Type"), String)
            End If
        Catch
            Return result
        Finally
            If regKey IsNot Nothing Then
                regKey.Close()
            End If
        End Try
        Return result
    End Function

    Public Shared Sub ZipAllFiles(ByVal tempFileName As String, ByVal tempZipName As String, ByVal AttachFileList As Collection)
        Dim buffer As Byte() = New Byte(4095) {}
        Dim ZipOutput As ZipOutputStream = New ZipOutputStream(File.Create(tempFileName))
        Dim filePath As String = [String].Empty
        Dim fileName As String = [String].Empty
        Dim readBytes = 0

        For Each AttFile As AttachFile In AttachFileList
            If Not String.IsNullOrEmpty(AttFile.FileName) Then
                Dim zipEntry = New ZipEntry(AttFile.FileName)
                ZipOutput.PutNextEntry(zipEntry)

                Using fs = File.OpenRead(AttFile.PhysicalPath)
                    Do
                        readBytes = fs.Read(buffer, 0, buffer.Length)
                        ZipOutput.Write(buffer, 0, readBytes)
                    Loop While readBytes > 0
                End Using
            End If
        Next

        If ZipOutput.Length = 0 Then
            Exit Sub
        End If

        ZipOutput.Finish()
        ZipOutput.Close()

        HttpContext.Current.Response.ContentType = "application/x-zip-compressed"
        HttpContext.Current.Response.AppendHeader("Content-Disposition", "attachment; filename=" & tempZipName)
        HttpContext.Current.Response.WriteFile(tempFileName)

        HttpContext.Current.Response.Flush()
        HttpContext.Current.Response.Close()

        ' delete the temp file
        If File.Exists(tempFileName) Then
            File.Delete(tempFileName)
        End If
    End Sub

End Class