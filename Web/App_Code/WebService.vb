Imports System
Imports System.Data
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.Services

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
<WebService(Namespace:="http://tempuri.org/"), WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1), System.Web.Script.Services.ScriptService()>
Public Class WebService
    Inherits System.Web.Services.WebService

    <WebMethod()>
    Public Function GetDocuments(ByVal uid As String, ByVal take As Integer, ByVal skip As Integer) As Object
        Dim list As New List(Of DocumentData)()
        Dim userID As String = DataLib.ValidUser(uid)

        If String.IsNullOrWhiteSpace(userID) Then
            Return list
        Else

            Try
                Dim dt As DataTable = DataLib.GetDocumentList("IN", userid)
                For Each dr In dt.Rows
                    Dim element As New DocumentData
                    element.DocID = dr("Source_ID")
                    element.Subject = dr("Subject")
                    element.Content = dr("Subject")
                    element.ReceiveTime = dr("SendTime")
                    element.PID = dr("PersonalBox_ID")
                    list.Add(element)
                Next
            Catch ex As Exception

            End Try
        End If

        Return New With {Key .Data = list.Skip(skip).Take(take).ToList(), Key .Total = list.Count}
    End Function

    <WebMethod()>
    Public Function GetMails(ByVal uid As String, ByVal take As Integer, ByVal skip As Integer) As Object
        Dim list As New List(Of MailData)()
        Dim userID As String = DataLib.ValidUser(uid)

        If String.IsNullOrWhiteSpace(userID) Then
            Return list
        Else
            Try
                Dim dt As DataTable = DataLib.GetMailList("IN", userid)
                For Each dr In dt.Rows
                    Dim element As New MailData
                    element.PID = dr("PersonalBox_ID")
                    element.MailID = dr("Source_ID")
                    element.Subject = dr("Subject")
                    element.Content = dr("Content")
                    element.ReceiveTime = dr("SendTime")
                    element.Sender = dr("Sender")
                    element.SenderID = dr("SenderID")
                    list.Add(element)
                Next
            Catch ex As Exception
                Dim element As New MailData
                element.PID = -1
                element.MailID = -1
                element.Subject = ""
                element.Content = ex.Message
                element.ReceiveTime = Now
                element.Sender = ""
                element.SenderID = ""
                list.Add(element)
            End Try
        End If
        Return New With {Key .Data = list.Skip(skip).Take(take).ToList(), Key .Total = list.Count}
    End Function

    <WebMethod()>
    Public Function GetDocumentByID(ByVal uid As String, Byval pid as Integer) As Object
        Dim list As New List(Of DocumentData)()
        Dim userID As String = DataLib.ValidUser(uid)

        If String.IsNullOrWhiteSpace(userID) Then
            Return list
        Else

            Try
                Dim dt As DataTable = DataLib.GetDocumentByID(pid, userid)
                For Each dr In dt.Rows
                    Dim element As New DocumentData
                    element.DocID = dr("Source_ID")
                    element.Subject = dr("Subject")
                    element.Content = dr("Subject")
                    element.ReceiveTime = dr("SendTime")
                    element.PID = dr("PersonalBox_ID")
                    list.Add(element)
                Next
            Catch ex As Exception

            End Try
        End If
        Return list.ToList()
    End Function

    <WebMethod()>
    Public Function GetMailByID(ByVal uid As String, ByVal pid As Integer) As Object
        Dim list As New List(Of MailData)()
        Dim userID As String = DataLib.ValidUser(uid)

        If String.IsNullOrWhiteSpace(userID) Then
            Return list
        Else
            Try
                Dim dt As DataTable = DataLib.GetMailByID(pid, userid)
                For Each dr In dt.Rows
                    Dim element As New MailData
                    element.PID = dr("PersonalBox_ID")
                    element.MailID = dr("Source_ID")
                    element.Subject = dr("Subject")
                    element.Content = dr("Content")
                    element.ReceiveTime = dr("SendTime")
                    element.Sender = dr("Sender")
                    element.SenderID = dr("SenderID")
                    list.Add(element)
                Next
            Catch ex As Exception
                Dim element As New MailData
                element.PID = -1
                element.MailID = -1
                element.Subject = ""
                element.Content = ex.Message
                element.ReceiveTime = Now
                element.Sender = ""
                element.SenderID = ""
                list.Add(element)
            End Try
        End If
        Return list.ToList()
    End Function

    <WebMethod()>
    Public Function GetMailAttachment( ByVal mailid As Integer) As String
		'Dim s as String = DataLib.GetMailAttachment(mailid)
		Dim s1 as string = "<a href='downloadall.aspx'>Mail</a>"
		Return "Hello Nam"
    End Function	
End Class

Public Class DocumentData
    Public Property PID() As Integer
    Public Property DocID() As Integer
    Public Property Subject() As String
    Public Property Content() As String
    Public Property ReceiveTime() As DateTime

    Sub New(ByVal pId As Integer, ByVal docId As Integer, ByVal subject As String, ByVal content As String, ByVal receiveTime As DateTime)
        Me.PID = pId
        Me.DocID = docId
        Me.Subject = subject
        Me.Content = content
        Me.ReceiveTime = receiveTime
    End Sub

    Sub New()
    End Sub
End Class

Public Class MailData
    Public Property PID() As Integer
    Public Property MailID() As Integer
    Public Property Subject() As String
    Public Property Content() As String
    Public Property SenderID() As String
    Public Property Sender() As String
    Public Property ReceiveTime() As DateTime
End Class