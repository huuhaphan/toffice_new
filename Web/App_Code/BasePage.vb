Imports Microsoft.VisualBasic

Public Class BasePage
    Inherits System.Web.UI.Page

    Protected Overloads Overrides Sub OnPreInit(ByVal e As EventArgs)
        MyBase.OnPreInit(e)
        If Session("MyTheme") Is Nothing Then
            Session.Add("MyTheme", "Glass")
            Page.Theme = DirectCast(Session("MyTheme"), String)
        Else
            Page.Theme = DirectCast(Session("MyTheme"), String)
        End If
    End Sub
End Class
