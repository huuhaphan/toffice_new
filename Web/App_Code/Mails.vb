Imports System.IO
Imports System.Data
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Data.Sql
Imports Microsoft.VisualBasic

Public Class Mails
    Private Shared ReadOnly kilobyte As Long = 1024
    Private Shared ReadOnly megabyte As Long = 1024 * kilobyte
    Private Shared ReadOnly gigabyte As Long = 1024 * megabyte
    Private Shared ReadOnly terabyte As Long = 1024 * gigabyte

    Public Shared Function GetMailList(ByVal Folder As String) As DataTable
        Dim sqlCommand As String = "SELECT PersonalBox_ID, Type, Subject, Sendtime, [From], AttachFile, Important, AttachFileSize, Source_ID,Seen"
        sqlCommand = sqlCommand + " FROM Mail_vw WHERE receiverid = @UserId AND Folder = @Folder ORDER BY PersonalBox_ID DESC"
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim dbCommand As DbCommand = db.GetSqlStringCommand(sqlCommand)
        db.AddInParameter(dbCommand, "UserID", SqlDbType.VarChar, Common.UserId)
        db.AddInParameter(dbCommand, "Folder", SqlDbType.VarChar, Folder)
        Return db.ExecuteDataSet(dbCommand).Tables(0)
    End Function

    Public Shared Function GetSearchList(ByVal SearchFor As String, ByVal SearchField As String) As DataTable
        Dim dt As New DataTable
        If Not String.IsNullOrEmpty(SearchFor) Then
            Dim condition As String = ""
            Select Case SearchField
                Case "[Tất cả]"
                    condition = "(charindex(@SearchFor,subject)>0 OR charindex(@SearchFor,content)>0)"
                Case "Chủ đề"
                    condition = "charindex(@SearchFor,subject)>0"
                Case "Nội dung"
                    condition = "charindex(@SearchFor,content)>0"
                Case "Người gửi"
                    condition = "charindex(@SearchFor,[from])>0"
            End Select

            Dim sqlCommand As String = "SELECT personalbox_id,source_id,type,sendtime,subject,content,folder "
            sqlCommand = sqlCommand & " FROM searchmail_vw WHERE receiverid = @UserId AND " & condition & " ORDER BY sendtime"
            Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
            Dim dbCommand As DbCommand = db.GetSqlStringCommand(sqlCommand)
            db.AddInParameter(dbCommand, "UserID", SqlDbType.VarChar, Common.UserId)
            db.AddInParameter(dbCommand, "SearchFor", SqlDbType.NVarChar, SearchFor)
            dt = db.ExecuteDataSet(dbCommand).Tables(0)
        End If
        Return dt
    End Function

    Public Shared Function GetMailDetail(ByVal MailID As Integer) As IMailDetail
        Dim objDetail As New IMailDetail
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim dbCommand As DbCommand = db.GetSqlStringCommand("SELECT * FROM Mails WHERE mail_id = @MailID")
        db.AddInParameter(dbCommand, "MailID", DbType.Int32, MailID)

        Dim RcCommand As DbCommand = db.GetStoredProcCommand("usp_getreceiverlist")
        db.AddInParameter(RcCommand, "Type", DbType.String, "ML")
        db.AddInParameter(RcCommand, "Id", DbType.Int32, MailID)
        db.AddInParameter(RcCommand, "MailType", DbType.String, "RC")
        db.AddOutParameter(RcCommand, "UserList", DbType.String, 4000)
        db.ExecuteNonQuery(RcCommand)

        Dim CcCommand As DbCommand = db.GetStoredProcCommand("usp_getreceiverlist")
        db.AddInParameter(CcCommand, "Type", DbType.String, "ML")
        db.AddInParameter(CcCommand, "Id", DbType.Int32, MailID)
        db.AddInParameter(CcCommand, "MailType", DbType.String, "CC")
        db.AddOutParameter(CcCommand, "UserList", DbType.String, 4000)
        db.ExecuteNonQuery(CcCommand)

        Using dr As IDataReader = db.ExecuteReader(dbCommand)
            While dr.Read
                objDetail.Id = dr("Mail_ID")
                objDetail.MsgType = "ML"
                objDetail.SenderId = dr("SenderID")
                objDetail.FromUser = Common.GetUserName(dr("SenderID"))
                objDetail.ToUser = VBLib.Common.NVL(db.GetParameterValue(RcCommand, "UserList"), String.Empty)
                objDetail.Cc = VBLib.Common.NVL(db.GetParameterValue(CcCommand, "UserList"), String.Empty)
                objDetail.Attachment = GetMailAttachmentLink(MailID)
                objDetail.Subject = dr("Subject")
                objDetail.Body = dr("Content")
                objDetail.SendTime = dr("SendTime")
            End While
        End Using
        Return objDetail
    End Function

    Public Shared Function GetMailDetailPart(ByVal MailID As Integer) As IMailDetail
        Dim objDetail As New IMailDetail
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim dbCommand As DbCommand = db.GetSqlStringCommand("SELECT SenderID, Subject, Content FROM Mails WHERE mail_id = @MailID")
        db.AddInParameter(dbCommand, "MailID", DbType.Int32, MailID)
        Using dr As IDataReader = db.ExecuteReader(dbCommand)
            While dr.Read
                objDetail.SenderId = dr("SenderID")
                objDetail.FromUser = Common.GetUserName(dr("SenderID"))
                objDetail.Attachment = GetMailAttachmentLink(MailID)
                objDetail.Subject = dr("Subject")
                objDetail.Body = dr("Content")
            End While
        End Using
        Return objDetail
    End Function

    Public Shared Function SaveMail(ByVal RefId As Integer, ByVal SenderID As String, ByVal Subject As String, ByVal Content As String, ByVal [To] As String, ByVal Receivers As Collection, ByVal Action As String, ByVal Important As Boolean) As Integer
        Dim MailID As Integer = -1
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Using connection As DbConnection = db.CreateConnection()
            connection.Open()
            Dim transaction As DbTransaction = connection.BeginTransaction()

            Try
                ' Update Mails
                Dim dbCommand As DbCommand = db.GetStoredProcCommand("usp_mails")
                db.AddInParameter(dbCommand, "Mail_Id", DbType.Int32, RefId)
                db.AddInParameter(dbCommand, "SenderID", DbType.String, SenderID)
                db.AddInParameter(dbCommand, "Subject", DbType.String, Subject)
                db.AddInParameter(dbCommand, "Content", DbType.String, Content)
                db.AddInParameter(dbCommand, "Folder", DbType.String, Action)
                db.AddInParameter(dbCommand, "Important", DbType.String, IIf(Important, "X", ""))
                db.AddOutParameter(dbCommand, "NewID", DbType.Int32, 4)
                db.ExecuteNonQuery(dbCommand)
                MailID = db.GetParameterValue(dbCommand, "NewID")

                If MailID > 0 And Action = "SD" Then
                    'Insert MailReceivers
                    For Each person As Object In Receivers
                        Dim rcCommand As DbCommand = db.GetStoredProcCommand("usp_mailreceivers")
                        db.AddInParameter(rcCommand, "Mail_Id", DbType.Int32, MailID)
                        db.AddInParameter(rcCommand, "Receiver_Type", DbType.String, person.UserType)
                        db.AddInParameter(rcCommand, "ReceiverID", DbType.String, person.loginid)
                        db.AddInParameter(rcCommand, "Type", DbType.String, person.receivertype)
                        db.ExecuteNonQuery(rcCommand)
                    Next

                    'Load UserReceiverList
                    Using dr As IDataReader = GetMailReceiverUserList(MailID)
                        While dr.Read()
                            'Insert PersonalBox
                            Dim pbCommand As DbCommand = db.GetStoredProcCommand("usp_personalbox")
                            db.AddInParameter(pbCommand, "Type", DbType.String, "ML")
                            db.AddInParameter(pbCommand, "Folder", DbType.String, "IN")
                            db.AddInParameter(pbCommand, "ReceiverID", DbType.String, dr("UserId"))
                            db.AddInParameter(pbCommand, "SenderID", DbType.String, SenderID)
                            db.AddInParameter(pbCommand, "Subject", DbType.String, Left(Subject, 200))
                            db.AddInParameter(pbCommand, "Source_ID", DbType.Int32, MailID)
                            db.AddInParameter(pbCommand, "AttachFile", DbType.String, "")
                            db.ExecuteNonQuery(pbCommand)
                        End While
                    End Using
                End If
                ' Commit the transaction.
                transaction.Commit()

            Catch ex As Exception
                ' Roll back the transaction. 
                transaction.Rollback()
            End Try
            connection.Close()
            Return MailID
        End Using
    End Function

    Public Shared Sub UpdatePersonalAttach(ByVal MailID As Integer)
        Dim nAttachSize As Integer = 0
        Dim nDefaultSize As Integer = 1024
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim sqlCommand As String = "SELECT Sum(Size) FROM mailfiles WHERE mail_id =" & MailID.ToString
        Dim DbCommand As DbCommand = db.GetSqlStringCommand(sqlCommand)
        nAttachSize = VBLib.Common.NVL(db.ExecuteScalar(DbCommand), 0) + nDefaultSize

        sqlCommand = "UPDATE PersonalBox SET attachfilesize = {0}, attachfile = '{1}' WHERE type='ML' AND source_id = {2}"
        sqlCommand = [String].Format(sqlCommand, nAttachSize, IIf(nAttachSize > nDefaultSize, "X", ""), MailID.ToString)
        DbCommand = db.GetSqlStringCommand(sqlCommand)
        Dim RowAffected As Integer = db.ExecuteScalar(DbCommand)
    End Sub

    Public Shared Sub MailAttachmentForward(ByVal MsgID As Integer, ByVal MsgType As String, ByVal NewMsgId As Integer)
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim dbCommand As DbCommand = db.GetStoredProcCommand("usp_MailForward")
        db.AddInParameter(dbCommand, "Type", DbType.String, MsgType)
        db.AddInParameter(dbCommand, "Id", DbType.Int32, MsgID)
        db.AddInParameter(dbCommand, "NewID", DbType.Int32, NewMsgId)
        db.ExecuteNonQuery(dbCommand)
    End Sub

    Public Shared Function GetMailAttachmentLink(ByVal MailID As Integer) As String
        Dim Result As String = String.Empty
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim sqlCommand As String = "SELECT mailfile_id,OriginalName FROM mailfiles WHERE mail_id =" & MailID.ToString
        Dim i As Integer = 0
        Using dr As IDataReader = db.ExecuteReader(CommandType.Text, sqlCommand)
            While dr.Read()
                Result = Result & Common.AttachmentLink("ML", dr("mailfile_id"), dr("OriginalName"))
                i = i + 1
            End While
            If i > 1 Then
                Result = Result + "<a href='downloadall.aspx?type=ML&refid=" & MailID.ToString & "' class='attach' target='_blank'>Download All</a>;"
            End If
        End Using

        Return Result
    End Function

    Public Shared Sub GetAllMailAttachment(ByVal Mailid As Integer, ByRef AttachList As Collection)
        Dim result As String = String.Empty
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim sqlCommand As String = "SELECT * FROM mailfiles WHERE mail_id =" & Mailid.ToString
        Using dr As IDataReader = db.ExecuteReader(CommandType.Text, sqlCommand)
            Do While dr.Read()
                If Common.IsValidUser(dr("Mail_Id"), "ML") Then
                    Dim attFile As New AttachFile
                    attFile.FileName = Trim(dr("OriginalName"))
                    attFile.PhysicalPath = Path.Combine(Trim(Common.GetPhysicalPathByPathID(dr("Path_id"))), dr("filename"))
                    AttachList.Add(attFile)
                End If
            Loop
        End Using
    End Sub

    Public Shared Sub GetMailAttachmentPath(ByVal fileid As Integer, ByRef FileName As String, ByRef PhysicalPath As String)
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim sqlCommand As String = "SELECT * FROM mailfiles WHERE mailfile_id =" & fileid.ToString
        Using dr As IDataReader = db.ExecuteReader(CommandType.Text, sqlCommand)
            If dr.Read() Then
                If Common.IsValidUser(dr("Mail_Id"), "ML") Then
                    FileName = Trim(dr("OriginalName"))
                    PhysicalPath = Path.Combine(Trim(Common.GetPhysicalPathByPathID(dr("Path_id"))), dr("filename"))
                End If
            End If
        End Using
    End Sub

    Public Shared Function GetMailAttachmentPathByMailID(ByVal MailId As Integer) As String
        Dim result As String = String.Empty
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim sqlCommand As String = "SELECT filename, path_id FROM mailfiles WHERE mail_id = " & MailId.ToString
        Using dr As IDataReader = db.ExecuteReader(CommandType.Text, sqlCommand)
            Do While dr.Read()
                Dim SaveDir As String = Trim(Common.GetPhysicalPathByPathID(dr("Path_id")))
                result = result & Path.Combine(SaveDir, dr("filename")) & ";"
            Loop
        End Using
        Return result
    End Function

    Shared Function InsMailAttach(ByVal MailID As Integer, ByVal OriginalName As String, ByVal FileSize As Integer, ByVal Ext As String, ByVal PathID As String) As String
        Dim cNewname As String = ""

        'Get filename
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim dbCommand As DbCommand = db.GetStoredProcCommand("usp_GenerateFile")
        db.AddOutParameter(dbCommand, "FileName", DbType.String, 20)
        db.ExecuteNonQuery(dbCommand)
        cNewname = VBLib.Common.NVL(db.GetParameterValue(dbCommand, "FileName"), String.Empty) & Ext

        'Insert MailFiles Table
        Dim dbCmd As DbCommand = db.GetStoredProcCommand("usp_mailfiles")
        db.AddInParameter(dbCmd, "Mail_Id", DbType.Int32, MailID)
        db.AddInParameter(dbCmd, "OriginalName", DbType.String, OriginalName)
        db.AddInParameter(dbCmd, "Size", DbType.Int32, FileSize)
        db.AddInParameter(dbCmd, "FileName", DbType.String, cNewname)
        db.AddInParameter(dbCmd, "Path_ID", DbType.String, PathID)
        db.ExecuteNonQuery(dbCmd)
        Return cNewname
    End Function

    Public Shared Function GetToUserList(ByVal MailID As Integer) As String
        Dim result As String = String.Empty
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim RcCommand As DbCommand = db.GetStoredProcCommand("usp_getreceiverlist")
        db.AddInParameter(RcCommand, "Type", DbType.String, "ML")
        db.AddInParameter(RcCommand, "Id", DbType.Int32, MailID)
        db.AddInParameter(RcCommand, "MailType", DbType.String, "RC")
        db.AddOutParameter(RcCommand, "UserList", DbType.String, 4000)
        db.ExecuteNonQuery(RcCommand)
        result = VBLib.Common.NVL(db.GetParameterValue(RcCommand, "UserList"), String.Empty)
        If result.Length > 25 Then
            result = Left(result, 25) + "..."
        End If
        Return result
    End Function

    Public Shared Function GetMailReceiverUserList(ByVal Id As Integer) As IDataReader
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim dbCommand As DbCommand = db.GetSqlStringCommand("SELECT UserId, Direct FROM mailuserreceiver_vw WHERE mail_id = @Id")
        db.AddInParameter(dbCommand, "Id", DbType.Int32, Id)
        Return db.ExecuteReader(dbCommand)
    End Function

    Public Shared Function AllowCreateMail() As Boolean
        Dim nCurrentSize As Integer = 0, nLimitSize As Integer = 0
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim sqlCommand As String = "SELECT Size FROM UserMailAttachmentSize_vw WHERE loginid ='" & Common.UserId & "'"
        Dim DbCommand As DbCommand = db.GetSqlStringCommand(sqlCommand)
        nCurrentSize = db.ExecuteScalar(DbCommand)
        nLimitSize = Common.GetSizeLimit(Enums.MailSize.MailBox)
        Return (nLimitSize > Int(nCurrentSize / megabyte))
    End Function

    Public Shared Function GetLimitPerSend() As Integer
        Dim nLimitSize As Integer = 0
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim sqlCommand As String = "SELECT Attach_Quota FROM Userlist WHERE loginid ='" & Common.UserId & "'"
        Dim DbCommand As DbCommand = db.GetSqlStringCommand(sqlCommand)
        nLimitSize = db.ExecuteScalar(DbCommand)
        Return CInt(nLimitSize) * megabyte
    End Function

    Public Shared Function ToByteString(ByVal bytes As Long) As String
        If bytes = 0 Then
            Return "&nbsp;"
        ElseIf bytes > terabyte Then
            Return (bytes / terabyte).ToString("0.00 TB")
        ElseIf bytes > gigabyte Then
            Return (bytes / gigabyte).ToString("0.00 GB")
        ElseIf bytes > megabyte Then
            Return (bytes / megabyte).ToString("0.00 MB")
        Else
            Return (bytes / kilobyte).ToString("0.00 KB")
        End If
    End Function

    Public Shared Function Statistic(ByVal Folder As String) As Long
        Dim nSize As Long = 0
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim dbCommand As DbCommand
        If Folder = "PF" Then 'Private Folder
            dbCommand = db.GetSqlStringCommand("SELECT Sum(attachfilesize) FROM PersonalBox WHERE receiverid = @UserId AND TaskFolder_ID <> '' AND status <> 'XX'")
            db.AddInParameter(dbCommand, "UserID", SqlDbType.VarChar, Common.UserId)
        Else
            dbCommand = db.GetSqlStringCommand("SELECT Sum(attachfilesize) FROM PersonalBox WHERE receiverid = @UserId AND Folder = @Folder AND status <> 'XX' GROUP BY folder")
            db.AddInParameter(dbCommand, "UserID", SqlDbType.VarChar, Common.UserId)
            db.AddInParameter(dbCommand, "Folder", SqlDbType.VarChar, Folder)
        End If
        nSize = VBLib.Common.NVL(db.ExecuteScalar(dbCommand), 0)
        Return nSize
    End Function

    Public Shared Sub GetPrevNextMail(ByVal PostID As Integer, ByRef PrevMsgId As Integer, ByRef PrevPostId As Integer, ByRef NextMsgId As Integer, ByRef NextPostId As Integer)
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim sqlCommand As String = ""
        sqlCommand = "SELECT top 1 personalbox_id, source_id FROM [dbo].[personalbox]"
        sqlCommand += " WHERE receiverid = '" & Common.UserId & "' AND type='ML' and folder='IN' and personalbox_id > " & PostID.ToString
        Using dr As IDataReader = db.ExecuteReader(CommandType.Text, sqlCommand)
            If dr.Read() Then
                PrevMsgId = dr("source_id")
                PrevPostId = dr("personalbox_id")
            End If
        End Using

        sqlCommand = "SELECT top 1 personalbox_id, source_id FROM [dbo].[personalbox]"
        sqlCommand += " WHERE receiverid = '" & Common.UserId & "' and type='ML' and folder='IN' and personalbox_id < " & PostID.ToString
        sqlCommand += " ORDER BY personalbox_id desc"
        Using dr As IDataReader = db.ExecuteReader(CommandType.Text, sqlCommand)
            If dr.Read() Then
                NextMsgId = dr("source_id")
                NextPostId = dr("personalbox_id")
            End If
        End Using
    End Sub
End Class

Public Class IMailDetail
    Public Id As Integer
    Public SenderId As String
    Public MsgType As String
    Public FromUser As String
    Public ToUser As String
    Public Cc As String
    Public Attachment As String
    Public Subject As String
    Public Body As String
    Public SendTime As DateTime
End Class
