Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Data.Sql
Imports System.Data
Imports System.Data.SqlClient
Public Class News
    Public Shared Function GetNews(ByVal NewsType As Integer) As DataTable
        Dim sqlCommand As String
        Select Case NewsType
            Case 98 'Tin moi nhat
                sqlCommand = "SELECT TOP 10 id,subject,description,recdate FROM News WHERE status='OK' ORDER BY recdate DESC"
            Case 99 'Tin xem nhieu nhat
                sqlCommand = "SELECT TOP 10 id,subject,description,viewcount,recdate FROM News WHERE status='OK' ORDER BY viewcount DESC"
            Case Else
                sqlCommand = String.Format("SELECT Top 5 id,subject,description,recdate FROM News WHERE NewsTypeID = {0} AND status='OK' ORDER BY recdate DESC", NewsType)
        End Select
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim dbCommand As DbCommand = db.GetSqlStringCommand(sqlCommand)
        Return db.ExecuteDataSet(dbCommand).Tables(0)
    End Function

    Public Shared Function GetNewsList(ByVal NewsType As Integer) As DataTable
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim dbCommand As DbCommand = db.GetSqlStringCommand("SELECT id,subject,description,recdate FROM News WHERE NewsTypeID = @NewsType AND status = 'OK' ORDER BY recdate DESC")
        db.AddInParameter(dbCommand, "NewsType", SqlDbType.Int, NewsType)
        Return db.ExecuteDataSet(dbCommand).Tables(0)
    End Function

    Public Shared Function GetNewsType() As IDataReader
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim sqlCommand As String = "SELECT id,name FROM NewsType WHERE status = 'OK' ORDER BY ord"
        Dim dbCommand As DbCommand = db.GetSqlStringCommand(sqlCommand)
        Return db.ExecuteReader(dbCommand)
    End Function

    Public Shared Function GetNewsTypeNameById(ByVal id As Integer) As String
        Dim result As String = String.Empty
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim dbCommand As DbCommand = db.GetSqlStringCommand("SELECT name FROM NewsType WHERE id = @Id")
        db.AddInParameter(dbCommand, "Id", SqlDbType.Int, id)
        Return VBLib.Common.NVL(db.ExecuteScalar(dbCommand), "")
    End Function

    Public Shared Function GetNewsDetail(ByVal Id As Integer) As DataTable
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim dbCommand As DbCommand = db.GetSqlStringCommand("SELECT * FROM News WHERE id = @Id")
        db.AddInParameter(dbCommand, "Id", SqlDbType.Int, Id)
        Return db.ExecuteDataSet(dbCommand).Tables(0)
    End Function

    Public Shared Function GetNewsByText(ByVal searchfor As String, ByVal searchin As String) As DataTable
        Dim dt As New DataTable
        If Not String.IsNullOrEmpty(searchfor) Then
            Dim sqlCommand As String = ""
            If Not String.IsNullOrEmpty(searchfor) Then
                If searchin = "[Tất cả]" Then
                    sqlCommand = "charindex(@searchfor,subject)>0 or charindex(@searchfor,description)>0 or charindex(@searchfor,content)>0"
                ElseIf searchin = "Tiêu đề" Then
                    sqlCommand = "charindex(@searchfor,subject)>0"
                ElseIf searchin = "Tóm tắt" Then
                    sqlCommand = "charindex(@searchfor,description)>0"
                ElseIf searchin = "Nội dung" Then
                    sqlCommand = "charindex(@searchfor,content)>0"
                End If
            End If
            sqlCommand = "SELECT * FROM news WHERE id in (SELECT DISTINCT id FROM News WHERE status='OK' AND (" + sqlCommand + "))"
            Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
            Dim dbCommand As DbCommand = db.GetSqlStringCommand(sqlCommand)
            db.AddInParameter(dbCommand, "searchfor", SqlDbType.NVarChar, searchfor)
            dt = db.ExecuteDataSet(dbCommand).Tables(0)
        End If
        Return dt
    End Function

    Public Shared Sub insNews(ByVal NewsTypeID As String, ByVal Subject As String, ByVal Description As String, ByVal Content As String, ByVal SenderID As String)
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim dbCommand As DbCommand = db.GetStoredProcCommand("usp_news")
        db.AddInParameter(dbCommand, "NewsTypeId", DbType.Int32, NewsTypeID)
        db.AddInParameter(dbCommand, "Subject", DbType.String, Subject)
        db.AddInParameter(dbCommand, "Description", DbType.String, Description)
        db.AddInParameter(dbCommand, "Content", DbType.String, Content)
        db.AddInParameter(dbCommand, "Recuserid", DbType.String, SenderID)
        db.ExecuteNonQuery(dbCommand)
    End Sub

    Public Shared Sub DeleteNews(ByVal Id As Integer)
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim dbCommand As DbCommand = db.GetSqlStringCommand("Update News Set Status = 'XX' WHERE id = @Id")
        db.AddInParameter(dbCommand, "Id", DbType.Int32, Id)
        db.ExecuteNonQuery(dbCommand)
    End Sub

    Public Shared Sub UpdateViewCount(ByVal id As Integer)
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim dbCommand As DbCommand = db.GetSqlStringCommand("UPDATE News SET viewcount = viewcount + 1 WHERE id = @Id")
        db.AddInParameter(dbCommand, "Id", DbType.Int32, id)
        db.ExecuteNonQuery(dbCommand)
    End Sub

    Public Shared Function GetFileName() As String
        'Get filename
        Dim cNewname As String = ""
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim dbCommand As DbCommand = db.GetStoredProcCommand("usp_GenerateFile")
        db.AddOutParameter(dbCommand, "FileName", DbType.String, 30)
        db.ExecuteNonQuery(dbCommand)
        cNewname = VBLib.Common.NVL(db.GetParameterValue(dbCommand, "FileName"), String.Empty)
        Return cNewname
    End Function

    Public Shared Sub UpdateTemplates(ByVal Id As Integer, Byval templateType as string, ByVal Subject As String, ByVal Description As String, ByVal Link As String)
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim dbCommand As DbCommand = db.GetStoredProcCommand("usp_Templates")
        db.AddInParameter(dbCommand, "Id", DbType.Int32, Id)
        db.AddInParameter(dbCommand, "Type", DbType.String, templateType)		
        db.AddInParameter(dbCommand, "Subject", DbType.String, Subject)
        db.AddInParameter(dbCommand, "Description", DbType.String, Description)
        db.AddInParameter(dbCommand, "Link", DbType.String, Link)
        db.ExecuteNonQuery(dbCommand)
    End Sub

    Public Shared Function GetTemplateById(ByVal Id As Integer) As IDataReader
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim dbCommand As DbCommand = db.GetSqlStringCommand("SELECT * FROM Templates WHERE Id = @Id AND status='OK' ORDER BY recdate DESC")
        db.AddInParameter(dbCommand, "Id", DbType.Int32, Id)
        Return db.ExecuteReader(dbCommand)
    End Function

    Public Shared Sub DeleteTemplate(ByVal Id As Integer)
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim dbCommand As DbCommand = db.GetSqlStringCommand("Update Templates Set Status = 'XX' WHERE id = @id")
        db.AddInParameter(dbCommand, "Id", DbType.Int32, Id)
        db.ExecuteNonQuery(dbCommand)
    End Sub

    Public Shared Sub DeleteAdvise(ByVal Id As Integer)
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim dbCommand As DbCommand = db.GetSqlStringCommand("Update Templates Set Status = 'XX' WHERE id = @id")
        db.AddInParameter(dbCommand, "Id", DbType.Int32, Id)
        db.ExecuteNonQuery(dbCommand)
    End Sub

End Class
