Imports System.IO
Imports System.Globalization

Public Class ErrHandler
    Public Shared Sub LogError(ByVal e As Exception, Optional ByVal code As Integer = 0)
        Dim request As HttpRequest = HttpContext.Current.Request
        Dim appfolder As String = HttpContext.Current.Request.PhysicalApplicationPath & "Logs\"
        Dim logfile As String = Path.Combine(appfolder, DateTime.Today.ToString("yyyyMMdd") & ".txt")
        If (Not File.Exists(logfile)) Then
            File.Create(logfile).Close()
        End If
        Dim RetrieveSourceLines As Boolean = True
        Dim StackTrace As String = e.StackTrace
        Dim v_stringBuilder As StringBuilder = New StringBuilder(1024)
        Dim v_stackTrace As New System.Diagnostics.StackTrace(e, True)
        Dim v_stackFrame As System.Diagnostics.StackFrame = v_stackTrace.GetFrame(0)
        Dim v_str As String = v_stackFrame.GetFileName
        Dim v_i As Integer = v_stackFrame.GetFileLineNumber
        Dim v_streamReader As StreamReader
        If (((Not v_stackFrame Is Nothing) AndAlso (RetrieveSourceLines)) AndAlso ((Not v_str Is Nothing))) AndAlso (v_i > 0) Then
            v_streamReader = New StreamReader(v_str)
            Dim v_i1 As Integer = 0
            For v_i1 = 0 To (v_i - 4) - 1
                v_streamReader.ReadLine()
            Next v_i1
            v_stringBuilder.Append("--- Code ---" & vbNewLine & " ")
            v_stringBuilder.AppendFormat("File: {0}" & vbNewLine & " ", v_str)
            v_stringBuilder.AppendFormat("Method: {0}" & vbNewLine & vbNewLine & " ", e.TargetSite)
            v_stringBuilder.AppendFormat("Line {0}: {1}" & vbNewLine & " ", v_i1 + 1, v_streamReader.ReadLine)
            v_stringBuilder.AppendFormat("Line {0}: {1}" & vbNewLine & " ", v_i1 + 2, v_streamReader.ReadLine)
            v_stringBuilder.AppendFormat("Line {0}: {1}" & vbNewLine & " ", v_i1 + 3, v_streamReader.ReadLine)
            v_stringBuilder.AppendFormat("Line {0}: {1}" & vbNewLine & " ", v_i1 + 4, v_streamReader.ReadLine)
            v_stringBuilder.AppendFormat("Line {0}: {1}" & vbNewLine & " ", v_i1 + 5, v_streamReader.ReadLine)
            v_stringBuilder.AppendFormat("Line {0}: {1}" & vbNewLine & " ", v_i1 + 6, v_streamReader.ReadLine)
            v_stringBuilder.AppendFormat("Line {0}: {1}" & vbNewLine & " ", v_i1 + 7, v_streamReader.ReadLine)
            v_streamReader.Close()
        End If
        Dim SourceCode As String = v_stringBuilder.ToString

        Using w As StreamWriter = File.AppendText(logfile)
            w.WriteLine(Constants.vbCrLf & "Log Entry : ")
            w.WriteLine("{0}", DateTime.Now.ToString(CultureInfo.InvariantCulture))
            Dim err As String = "Error in: " & System.Web.HttpContext.Current.Request.Url.ToString() _
                & vbCrLf & "Ip Address: " & request.UserHostAddress _
                & vbCrLf & "Error Message: " & e.Message.ToString _
                & vbCrLf & "Source: " & SourceCode _
                & vbCrLf & "Program: " & e.StackTrace

            w.WriteLine(err)
            w.WriteLine("__________________________")
            w.Flush()
            w.Close()
        End Using
        If code > 0 Then
            ShowError(code)
        End If
    End Sub

    Public Shared Sub ShowError(ByVal code As Integer)
        Dim url As String = "error.aspx?code=" + code.ToString
        HttpContext.Current.Response.Redirect(url)
    End Sub

    Public Shared Sub ShowError(ByVal msg As String)
        Dim url As String = "error.aspx?msg=" + HttpContext.Current.Server.UrlEncode(msg)
        HttpContext.Current.Response.Redirect(url)
    End Sub

End Class
