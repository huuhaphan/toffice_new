Imports System.Web
Imports Microsoft.VisualBasic
Imports system.IO
Imports System.Collections
imports System.Collections.Generic

Public Class ThemeManager
    Public Sub New()
        '
        ' TODO: Add constructor logic here
        '
    End Sub

    Public Shared Function GetThemes() As List(Of Theme)
        Dim dInfo As New DirectoryInfo(System.Web.HttpContext.Current.Server.MapPath("App_Themes"))
        Dim dArrInfo As DirectoryInfo() = dInfo.GetDirectories()
        Dim list As New List(Of Theme)()

        For Each d As DirectoryInfo In dArrInfo
            Dim temp As New Theme(d.Name)
            list.Add(temp)
        Next

        Return list
    End Function
End Class

Public Class Theme
    Private _name As String

    ' Lấy Tên.
    Public Property Name() As String
        Get
            Return _name
        End Get
        Set(ByVal value As String)
            _name = value
        End Set
    End Property

    ' Contractor
    Public Sub New(ByVal value As String)
        Name = value
    End Sub
End Class
