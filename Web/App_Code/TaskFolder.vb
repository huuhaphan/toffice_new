Imports Microsoft.VisualBasic
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Data.Sql
Imports System.Data
Imports System.Data.Common

Public Class TaskFolders
    Public Shared Function GetTaskList(ByVal UserId As String, ByVal TaskFolderID As Integer) As DataTable
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim sqlCommand As String = "SELECT * FROM PersonalBox_vw WHERE receiverid = '" & UserId & "' AND TaskFolder_ID = " & TaskFolderID.ToString + " ORDER BY PersonalBox_ID DESC"
        Dim dbCommand As DbCommand = db.GetSqlStringCommand(sqlCommand)
        Return db.ExecuteDataSet(dbCommand).Tables(0)
    End Function

    Public Shared Function GetTaskFolderListByUser(ByVal UserId As String) As IDataReader
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim sqlCommand As String = "SELECT TaskFolder_ID, TaskName, Recdate FROM TaskFolders WHERE createid = '" & UserId & "' AND status = 'OK' "
        Dim dbCommand As DbCommand = db.GetSqlStringCommand(sqlCommand)
        Return db.ExecuteReader(dbCommand)
    End Function

    Public Shared Function GetTaskFolderByID(ByVal TaskId As Integer) As IDataReader
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim sqlCommand As String = "SELECT TaskFolder_ID, TaskName, Remark FROM TaskFolders WHERE taskfolder_id = " & TaskId.ToString & " AND status = 'OK'"
        Dim dbCommand As DbCommand = db.GetSqlStringCommand(sqlCommand)
        Return db.ExecuteReader(dbCommand)
    End Function

    Public Overloads Shared Function GetTaskFolderList(ByVal UserId As String) As DataTable
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim sqlCommand As String = "SELECT TaskFolder_ID, TaskName, Remark, RecDate FROM TaskFolders WHERE createid = '" & UserId & "' AND status = 'OK'"
        Dim dbCommand As DbCommand = db.GetSqlStringCommand(sqlCommand)
        Return db.ExecuteDataSet(dbCommand).Tables(0)
    End Function

    Public Shared Sub UpdateTaskfolder(ByVal TaskId As Integer, ByVal TaskName As String, ByVal Remark As String, ByVal UserId As String)
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim dbCommand As DbCommand = db.GetStoredProcCommand("usp_taskfolders")
        db.AddInParameter(dbCommand, "TaskFolder_Id", DbType.Int32, TaskId)
        db.AddInParameter(dbCommand, "TaskName", DbType.String, TaskName)
        db.AddInParameter(dbCommand, "Remark", DbType.String, Remark)
        db.AddInParameter(dbCommand, "CreateId", DbType.String, UserId)
        db.ExecuteNonQuery(dbCommand)
    End Sub

    Public Shared Sub DeleteTaskfolder(ByVal TaskId As Integer)
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim sqlCommand As String = "UPDATE Taskfolders SET status = 'XX', RecDate = Getdate() WHERE taskfolder_id = " & TaskId.ToString
        Dim dbCommand As DbCommand = db.GetSqlStringCommand(sqlCommand)
        db.ExecuteNonQuery(dbCommand)

        sqlCommand = "UPDATE personalbox SET folder = 'DL', taskfolder_id=0 WHERE taskfolder_id = " & TaskId.ToString
        dbCommand = db.GetSqlStringCommand(sqlCommand)
        db.ExecuteNonQuery(dbCommand)
    End Sub
End Class
