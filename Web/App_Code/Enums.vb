Imports Microsoft.VisualBasic

Public Class Enums
    Enum Mark
        Unread = 0
        Read = 1
    End Enum
    Enum MailSize
        Attach = 0
        MailBox = 1
    End Enum
    'User's type
    Enum UserType
        UserId = 0
        LoginId = 1
    End Enum
End Class
