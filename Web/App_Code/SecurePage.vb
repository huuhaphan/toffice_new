Imports System.Web

Public Class SecurePage
    Inherits System.Web.UI.Page

    Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Response.Cache.SetAllowResponseInBrowserHistory(False)
        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetNoStore()
        Response.Expires = 0
        RequestAuthorize()
    End Sub

    Public ReadOnly Property BaseUrl() As String
        Get
            Dim url As String = Me.Request.ApplicationPath
            If url.EndsWith("/") Then
                Return url
            Else
                Return url + "/"
            End If
        End Get
    End Property

    Public ReadOnly Property FullBaseUrl() As String
        Get
            Return Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, "") + BaseUrl
        End Get
    End Property

    Public Sub RequestAuthorize()
        If Not IsAuthenticated() Then
            Response.Redirect(FormsAuthentication.LoginUrl & "?ReturnUrl=" + Request.Url.PathAndQuery)
        End If
    End Sub

    Function IsInRole(ByVal rolename As String) As Boolean
        Dim userrole As String = Session("UserRole")
        If Not IsNothing(userrole) AndAlso Not IsNothing(rolename) AndAlso userrole.IndexOf(rolename.Trim) >= 0 Then
            Return True
        End If
        Return False
    End Function

    Function IsAuthenticated() As Boolean
        'Return User.Identity.IsAuthenticated
        Return Common.IsAuthenticated
    End Function
End Class
