Imports System.Data
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports Microsoft.Practices.EnterpriseLibrary.Data.Sql

Public Class IUser
    Private mLoginID As String = String.Empty
    Private mUserID As String = String.Empty
    Private mName As String = String.Empty
    Private mRole As String = String.Empty
    Private misAuthenticated As Boolean = False
    Private mError As String = String.Empty
    Private dbSystem As String = System.Configuration.ConfigurationManager.AppSettings.Get("dbSystemName")
    Public ReadOnly Property ID() As String
        Get
            Return mUserID
        End Get
    End Property

    Public ReadOnly Property LoginID() As String
        Get
            Return mLoginID
        End Get
    End Property

    Public ReadOnly Property Name() As String
        Get
            Return mName
        End Get
    End Property

    Public ReadOnly Property Role() As String
        Get
            Return mRole
        End Get
    End Property
    Public ReadOnly Property IsAuthenticated() As String
        Get
            Return misAuthenticated
        End Get
    End Property

    Public ReadOnly Property ErrorCode() As String
        Get
            Return mError
        End Get
    End Property

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub New(ByVal username As String, ByVal password As String)
        MyBase.New()
        If username <> String.Empty Then
            Connect(username, password)
        End If
    End Sub

    Public Sub Connect(ByVal username As String, ByVal password As String)
        mError = String.Empty
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim dbCommand As DbCommand = db.GetStoredProcCommand("" & dbSystem & ".dbo.usp_Login")
        db.AddInParameter(dbCommand, "LoginName", DbType.String, username)
        db.AddInParameter(dbCommand, "Password", DbType.String, password)
        db.AddOutParameter(dbCommand, "KetQua", DbType.String, 500)
        db.ExecuteNonQuery(dbCommand)

        'Result
        Dim Result As String = db.GetParameterValue(dbCommand, "@KetQua")
        If Result.Chars(0) = "0" Then
            mUserID = VBLib.Strings.GetWordNum(Result, 2, Chr(13)).Trim
            mName = VBLib.Strings.GetWordNum(Result, 4, Chr(13)).Trim
            mLoginID = username
            mRole = GetRole(mUserID)
            misAuthenticated = True
        Else
            mError = VBLib.Strings.GetWordNum(Result, 2, Chr(13)).Trim
        End If
    End Sub

    Public Sub ChangePassword(ByVal username As String, ByVal oldpassword As String, ByVal newpassword As String)
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim dbCommand As DbCommand = db.GetStoredProcCommand("" & dbSystem & ".dbo.usp_ChangePassword")
        db.AddInParameter(dbCommand, "LoginName", DbType.String, username)
        db.AddInParameter(dbCommand, "Password", DbType.String, oldpassword)
        db.AddInParameter(dbCommand, "Newpassword", DbType.String, newpassword)
        db.AddOutParameter(dbCommand, "KetQua", DbType.String, 500)
        db.ExecuteNonQuery(dbCommand)
        'Reset error
        mError = String.Empty
        Dim Result As String = db.GetParameterValue(dbCommand, "@KetQua")
        If Result.Chars(0) = "1" Then
            mError = VBLib.Strings.GetWordNum(Result, 2, Chr(13)).Trim

        End If
    End Sub

    Function GetRole(ByVal LoginId As String) As String
        ' Create a string to persist the roles.
        Dim rolesString As String = String.Empty
        ' Get roles from UserRoles table, and add to cookie
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim sqlCommand As String = "SELECT role FROM dbo.userlist WHERE loginid ='" & LoginId + "'"
        Using dr As IDataReader = db.ExecuteReader(CommandType.Text, sqlCommand)
            If dr.Read() Then
                rolesString = dr("role")
            End If
        End Using
        GetRole = rolesString
    End Function

    Public Shared Function GetProfile(ByVal LoginId As String) As IProfile
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim sqlCommand As String = "SELECT deletetofolder, signature, signaturecontent FROM dbo.userlist WHERE loginid ='" & LoginId + "'"
        Dim Profile As New IProfile
        Using dr As IDataReader = db.ExecuteReader(CommandType.Text, sqlCommand)
            If dr.Read() Then
                Profile.LoginId = LoginId
                Profile.DeleteToFolder = dr("DeleteToFolder")
                Profile.Signature = dr("Signature")
                Profile.SignatureContent = dr("SignatureContent")
            End If
        End Using
        Return Profile
    End Function

    Public Shared Sub UpdateProfile(ByVal LoginId As String, ByVal DeleteToFolder As String, ByVal Signature As String, ByVal SignatureContent As String)
        Dim db As SqlDatabase = DatabaseFactory.CreateDatabase(Common.ConnectString)
        Dim dbCommand As DbCommand = db.GetStoredProcCommand("dbo.usp_UpdateProfile")
        db.AddInParameter(dbCommand, "LoginId", DbType.String, LoginId)
        db.AddInParameter(dbCommand, "DeleteToFolder", DbType.String, DeleteToFolder)
        db.AddInParameter(dbCommand, "Signature", DbType.String, Signature)
        db.AddInParameter(dbCommand, "SignatureContent", DbType.String, SignatureContent)
        db.ExecuteNonQuery(dbCommand)
    End Sub
End Class

Public Class IProfile
    Public LoginId As String
    Public DeleteToFolder As Char
    Public Signature As Char
    Public SignatureContent As String
End Class