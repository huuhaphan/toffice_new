Imports System.IO
Imports Telerik.Web.UI
Imports System.Data

Partial Public Class TemplateEdit
    Inherits SecurePage

    Private Property TemplateId() As Integer
        Get
            Return ViewState("TemplateId")
        End Get
        Set(ByVal value As Integer)
            ViewState("TemplateId") = value
        End Set
    End Property

    Private Property Link() As String
        Get
            Return ViewState("Link")
        End Get
        Set(ByVal value As String)
            ViewState("Link") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If Not IsInRole("NEWS") Then
                ErrHandler.ShowError(401)
            End If
            TemplateId = CInt(Common.GetQueryString("ID"))
            If TemplateId > 0 Then
                Using dr As IDataReader = News.GetTemplateById(TemplateId)
                    While dr.Read()
                        Me.txtDescription.Text = dr("Description")
                        Me.txtSubject.Text = dr("Subject")
                        Link = Trim(dr("Link"))
                    End While
                End Using
            End If
        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            Dim FolderDir As String = "~/Download/Templates/"
            RadUpload1.TargetFolder = FolderDir
            'File dinh kem
            If RadUpload1.UploadedFiles.Count > 0 Then
                For Each File As UploadedFile In RadUpload1.UploadedFiles
                    Link = FolderDir & File.GetName()
                    File.SaveAs(Path.Combine(Server.MapPath(FolderDir), File.GetName()))
                Next
            End If
            News.UpdateTemplates(TemplateId, "DO", txtSubject.Text, txtDescription.Text, Link)
            'Refresh page
            txtSubject.Text = ""
            txtDescription.Text = ""
        Catch ex As Exception
            ErrHandler.LogError(ex)
        End Try
    End Sub

End Class