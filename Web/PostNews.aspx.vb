Imports System.IO
Imports Telerik.Web.UI

Partial Public Class PostNews
    Inherits SecurePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If Not IsInRole("NEWS") Then
                ErrHandler.ShowError(401)
            End If
            ddlType.DataSource = News.GetNewsType()
            ddlType.DataBind()
        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            'File dinh kem
            If RadUpload1.UploadedFiles.Count > 0 Then
                Dim Link As String = "", Filename As String = ""
                Dim PathId As String = "", SaveDir As String = ""
                Call Common.GetPhysicalPath("NEWS", PathId, SaveDir)
                SaveDir = Common.VerifyFolder(SaveDir)
                Dim VirtualDir As String = Request.ApplicationPath & Common.AddBs("/News/" & Common.GetVitualPath(PathId))
                For Each File As UploadedFile In RadUpload1.UploadedFiles
                    Filename = News.GetFileName() & File.GetExtension
                    File.SaveAs(Path.Combine(SaveDir, Filename))
                    Link = Link & Common.AttachmentLink(VirtualDir & Filename, File.GetName)
                Next
                redtBody.Content = redtBody.Content & "<p>Đính kèm: " & Link & "</p>"
            End If
            News.insNews(ddlType.Text, txtSubject.Text, txtDescription.Text, redtBody.Content, Common.UserId)
            'Refresh page
            RadPageView1.Selected = True
            ddlType.Text = 4
            txtSubject.Text = ""
            txtDescription.Text = ""
            redtBody.Content = ""
        Catch ex As Exception
            ErrHandler.LogError(ex)
        End Try
    End Sub

End Class