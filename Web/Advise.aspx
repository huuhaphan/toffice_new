﻿<%@ Page Language="VB" MasterPageFile="~/Template.Master" AutoEventWireup="false" CodeFile="Advise.aspx.vb" Inherits="Advise" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
<div class="componentheading">Truyền thông và tư vấn </div>
    <div class="box-ct clearfix">
        <telerik:RadGrid ID="GridView" runat="server" AllowPaging="True" PageSize="20" AutoGenerateColumns="False"
            BorderWidth="0" GridLines="None" ShowHeader="false" OnNeedDataSource="GridView_NeedDataSource"
            OnDeleteCommand="RadGrid1_DeleteCommand">
            <MasterTableView DataKeyNames="Id">
                <NoRecordsTemplate>&nbsp;</NoRecordsTemplate>
                <Columns>
                    <telerik:GridTemplateColumn>
                        <ItemTemplate>
                            <asp:HyperLink ID="HyperLink" Text='<%# Eval("Subject") %>' NavigateUrl='<%# Eval("Link") %>'
                                 ToolTip='<%# Eval("Description") %>' runat="server" CssClass="templateLink"></asp:HyperLink> -
                            <asp:hyperlink ID="EditLink" Text='Sửa' NavigateUrl='<%# "~/adviseEdit.aspx?id=" & eval("Id") %>' runat="server"></asp:hyperlink><span> | </span>
                            <asp:LinkButton ID="lnkDelete" runat="server" CommandName="Delete" Text="Xóa" OnClientClick="javascript : return confirm('Bạn thực sự muốn xóa biểu mẫu này?');"></asp:LinkButton>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                </Columns>
                <PagerStyle Mode="NextPrevAndNumeric" PageSizeLabelText="Số mẫu hiển thị" NextPagesToolTip="" NextPageToolTip="Trang kế"
                    PageButtonCount="5" PagerTextFormat="Trang {0}/{1} {4} Tổng cộng {5} mẫu" PrevPagesToolTip=""
                    PrevPageToolTip="Trang trước" LastPageToolTip="Trang cuối" FirstPageToolTip="Trang đầu" />
            </MasterTableView>
        </telerik:RadGrid>
    </div>
</asp:Content>

