﻿
Partial Class UserProfile
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim UserProf As IProfile = IUser.GetProfile(Common.UserId)
            chkPurge.Checked = IIf(UserProf.DeleteToFolder = "N", True, False)
            chkSignature.Checked = IIf(UserProf.Signature = "Y", True, False)
            redtBody.Visible = chkSignature.Checked
            redtBody.Content = UserProf.SignatureContent
        End If
    End Sub

    Protected Sub btnExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Response.Redirect("~/Default.aspx")
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        IUser.UpdateProfile(Common.UserId, IIf(chkPurge.Checked = True, "N", "Y"), IIf(chkSignature.Checked = True, "Y", "N"), redtBody.Content)
    End Sub

    Protected Sub chkSignature_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSignature.CheckedChanged
        redtBody.Visible = chkSignature.Checked
    End Sub
End Class
