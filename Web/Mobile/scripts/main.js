﻿var userID;
function refreshLoginState() {
	var _userName = $.cookie("userName");
	if (_userName == null) {
		$("#loginButton").show();
		$("#logoutButton").hide();
		$("#loginResult").html("Bạn chưa đăng nhập vào hệ thống");
	}
	else {
		$("#loginResult").html("Ðăng nhập thành công: " + _userName);
		$("#loginButton").hide();
		$("#logoutButton").show();
	}
}

function loginUser() {
	logoutUser();

	var nameValue = $("#username").val();
	var pwdValue = $("#userpwd").val();
	var url = "/wsOffice/service.svc/GetUsers";
	$.getJSON(url, {usercode:nameValue,password:pwdValue},function(data)
	{
		userID = data.UserID;
		$.cookie("userID", userID)
		$.cookie("userName", data.FullName)
		refreshLoginState();
		kendo.history.navigate("#:back");
	});
}

function getMailByID(input) {
	var param = { uid:userID, pid:input};
	// $.getJSON('WebService.asmx/GetMailByID', {uid:userID,pid:input}, function(data)
	// {
		// $.cookie("currentrow", data);
		// alert(data);
	// });
	
	$.ajax(
		{ 
			type: 'POST', 
			dataType: 'json', 
			contentType: 'application/json', 
			url: 'WebService.asmx/GetMailByID',
			data: JSON.stringify(param),
		success: function(response) {
			var row = response.d[0];
			$.cookie("currentrow", row);
			alert("Load success");
		}
	});
	return $.cookie("currentrow");
}


function logoutUser() {
	$.cookie("userID",null);
	$.cookie("userName",null);
	refreshLoginState();
}

function isAuthenticated(e) {
	userID = $.cookie("userID");
	if (userID==null) {
		e.preventDefault();
		kendo.history.navigate(e.view.id);
		app.navigate("#loginView");
	}
}

var EndlessScrollDataSource = kendo.data.DataSource.extend({
	_observe: function(data) {
		if(this._page > 1) {
			this._data.push.apply(this._data, data);
			return this._data;
		}
		return kendo.data.DataSource.prototype._observe.call(this, data);
	}
});


var viewMailModel = kendo.observable({
	mailsData: new EndlessScrollDataSource({
		pageSize: 20,
		serverPaging: true,
		serverFiltering: true,
		transport: {
			read: {
				url: "WebService.asmx/GetMails",
				dataType: "json",
				contentType: "application/json; charset=utf-8",
				type: "POST"
			},
			parameterMap: function (options, operation) {
				var data = { uid: userID, skip: (options.page-1)*options.pageSize, take: options.pageSize}
				return JSON.stringify(data);
			}
		},
		error: function (e) {
			console.log(e.responseText);
		},
		//sort: { field: "date", dir: "desc" },           
		schema: {
			data: "d.Data",
			model: {
				id: "PID" 
			}
		}
	}),
	currentItem: null,
	mailto: [],
	mailbc: [],
	mailcc: [],
	showDetailsView: function (e) {
		var id = parseInt(e.view.params.id);
		var item = viewMailModel.mailsData.get(id);
		viewMailModel.set("currentItem", item);
		// var frame = $("#ifrm")[0].contentWindow.document;  			
		// frame.open();
		// frame.write(item.Content);
		// frame.close();
	},
	onOpen: function (e) {
		var item = viewMailModel.mailsData.get(parseInt(e.context));
		viewMailModel.set("currentItem", item);
		viewMailModel.set("mailto", []);
		viewMailModel.set("mailbc", []);
		viewMailModel.set("mailcc", []);
	},
	reply: function (e) {
		var item = viewMailModel.get("currentItem");
		if (item != null) { 
			$("#mailCompose").find(".km-view-title").html("Trả lời thư")
			$("#compose-subject").val("RE: " + item.Subject);
			$("#compose-content").html(item.Content);
			var toItem = {id: item.SenderID, name: item.Sender};
			viewMailModel.mailto.push(toItem);
			app.navigate("#mailCompose");
		}
	},
	replyAll: function (e) {
		var item = viewMailModel.get("currentItem");
		$("#mailCompose").find(".km-view-title").html("Trả lời cho mọi người")
		$("#compose-subject").val("RE: " + item.Subject);
		app.navigate("#mailCompose");
	},
	forward: function (e) {
		var item = viewMailModel.get("currentItem");
		$("#mailCompose").find(".km-view-title").html("Chuyển tiếp thư")
		$("#compose-subject").val("FW: " + item.Subject);
		viewMailModel.mailto.push(item.Sender);
		app.navigate("#mailCompose");
	},
	deleteMail: function (e) {
		$("#mailResult").html("Xóa thư #" + e.context);
	}
});

var viewDocumentModel = kendo.observable(
	{
		documentsData: new kendo.data.DataSource(
		{
			pageSize: 10,
			serverPaging: true,
			transport: {
				read: {
					type: "POST",
					url: "WebService.asmx/GetDocuments",
					contentType: "application/json; charset=utf-8",
					dataType: "json"
				},				
				parameterMap: function (options, operation) {
					var data = { uid: userID, skip: (options.page-1)*options.pageSize, take: options.pageSize}
					return JSON.stringify(data);
				}
			},
			schema: {
				data: "d.Data",
				model: {
					id: "PID"
				}
			}		
		}),
		currentItem: null,
		showDetailsView: function (e) {
			var id = parseInt(e.view.params.id), item = viewDocumentModel.documentsData.get(id);
			viewDocumentModel.set("currentItem", item);
		},
		onOpen: function (e) {
			var item = viewDocumentModel.documentsData.get(parseInt(e.context));
			currentItem = item;
			//this.element.find(".km-actionsheet-title").text(e.target.next().text());
		},
		forward: function (e) {
			$("#documentResult").html("Chuyển tiếp công văn #" + e.context);
		},
		deleteDocument: function (e) {
			$("#documentResult").html("Xóa công văn #" + e.context);
		}
	});

function mailInit(e) {
	$("#mailList").kendoMobileListView({
		dataSource: viewMailModel.mailsData,
		filterable: true,
		endlessScroll: true,
		scrollTreshold: 30, //treshold in pixels
		template: $("#mailTemplate").text()
	});
}

function documentInit(e) {
	$("#documentList").kendoMobileListView({
		dataSource: viewDocumentModel.documentsData,
		endlessScroll: true,
		scrollTreshold: 300, //treshold in pixels               
		template: $("#documentTemplate").text()
	});
	alert("end Init");
}	
	
function onCompose(e) {
	var toItems = viewMailModel.get("mailto"), bcItems = viewMailModel.get("mailbc"), ccItems = viewMailModel.get("mailcc");
	var toList="", bcList = "", ccList = "";
	for(var idx = 0; idx < toItems.length; idx++) {
		toList += "<a data-role='button' data-click='onUserClick' data-id='" + toItems[idx].id + "'>" + toItems[idx].name + "</a>";
	}
	$("#compose-to").html(toList);
	alert(toList);

	for(var idx = 0; idx < bcItems.length; idx++) {
		bcList += bcItems[idx];
	}
	$("#compose-bc").html(bcList);

	for(var idx = 0; idx < ccItems.length; idx++) {
		ccList += ccItems[idx];
	}
	$("#compose-cc").html(ccList);

	// $("#files").kendoUpload({
		// async: {
			// saveUrl: "save",
			// removeUrl: "remove",
			// autoUpload: true
		// }
	// });
}

function convertDateJToDateJSON(input) {
	var d = new Date(input);
	if (isNaN(d)) {
		throw new Error("input not date");
	}
	// here is how we force wcf to parse as UTC and give correct local time serverside        
	var date = '\/Date(' + d.getTime() + '-0000)\/';
	return date;
}

function convertJSONDateToDate(input, throwOnInvalidInput) {
	var pattern = /Date\(([^)]+)\)/;
	var results = pattern.exec(input);
	if (results.length != 2) {
		if (!throwOnInvalidInput) {
			return s;
		}
		throw new Error(s + " is not .net json date.");
	}
	return new Date(parseFloat(results[1]));
}

function formatJSONDate(input,format) {
	var pattern = /Date\(([^)]+)\)/;
	var results = pattern.exec(input);
	if (results.length != 2) {
		if (!throwOnInvalidInput) {
			return s;
		}
		throw new Error(s + " is not .net json date.");
	}
	return $.format.date(new Date(parseFloat(results[1])),format);
}
	
function writeCookie(name,value,days) {
    var date, expires;
    if (days) {
        date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        expires = "; expires=" + date.toGMTString();
            }else{
        expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";
	alert("Write Cookie done");
}

function readCookie(name) {
    var i, c, ca, nameEQ = name + "=";
    ca = document.cookie.split(';');
    for(i=0;i < ca.length;i++) {
        c = ca[i];
        while (c.charAt(0)==' ') {
            c = c.substring(1,c.length);
        }
        if (c.indexOf(nameEQ) == 0) {
            return c.substring(nameEQ.length,c.length);
        }
    }
    return '';
}

function env() { 
	var b = navigator.userAgent;
	var a = {};
	a.RIM = RegExp("BlackBerry").test(b);
	a.RIM6 = a.RIM && RegExp("Version/6").test(b);
	a.IE = RegExp("MSIE ").test(b);
	a.OperaMobile = RegExp("Opera Mobi").test(b);
	a.Webkit = RegExp(" AppleWebKit/").test(b);
	a.Apple = RegExp("iPhone").test(b) || RegExp("iPad").test(b) || RegExp("iPod").test(b);
	a.iPhoneNew = a.Apple && RegExp("OS (\\d+)").test(b) && RegExp.$1 >= 3;
	a.Android = RegExp("Android").test(b);
	a.Android2 = RegExp("Android 2").test(b);
	a.WebOS = RegExp("webOS").test(b);
	return a;
}
