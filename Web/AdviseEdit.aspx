﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Template.Master"
    CodeFile="AdviseEdit.aspx.vb" Inherits="AdviseEdit" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="componentheading">Cập nhật tư vấn và truyền thông</div>
    <table cellpadding="2" cellspacing="0" border="0" width="100%">
        <colgroup>
            <col width="60" />
            <col width="600" />
        </colgroup>
        <tr>
            <td>Tiêu đề</td>
            <td>
                <asp:TextBox ID="txtSubject" runat="server" Width="600px" CssClass="inputfield"></asp:TextBox>&nbsp;
            </td>
        </tr>
        <tr>
            <td valign="top">Ghi chú</td>
            <td>
                <asp:TextBox ID="txtDescription" runat="server" Height="56px" TextMode="MultiLine" Width="600px" ></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td valign="top">File kèm</td>
            <td>
                <telerik:RadUpload ID="RadUpload1" runat="server" InitialFileInputsCount="1"
                EnableFileInputSkinning="true" InputSize="50" MaxFileInputsCount="1" OverwriteExistingFiles="True" ControlObjectsVisibility="None"/>
            </td>
        </tr>
    </table>
    <hr />
    <asp:Button ID="btnSubmit" runat="server" Text="Cập nhật" ValidationGroup="Post"/>
</asp:Content>
