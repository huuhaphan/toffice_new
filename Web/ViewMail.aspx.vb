Imports Telerik.Web.UI

Partial Class ViewMail
    Inherits SecurePage
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            Dim MailId As Integer = CInt(Common.GetQueryString("id"))
            Dim PostId As Integer = CInt(Common.GetQueryString("postid"))

            Dim objDetail As Object = Mails.GetMailDetail(MailId)
            lblFrom.Text = objDetail.FromUser
            lblTo.Text = objDetail.ToUser
            lblCc.Text = objDetail.Cc
            lblSubject.Text = Server.HtmlEncode(objDetail.Subject)
            lblAttachment.Text = objDetail.Attachment
            lblBody.Text = objDetail.Body

            Dim replybtn As RadToolBarButton = RadToolBar1.FindItemByValue("Reply")
            replybtn.NavigateUrl = "~/compose.aspx?cmd=reply&type=ML&id=" & MailId.ToString
            Dim forwardbtn As RadToolBarButton = RadToolBar1.FindItemByValue("Forward")
            forwardbtn.NavigateUrl = "~/compose.aspx?cmd=forward&type=ML&id=" & MailId.ToString

            Dim NextMsgId, NextPostId, PrevMsgId, PrevPostId As Integer
            Mails.GetPrevNextMail(PostId, PrevMsgId, PrevPostId, NextMsgId, NextPostId)
            If PrevMsgId > 0 Then
                Dim prevbtn As RadToolBarButton = RadToolBar1.FindItemByValue("PrevMsg")
                prevbtn.NavigateUrl = "~/viewmail.aspx?id=" & PrevMsgId.ToString & "&postid=" & PrevPostId.ToString
            End If
            If NextMsgId > 0 Then
                Dim nextbtn As RadToolBarButton = RadToolBar1.FindItemByValue("NextMsg")
                nextbtn.NavigateUrl = "~/viewmail.aspx?id=" & NextMsgId.ToString & "&postid=" & NextPostId.ToString
            End If
        End If
    End Sub

    Protected Sub RadToolBar1_ButtonClick(ByVal sender As Object, ByVal e As RadToolBarEventArgs)
        If e.Item.Value = "Delete" Then
            Dim PostId As Integer = Integer.Parse(Common.GetQueryString("postid"))
            Common.MoveMessage(PostId, "DL")
            Common.CloseWindow()
        End If
    End Sub
End Class
