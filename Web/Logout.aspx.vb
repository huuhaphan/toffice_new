Public Partial Class Logout
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Session.Abandon()
        HttpContext.Current.User = Nothing
        FormsAuthentication.SignOut()
        Response.Redirect(FormsAuthentication.DefaultUrl)
    End Sub

End Class