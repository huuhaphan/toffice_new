Imports Telerik.Web.UI
Imports System.Data

Partial Public Class BrowseDocument
    Inherits SecurePage

    Private ReadOnly Property Folder() As String
        Get
            Return Common.GetQueryString("Folder")
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If Not IsInRole("DOCS") Then
                ErrHandler.ShowError(401)
            End If
            If Not Common.ValidFolder(Folder) Then
                ErrHandler.ShowError(601)
            End If
        End If
        Common.BuildToolbar(RadToolBar1, Common.UserId, "DO", Folder)
    End Sub

    Private Sub Radgrid1_ItemCreated(ByVal sender As Object, ByVal e As GridItemEventArgs) Handles RadGrid1.ItemCreated
        If TypeOf e.Item Is GridDataItem Then
            e.Item.Font.Bold = Not (e.Item.OwnerTableView.DataKeyValues(e.Item.ItemIndex)("Seen"))
        End If
    End Sub

    Public Sub RadGrid1_NeedDataSource(ByVal source As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs)
        RadGrid1.DataSource = Documents.GetDocumentList(Folder)
    End Sub

    Protected Sub RadAjaxManager1_AjaxRequest(ByVal sender As Object, ByVal e As Telerik.Web.UI.AjaxRequestEventArgs)
        Dim arguments As String() = e.Argument.Split(":")
        Select Case arguments(0)
            Case "ShowDetail"
                RefreshDetail(CInt(arguments(1)))
            Case "SetRead"
                Common.UpdateRead(CInt(arguments(1)), Enums.Mark.Read)
        End Select
    End Sub

    Sub RefreshDetail(Optional ByVal DocId As Integer = -1)
        If DocId > 0 Then
            Dim objDetail As Object = Documents.GetDocumentDetail(DocId)
            lblTo.Text = objDetail.ToUser
            lblSubject.Text = Server.HtmlEncode(objDetail.Subject)
            lblAttachment.Text = objDetail.Attachment
            lblBody.Text = objDetail.Body
            lblDocNum.Text = objDetail.Docnum
            lblDate.Text = objDetail.SendTime
        Else
            lblTo.Text = ""
            lblSubject.Text = ""
            lblAttachment.Text = ""
            lblBody.Text = ""
            lblDocNum.Text = ""
            lblDate.Text = ""
        End If
    End Sub

    Protected Sub RadToolBar1_ButtonClick(ByVal sender As Object, ByVal e As RadToolBarEventArgs)
        RequestAuthorize()
        Select Case e.Item.Value
            Case "Check"
                RadGrid1.Rebind()
            Case "Inbox"
                MoveToFolder("IN")
            Case "Delete"
                MoveToFolder("DL")
            Case Else
                MoveToFolder(e.Item.Value)
        End Select
    End Sub

    Protected Sub RadMenu1_ItemClick(ByVal sender As Object, ByVal e As RadMenuEventArgs)
        RequestAuthorize()
        Select Case e.Item.Value
            Case "Delete"
                MoveToFolder("DL")
            Case "DeleteAll"
                Common.MoveAllMessage("DO", Folder, "DL")
                RadGrid1.Rebind()
                RefreshDetail()
            Case "MarkRead"
                MarkMessage(Enums.Mark.Read)
            Case "MarkUnRead"
                MarkMessage(Enums.Mark.Unread)
        End Select
    End Sub

    Sub MoveToFolder(ByVal newvalue As String)
        If RadGrid1.SelectedItems.Count > 0 AndAlso RadGrid1.SelectedItems.Count <= RadGrid1.PageSize Then
            Dim ForceEmpty As Boolean = IIf(Folder = "DL" AndAlso newvalue = "DL", True, False)
            Dim PersonalBox_Id As Integer

            For Each dataItem As GridDataItem In RadGrid1.SelectedItems
                PersonalBox_Id = dataItem.GetDataKeyValue("PersonalBox_Id")
                If PersonalBox_Id <> 0 Then
                    Common.MoveMessage(PersonalBox_Id, newvalue, ForceEmpty)
                End If
            Next
            RadGrid1.Rebind()
            RefreshDetail()
        End If
    End Sub

    Sub MarkMessage(ByVal value As String)
        If RadGrid1.SelectedItems.Count > 0 AndAlso RadGrid1.SelectedItems.Count <= RadGrid1.PageSize Then
            Dim PersonalBox_Id As Integer

            For Each dataItem As Telerik.Web.UI.GridDataItem In RadGrid1.SelectedItems
                PersonalBox_Id = dataItem.GetDataKeyValue("PersonalBox_Id")
                If PersonalBox_Id <> 0 Then
                    Common.UpdateRead(PersonalBox_Id, value)
                End If
            Next
            RadGrid1.Rebind()
            RefreshDetail()
        End If
    End Sub
End Class