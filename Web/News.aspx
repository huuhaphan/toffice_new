﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/Template.Master" CodeFile="News.aspx.vb" Inherits="_News" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <script type="text/javascript">
        //<![CDATA[
        function ToolBarClicking(sender, args)
        {
            if (args.get_item().get_value() == "Delete") {if (!confirm("Bạn thực sự muốn xóa mẫu tin này?")) {args.set_cancel(true);}}
        } 
        //]]>
    </script>

    <div class="news">
        <telerik:RadToolBar ID="RadToolBar1" runat="server" OnClientButtonClicking="ToolBarClicking">
            <Items>
                <telerik:RadToolBarButton ImageUrl="~/images/print.gif" NavigateUrl="javascript:window.print()" ToolTip="In trang" PostBack="false" />
                <telerik:RadToolBarButton ImageUrl="~/images/delete.gif" Value="Delete" ToolTip="Xóa tin" />
            </Items>
        </telerik:RadToolBar>
        <div style="padding-top:1px" />
        <asp:GridView ID="RadGrid" runat="server" AutoGenerateColumns="False"
            ShowHeader="False" CaptionAlign="Top" Width="100%" BorderWidth="0px" CellPadding="6" CellSpacing="1">
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <h3 class="contentheading"><asp:Literal ID="Literal1" runat="server" Text='<%# Eval("Subject") %>' /></h3>
                        <div class="article-tools clearfix">
                            <div class="article-meta">
                                <asp:Literal runat="server" Text='<%# Format(Eval("RecDate"),"dd/MM/yyyy H:m") %>' /> |
                                <b><asp:Literal runat="server" Text='<%# VBLib.Strings.Proper(Common.GetUserName(Eval("RecUserId"))) %>' /></b>
                            </div>
                        </div>
                        <p style="font-weight:bold"><asp:Literal ID="Literal2" runat="server" Text='<%# Eval("Description") %>' /></p>
                        <span><asp:Literal ID="lblContent" runat="server" Text='<%# Eval("Content") %>' /></span>
                        <br /><div style="text-align:right">[<a href="javascript:history.go(-1)">Quay lại</a>]</div>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
</asp:Content>