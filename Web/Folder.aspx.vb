Imports Telerik.Web.UI
Imports system.Data
Partial Public Class Folder
    Inherits SecurePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If Not IsAuthenticated() Then
                ErrHandler.ShowError(401)
            End If
        End If
    End Sub
    Public Sub RadGrid1_NeedDataSource(ByVal source As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs)
        RadGrid1.DataSource = TaskFolders.GetTaskFolderList(Common.UserId)
    End Sub

    Protected Sub RadToolBar1_ButtonClick(ByVal sender As Object, ByVal e As RadToolBarEventArgs)
        If e.Item.Value = "Delete" Then
            'Delete
            Dim TaskFolder_Id As Integer
            For Each dataItem As Telerik.Web.UI.GridDataItem In RadGrid1.SelectedItems
                TaskFolder_Id = dataItem.GetDataKeyValue("TaskFolder_Id")
                TaskFolders.DeleteTaskfolder(TaskFolder_Id)
            Next
            RadGrid1.Rebind()
        End If
        Response.Redirect("Folder.aspx")
    End Sub

End Class
