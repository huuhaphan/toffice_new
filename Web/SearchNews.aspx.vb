
Partial Class SearchNews
    Inherits System.Web.UI.Page

    Protected Sub RadGrid1_NeedDataSource(ByVal source As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles RadGrid1.NeedDataSource
        RadGrid1.DataSource = News.GetNewsByText(Trim(txtSearch.Text), ddlIn.Text)
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        RadGrid1.DataSource = News.GetNewsByText(Trim(txtSearch.Text), ddlIn.Text)
        RadGrid1.DataBind()
    End Sub
End Class
