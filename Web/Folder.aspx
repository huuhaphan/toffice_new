﻿<%@ Page Language="VB" ValidateRequest="false" MasterPageFile="~/Template.Master" AutoEventWireup="false" CodeFile="Folder.aspx.vb" Inherits="Folder"%>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
    <script type="text/javascript">
        //<![CDATA[
        function ToolBarClicking(sender, args)
        {
            if (args.get_item().get_value() == "Delete") {if (!confirm("Bạn thực sự muốn xóa thư mục này?")) {args.set_cancel(true);}}
        } 
        //]]>
    </script>
    <div class="componentheading">Thư mục riêng</div>
    <telerik:RadToolBar ID="RadToolBar1" runat="server" OnButtonClick="RadToolBar1_ButtonClick" OnClientButtonClicking="ToolBarClicking">
        <Items>
            <telerik:RadToolBarButton ImageUrl="~/images/new.png" Text="Tạo mới" ToolTip="Tạo thư mục" Value="New" NavigateUrl="FolderEdit.aspx" PostBack="False" runat="server" />
            <telerik:RadToolBarButton ImageUrl="~/images/delete.gif" Text="Xóa" Value="Delete" ToolTip="X&#243;a" runat="server" />
        </Items>
    </telerik:RadToolBar>
    <div style="padding-top:1px"></div>
    <telerik:RadGrid ID="RadGrid1" runat="server" GridLines="None" AllowMultiRowSelection="True" AllowSorting="True"
        AllowPaging="true" AutoGenerateColumns="false" OnNeedDataSource="RadGrid1_NeedDataSource">
        <MasterTableView DataKeyNames="TaskFolder_Id">
            <Columns>
                <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn" HeaderStyle-Width="25px" />
                <telerik:GridHyperLinkColumn HeaderText="Tên thư mục" SortExpression="TaskName" DataNavigateUrlFields="taskfolder_id" DataTextField="TaskName" DataNavigateUrlFormatString="~/FolderEdit.aspx?id={0}" UniqueName="TaskName" />
                <telerik:GridBoundColumn DataField="Remark" HeaderText="Ghi chú" SortExpression="Remark"
                    UniqueName="Remark">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="RecDate" HeaderText="Ngày tạo" SortExpression="RecDate"
                    UniqueName="RecDate" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-Width="75px">
                </telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
        <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True" Selecting-AllowRowSelect="true" EnableRowHoverStyle="true" />
        <SortingSettings SortToolTip="Nhấn vào đây để sắp xếp"></SortingSettings>
        <PagerStyle Mode="NextPrevAndNumeric" NextPagesToolTip="" NextPageToolTip="Trang kế"
            PageButtonCount="5" PagerTextFormat="Trang {0}/{1} {4}" PrevPagesToolTip="" PrevPageToolTip="Trang trước" />
    </telerik:RadGrid>

    <telerik:RadAjaxManager ID="RadAjaxManager1" DefaultLoadingPanelID="LoadingPanel1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                    <telerik:AjaxUpdatedControl ControlID="RadMenu1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadToolBar1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>   
    <telerik:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server" />
</asp:Content>

