﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/TemplateFull.Master"
    CodeFile="Error.aspx.vb" Inherits="Error" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="componentheading">Thông báo lỗi</div>
    <div style="text-align :center; padding-top:30px;min-height:450px;" >
        <asp:Label runat="server" ID="lblError"  CssClass="error"/>            
        <br/>
        <br/>
        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Default.aspx" >Trở về Trang chủ</asp:HyperLink>
    </div>
</asp:Content>
