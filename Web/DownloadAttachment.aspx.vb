Partial Class DownloadAttachment
    Inherits SecurePage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        RequestAuthorize()
        If Common.GetQueryString("fileid") IsNot Nothing AndAlso Common.GetQueryString("type") IsNot Nothing Then
            Dim fileid As Integer = CInt(Request.QueryString("fileid"))
            Dim FileName As String = ""
            Dim PhysicalPath As String = ""
            If Common.GetQueryString("type") = "ML" Then
                Mails.GetMailAttachmentPath(fileid, FileName, PhysicalPath)
            Else
                Documents.GetDocAttachmentPath(fileid, FileName, PhysicalPath)
            End If
            If String.IsNullOrEmpty(PhysicalPath) Then
                ErrHandler.ShowError(604)
            Else
                Utils.SendFile(PhysicalPath, FileName)
            End If
        End If
    End Sub
End Class
