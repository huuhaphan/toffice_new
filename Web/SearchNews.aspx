﻿<%@ Page Language="VB" MasterPageFile="~/Template.Master" AutoEventWireup="false" CodeFile="SearchNews.aspx.vb" Inherits="SearchNews" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
    <div class="componentheading">Tìm tin tức</div>
    <table cellpadding="1" cellspacing="0" border="0">
        <tr> 
            <td>Tìm theo</td>
            <td>
                <asp:DropDownList ID="ddlIn" runat="server">
                    <asp:ListItem>[Tất cả]</asp:ListItem>
                    <asp:ListItem>Tiêu đề</asp:ListItem>
                    <asp:ListItem>Tóm tắt</asp:ListItem>
                    <asp:ListItem>Nội dung</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td><telerik:RadTextBox ID="txtSearch" runat="server" width="400px" EmptyMessage="Nhập từ khóa cần tìm." /></td>
            <td><asp:Button ID="btnSearch" runat="server" Text="Tìm kiếm" /></td>
        </tr>
    </table>
    <hr />
    <div class="news">
        <telerik:RadGrid ID="RadGrid1" runat="server" AutoGenerateColumns="False" GridLines="None"
            OnNeedDataSource="RadGrid1_NeedDataSource" ShowHeader="false" BorderWidth="0"
            PageSize="20" AllowPaging="True" AllowSorting="True" CssClass="searchfrm" Height="500" Width="728">
            <PagerStyle Mode="NextPrevAndNumeric" Position="Top"></PagerStyle>
            <MasterTableView Width="98%">
                <NoRecordsTemplate>Không tìm thấy kết quả nào</NoRecordsTemplate>
                <Columns>
                    <telerik:GridTemplateColumn>
                        <ItemTemplate>
                            <asp:HyperLink ID="HyperLink3" Text='<%# Eval("Subject") %>' NavigateUrl='<%# Eval("Id", "~/News.aspx?id={0}") %>'
                                runat="server" CssClass="thead"></asp:HyperLink><br />
                            <span class="subtitle">Ngày cập nhật:<asp:Literal ID="Literal2" runat="server" Text='<%# Format(Eval("recdate"),"dd/MM/yyyy") %>' /></span>
                            <span><asp:Literal ID="lblBody" runat="server" Text='<%# Eval("Description") %>' /></span>
                            <span class="article_seperator"></span>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                </Columns>
            </MasterTableView>
            <ClientSettings ReorderColumnsOnClient="True" AllowColumnsReorder="True" EnableRowHoverStyle="true">
                <Selecting AllowRowSelect="True"></Selecting>
                <Scrolling AllowScroll="True" UseStaticHeaders="True"></Scrolling>
            </ClientSettings>

        </telerik:RadGrid>
    </div>
</asp:Content>

