<%@ Page Language="vb" AutoEventWireup="true" CodeFile="InsertEmoticon.aspx.vb" Inherits="InsertEmoticon" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Insert Emoticon</title>
</head>
<body>
    <form id="form1" runat="server">
    <script type="text/javascript">
    function getRadWindow() //mandatory for the RadWindow dialogs functionality
    {
            if (window.radWindow)
            {
                return window.radWindow;
            }
            if (window.frameElement && window.frameElement.radWindow)
            {
                return window.frameElement.radWindow;
            }
                return null;
    }

    function initDialog() //called when the dialog is initialized
    {
       var clientParameters = getRadWindow().ClientParameters;
    }
    if (window.attachEvent)
    {
       window.attachEvent("onload", initDialog);
    }
    else if (window.addEventListener)
    {
       window.addEventListener("load", initDialog, false);
    }

    function insertEmoticon(url) //fires when the Insert Link button is clicked
    {
           var closeArgument = {};
           closeArgument.image = url
           getRadWindow().close(closeArgument); 
    }
    </script>

    <style>
        img
        {
            cursor: hand;
        }
    </style>


    <fieldset>
       <legend>Click on Emoticon to insert</legend>
        <img src="Images/Emoticons/smil1.GIF" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/smil2.GIF" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/smil9.GIF" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/smil3.GIF" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/smil4.GIF" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/smil5.GIF" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/smil6.GIF" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/smil7.GIF" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/smil8.GIF" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/smil10.GIF" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/smil11.GIF" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/1.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/2.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/3.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/4.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/5.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/6.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/7.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/8.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/9.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/10.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/11.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/12.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/13.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/14.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/15.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/16.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/17.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/18.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/19.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/20.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/21.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/22.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/23.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/24.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/25.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/26.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/27.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/28.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/29.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/30.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/31.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/32.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/33.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/34.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/35.gif"  onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/36.gif"  onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/37.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/38.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/39.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/40.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/41.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/42.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/43.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/44.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/45.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/46.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/47.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/48.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/49.gif" onclick="insertEmoticon(this.src)"  />
        <img src="Images/Emoticons/50.gif" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/51.gif" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/52.gif" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/53.gif" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/54.gif" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/55.gif" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/56.gif" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/57.gif" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/58.gif" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/59.gif" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/60.gif" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/61.gif" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/62.gif" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/63.gif" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/64.gif" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/65.gif" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/66.gif" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/67.gif" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/68.gif" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/69.gif" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/70.gif" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/71.gif" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/72.gif" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/73.gif" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/74.gif" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/75.gif" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/76.gif" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/77.gif" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/78.gif" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/79.gif" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/80.gif" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/81.gif" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/82.gif" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/83.gif" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/84.gif" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/85.gif" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/86.gif" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/87.gif" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/88.gif" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/89.gif" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/90.gif" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/91.gif" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/92.gif" onclick="insertEmoticon(this.src)" />
        <img src="Images/Emoticons/93.gif" onclick="insertEmoticon(this.src)" />
    </fieldset> 
    </form>
</body>
</html>
