﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/InnerTemplate.Master"
    ValidateRequest="false" CodeFile="BrowseMail.aspx.vb" Inherits="BrowseMail" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">

        <script type="text/javascript">
            //<![CDATA[
            var msgid;
            var postid;
            var grid;
            var ajaxManager;
            var alertTimerId = 0;
            var dataItem;
            var msgidover;
            var postidover;

            function pageLoad() {
                grid = $find("<%=RadGrid1.ClientID %>");
                ajaxManager = $find("<%=RadAjaxManager1.ClientID %>");
            }

            function RowSelected(sender, args) {
                clearTimeout(alertTimerId);
                msgid = args.getDataKeyValue("Source_Id");
                postid = args.getDataKeyValue("PersonalBox_Id");

                if (grid.get_masterTableView().get_selectedItems().length == 1) {
                    ajaxManager.ajaxRequest("ShowDetail:" + msgid);
                    dataItem = grid.get_masterTableView().get_dataItems()[args.get_itemIndexHierarchical()];
                    if (dataItem.get_element() != undefined && dataItem.get_element().style.fontWeight == "bold") {
                        alertTimerId = setTimeout("SetMarkRead()", 5000);
                    }
                }
            }

            function RowDblClick(sender, args) {
                var qs = new Querystring();
                var folder = qs.get('folder');
                var page = (folder == "DR") ? "compose.aspx" : "viewmail.aspx";
                openWindow(getUrl(page, "ML", "", msgid, postid));
                //SetMarkRead();
            }

            function RowMouseOver(sender, args) {
                msgidover = args.getDataKeyValue("Source_Id");
                postidover = args.getDataKeyValue("PersonalBox_Id");
            }

            function SetMarkRead() {
                if (dataItem.get_element() != undefined) {
                    dataItem.get_element().style.fontWeight = "Normal";
                    ajaxManager.ajaxRequest("SetRead:" + postid);
                }
            }

            function ToolbarClicking(sender, args) {
                var item = args.get_item();
                var itemvalue = args.get_item().get_value();
                if (itemvalue == "Forward" || itemvalue == "Reply" || itemvalue == "ReplyAll") {
                    if (grid.get_masterTableView().get_selectedItems().length == 0) {
                        alert("Bạn phải chọn thư !");
                        args.set_cancel(true);
                    }
                    else {
                        item.set_navigateUrl(getUrl("compose.aspx", "ML", itemvalue, msgid));
                    }
                }
                else if (itemvalue == "Delete") {
                    if (!confirm("Bạn thực sự muốn xóa tất cả các thư đã chọn ?")) { args.set_cancel(true); }
                }
            }

            function RowContextMenu(sender, args) {
                var menu = $find("<%=RadMenu1.ClientID %>");
                var evt = args.get_domEvent();
                menu.show(evt);

                evt.cancelBubble = true;
                evt.returnValue = false;

                if (evt.stopPropagation) {
                    evt.stopPropagation();
                    evt.preventDefault();
                }
            }

            function ContextMenuClicking(sender, args) {
                var item = args.get_item();
                var itemvalue = item.get_value();
                if (itemvalue == "Reply" || itemvalue == "Forward") { item.set_navigateUrl(getUrl("compose.aspx", "ML", itemvalue, msgidover)); }
                else if (itemvalue == "Open") {
                    var qs = new Querystring();
                    var folder = qs.get('folder');
                    var page = (folder == "DR") ? "compose.aspx" : "viewmail.aspx";
                    item.set_navigateUrl(getUrl(page, "ML", "", msgidover, postidover));
                }
                else if (itemvalue == "Delete") {
                    if (!confirm("Bạn thực sự muốn xóa thư này ?")) { args.set_cancel(true); }
                }
            }

            function GridKeyPressed(sender, eventArgs) {
                if (eventArgs.get_keyCode() == 40 || eventArgs.get_keyCode() == 38) { eventArgs.set_cancel(false) }
                else { eventArgs.set_cancel(true) }
            }
            //]]>
        </script>
    </telerik:RadCodeBlock>

    <telerik:RadSplitter ID="RadSplitter1" runat="server" Width="100%" Orientation="Horizontal"
        Height="100%" BorderStyle="None" PanesBorderSize="0">
        <telerik:RadPane ID="ToolbarPane" runat="server" Height="30px" Scrolling="None">
            <telerik:RadToolBar ID="RadToolBar1" runat="server" CssClass="toolbar" OnButtonClick="RadToolBar1_ButtonClick" OnClientButtonClicking="ToolbarClicking">
                <Items>
                    <telerik:RadToolBarButton ImageUrl="~/images/newmail.gif" Text="Tạo mới" ToolTip="Tạo thư"
                        NavigateUrl="compose.aspx?type=ML" Target="_blank" PostBack="False" runat="server" />
                    <telerik:RadToolBarButton IsSeparator="True" runat="server" />
                    <telerik:RadToolBarButton ImageUrl="~/images/checkmail.gif" ToolTip="Nhận thư mới"
                        Value="Check" runat="server" />
                    <telerik:RadToolBarButton IsSeparator="True" runat="server" />
                    <telerik:RadToolBarButton ImageUrl="~/images/delete.gif" Value="Delete" ToolTip="X&#243;a thư"
                        runat="server" />
                    <telerik:RadToolBarButton IsSeparator="True" runat="server" />
                    <telerik:RadToolBarButton ImageUrl="~/images/reply.gif" Text="Trả lời" Target="_blank"
                        NavigateUrl="#" Value="Reply" PostBack="False" runat="server" />
                    <telerik:RadToolBarButton ImageUrl="~/images/replyall.gif" Text="Trả lời tất cả" Target="_blank"
                        NavigateUrl="#" Value="ReplyAll" PostBack="False" runat="server" />                       
                    <telerik:RadToolBarButton ImageUrl="~/images/forward.gif" Text="Chuyển tiếp" Target="_blank"
                        NavigateUrl="#" Value="Forward" PostBack="False" runat="server" />
                    <telerik:RadToolBarDropDown ImageUrl="~/images/movetofolder.gif" Text="Thư mục" ToolTip="Di chuyển đến thư mục"
                        runat="server" />
                    <telerik:RadToolBarButton IsSeparator="True" runat="server" />
                    <telerik:RadToolBarButton Text="Thống kê" NavigateUrl="Statistic.aspx" PostBack="false" Target="_blank"
                        runat="server">
                    </telerik:RadToolBarButton>
                </Items>
            </telerik:RadToolBar>
            <div style="border-top: 1px solid gray" />
        </telerik:RadPane>
        <telerik:RadPane ID="GridPane" runat="server" Scrolling="None">
            <telerik:RadGrid ID="RadGrid1" runat="server" BorderWidth="0" AutoGenerateColumns="False"
                OnNeedDataSource="RadGrid1_NeedDataSource" ShowGroupPanel="True" Width="100%"
                Height="100%" AllowMultiRowSelection="True" AllowSorting="True" OnItemCreated="RadGrid1_ItemCreated"
                AllowPaging="true" PageSize="20" >
                <MasterTableView TableLayout="Fixed" DataKeyNames="PersonalBox_Id,Source_ID,Seen"
                    ClientDataKeyNames="PersonalBox_Id,Source_Id" GroupLoadMode="Client">
                    <NoRecordsTemplate><div class="NoRecord">Không có thư trong thư mục này</div></NoRecordsTemplate>
                    <Columns>
                        <telerik:GridClientSelectColumn UniqueName="ClientSelectColumn" Reorderable="False"
                            HeaderStyle-Width="27px" />
                        <telerik:GridTemplateColumn UniqueName="Important" Groupable="False" HeaderStyle-Width="27px">
                            <ItemTemplate><%# IIF(Eval("Important")="X","<img src='images/important.gif'>","&nbsp;") %></ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn SortExpression="From" HeaderText="Người gửi" DataField="From"
                            UniqueName="From" HeaderStyle-Width="180px">
                        </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn HeaderText="Người nhận" UniqueName="To" HeaderStyle-Width="180px">
                            <ItemTemplate></ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn SortExpression="Subject" HeaderText="Chủ đề" DataField="Subject"
                            UniqueName="Subject" HeaderStyle-Width="270px" ItemStyle-Wrap="true">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="Sendtime" HeaderText="Ng&#224;y nhận" DataField="Sendtime"
                            DataFormatString="{0:dd/MM/yyyy}" UniqueName="Sendtime" HeaderStyle-Width="70px">
                        </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn SortExpression="AttachFileSize" HeaderText="Kích cỡ"
                            UniqueName="AttachFileSize" ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="65px">
                            <ItemTemplate><%# Mails.ToByteString(Eval("AttachFileSize"))%></ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn UniqueName="Attach" Groupable="False" HeaderStyle-Width="27px">
                            <ItemTemplate><%# IIF(Eval("AttachFile")="X","<img src='images/Attch.gif'>","&nbsp;") %></ItemTemplate>
                        </telerik:GridTemplateColumn>
                    </Columns>
                </MasterTableView>
                <ClientSettings AllowKeyboardNavigation="true" ClientEvents-OnKeyPress="GridKeyPressed" ReorderColumnsOnClient="True" 
                    AllowDragToGroup="True" AllowColumnsReorder="True" EnableRowHoverStyle="True">
                    <Selecting AllowRowSelect="True"></Selecting>
                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                    <ClientEvents OnRowContextMenu="RowContextMenu" OnRowSelected="RowSelected" OnRowDblClick="RowDblClick" OnRowMouseOver="RowMouseOver" />
                </ClientSettings>
                <PagerStyle Mode="NextPrevAndNumeric" PageSizeLabelText="Số thư hiển thị" NextPagesToolTip="" NextPageToolTip="Trang kế"
                    PageButtonCount="5" PagerTextFormat="Trang {0}/{1} {4} Tổng cộng {5} thư" PrevPagesToolTip=""
                    PrevPageToolTip="Trang trước" LastPageToolTip="Trang cuối" FirstPageToolTip="Trang đầu" />
                <GroupPanel Text="Nh&#243;m theo t&#234;n cột" ToolTip="Đưa con trỏ v&#224;o &#244; t&#234;n cột, nhấn v&#224; giữ nguy&#234;n n&#250;t tr&#225;i chuột sau đ&#243; k&#233;o v&#224; thả v&#224;o &#244; n&#224;y" />
                <GroupingSettings ShowUnGroupButton="true" />
            </telerik:RadGrid>
        </telerik:RadPane>
        <telerik:RadSplitBar ID="RadSplitBar1" runat="server" CollapseMode="Backward" />
        <telerik:RadPane ID="ContentPane" runat="server" Height="150">
            <asp:Panel ID="MessagePanel" runat="server">
                <div class="messagetitle">
                    <asp:Label ID="lblSubject" runat="server" Font-Bold="true" />
                    <br />
                    <asp:Label ID="lblFrom" runat="server" Font-Bold="true" />
                    <asp:Label ID="lblSendTime" runat="server" CssClass="rightcol" />
                    <br />
                    <asp:Label ID="lblTo" runat="server" />
                    <br />
                    <asp:Label ID="lblAttachment" runat="server" />
                </div>
                <div class="messagebody">
                    <asp:Label ID="lblBody" runat="server"></asp:Label>
                </div>
            </asp:Panel>
        </telerik:RadPane>
    </telerik:RadSplitter>

    <telerik:RadAjaxManager ID="RadAjaxManager1" DefaultLoadingPanelID="LoadingPanel1"
        OnAjaxRequest="RadAjaxManager1_AjaxRequest" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="MessagePanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadToolBar1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadMenu1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server" />

    <telerik:RadContextMenu ID="RadMenu1" runat="server" OnItemClick="RadMenu1_ItemClick"
        OnClientItemClicking="ContextMenuClicking">
        <Items>
            <telerik:RadMenuItem Text="Xem thư" runat="server" Value="Open" NavigateUrl="#" Target="_blank"
                PostBack="false" />
            <telerik:RadMenuItem IsSeparator="true" />
            <telerik:RadMenuItem Text="Trả lời" runat="server" ImageUrl="images/reply.gif" Value="Reply"
                NavigateUrl="#" Target="_blank" PostBack="false" />
            <telerik:RadMenuItem Text="Chuyển tiếp" runat="server" ImageUrl="images/forward.gif"
                Value="Forward" NavigateUrl="#" Target="_blank" PostBack="false" />
            <telerik:RadMenuItem Text="Xóa thư" runat="server" Value="Delete" />
            <telerik:RadMenuItem IsSeparator="true" />
            <telerik:RadMenuItem Text="Đánh dấu đã đọc" runat="server" Value="MarkRead" />
            <telerik:RadMenuItem Text="Đánh dấu chưa đọc" runat="server" Value="MarkUnRead" />
            <telerik:RadMenuItem IsSeparator="true" />
            <telerik:RadMenuItem Text="Xóa tất cả thư" runat="server" Value="DeleteAll" />
        </Items>
        <CollapseAnimation Duration="200" Type="OutQuint" />
    </telerik:RadContextMenu>
</asp:Content>
