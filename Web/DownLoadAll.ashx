﻿<%@ WebHandler Language="VB" Class="DownLoadAll" %>

Imports System
Imports System.Web

Public Class DownLoadAll : Implements IHttpHandler
    
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        Dim SoureId As Integer = CInt(context.Request.QueryString("refid"))
        Dim Type As String = context.Request.QueryString("type").Trim

        Dim AttachList As New Collection
        If Type = "ML" Then
            Mails.GetAllMailAttachment(SoureId, AttachList)
        Else
            Documents.GetAllDocAttachment(SoureId, AttachList)
        End If
        Dim tempFileName = context.Server.MapPath("TempFiles/" & Guid.NewGuid().ToString() & ".zip")
        Dim tempZipName As String = SoureId.ToString + ".zip"
        Utils.ZipAllFiles(tempFileName, tempZipName, AttachList)
    End Sub
 
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class